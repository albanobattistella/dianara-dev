Name:		dianara
Version:	1.3.1
Release:	1%{?dist}
Summary:	Pump.io social network client

License:	GPLv2+
# Group:	Networking/News
URL:		http://jancoding.wordpress.com/dianara/
Source0:	http://qt-apps.org/CONTENT/content-files/148103-dianara-v%{version}.tar.gz

BuildRequires:	qt-devel
BuildRequires:	qjson-devel
BuildRequires:	qoauth-devel
BuildRequires:	ImageMagick
BuildRequires:  file-devel
Requires:	qt
Requires:	qjson
Requires:	qoauth
Requires:	qca-ossl

%description
Dianara is a Pump.io client, a desktop application for GNU/Linux
that allows users to manage their Pump.io social networking
accounts without the need to use a web browser.

%prep
%setup -qn %{name}-v%{version}

%build
qmake-qt4
make

%install
%define apps %{_datadir}/applications/
%define pixmaps %{_datadir}/pixmaps/
%define locale	%{_datadir}/%{name}/locale/

rm -rf %{buildroot}

mkdir -p %{buildroot}%{_bindir}/
cp -p %{name} %{buildroot}%{_bindir}/

mkdir -p %{buildroot}%{apps}
cp -p %{name}.desktop %{buildroot}%{apps}

mkdir -p %{buildroot}%{pixmaps}
cp -p icon/64x64/%{name}.png %{buildroot}%{pixmaps}%{name}.png

mkdir -p %{buildroot}%{locale}
cp -p translations/*.qm %{buildroot}%{locale}

%files
%doc CHANGELOG LICENSE README BUGS TODO
%{_bindir}/%{name}
%{apps}%{name}.desktop
%{pixmaps}%{name}.png
%{locale}*.qm

%changelog
* Tue Aug 18 2015 Roman Yepishev <rye@keypressure.com> 1.3.1-1
- Upgrade to 1.3.1
* Wed Oct 30 2013 Silvio Amici <silkevicious@gmail.com> 1.0-3
- Upgrade from beta2 to release, fixed i686 package problem
* Thu Oct 24 2013 Silvio Amici <silkevicious@gmail.com> 1.0-2
- Dianara 1.0-2 package creation
