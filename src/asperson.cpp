/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "asperson.h"

ASPerson::ASPerson(QVariantMap personMap,
                   QObject *parent) : QObject(parent)
{
    m_id = this->cleanupId(personMap.value("id").toString());
    m_name = personMap.value("displayName").toString().trimmed();
    m_avatar = personMap.value("image").toMap()
                        .value("url").toString();
    m_url = personMap.value("url").toString();
    m_hometown = personMap.value("location").toMap()
                          .value("displayName").toString().trimmed();
    m_bio = personMap.value("summary").toString();

    m_outboxLink = personMap.value("links").toMap()
                            .value("activity-outbox").toMap()
                            .value("href").toString();

    m_followed = personMap.value("pump_io").toMap()
                          .value("followed").toBool();

    m_createdAt = personMap.value("published").toString();
    m_updatedAt = personMap.value("updated").toString();


    //qDebug() << "ASPerson created" << m_id;
}


ASPerson::~ASPerson()
{
    //qDebug() << "ASPerson destroyed" << m_id;
}



void ASPerson::updateDataFromPerson(ASPerson *person)
{
    m_id = person->getId();
    m_name = person->getNameWithFallback();
    m_avatar = person->getAvatarUrl();
    m_url = person->getUrl();
    m_hometown = person->getHometown();
    m_bio = person->getBio();

    m_outboxLink = person->getOutboxLink();

    m_followed = person->isFollowed();

    m_createdAt = person->getCreatedAt();
    m_updatedAt = person->getupdatedAt();
}


QString ASPerson::getId()
{
    return m_id;
}

QString ASPerson::cleanupId(QString id)
{
    if (id.startsWith(QStringLiteral("acct:")))
    {
        id.remove(0, 5);
    }

    return id;
}



QString ASPerson::getName()
{
    return m_name;
}

QString ASPerson::getNameWithFallback()
{
    if (!m_name.isEmpty())
    {
        return m_name;
    }
    else
    {
        return m_id;
    }
}


QString ASPerson::makeNameIdString(QString userName, QString userId)
{
    QString nameAndId = userName.trimmed();
    if (nameAndId.isEmpty())
    {
        nameAndId = userId;
    }
    else
    {
        nameAndId.append(QStringLiteral(" (")
                         + userId
                         + QStringLiteral(")"));
    }

    return nameAndId;
}


QString ASPerson::getHometown()
{
    return m_hometown;
}

QString ASPerson::getBio()
{
    return m_bio;
}

QString ASPerson::getAvatarUrl()
{
    return m_avatar;
}

QString ASPerson::getUrl()
{
    return m_url;
}

QString ASPerson::getTooltipInfo()
{
    if (m_id.isEmpty())
    {
        return QString(); // If there's no ID, there's no valid person data
    }

    // FIXME: make these fields safe for HTML

    QString tooltipContents;
    if (!m_name.trimmed().isEmpty())
    {
        tooltipContents.append("<b>" + m_name.trimmed() + "</b><br />");
    }

    tooltipContents.append("<b><i>" + m_id + "</i></b>");

    if (!m_hometown.isEmpty() || !m_bio.isEmpty())
    {
        // More data coming, add a line
        tooltipContents.append(QStringLiteral("<hr><br />"));
    }

    if (!m_hometown.isEmpty())
    {
        tooltipContents.append("<b>" + tr("Hometown") + "</b>: "
                               + m_hometown);
        tooltipContents.append(QStringLiteral("<br /><br />"));
    }

    if (!m_bio.isEmpty())
    {
        tooltipContents.append(m_bio);
    }

    tooltipContents.replace("\n",
                            QStringLiteral("<br />")); // HTML newlines


    return tooltipContents;
}



QString ASPerson::getOutboxLink()
{
    return m_outboxLink;
}



bool ASPerson::isFollowed()
{
    return this->m_followed;
}


QString ASPerson::getCreatedAt()
{
    return this->m_createdAt;
}

QString ASPerson::getupdatedAt()
{
    return this->m_updatedAt;
}
