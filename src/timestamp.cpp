/*
 *   This file is part of Dianara
 *   Copyright 2012-2019  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "timestamp.h"

Timestamp::Timestamp(QObject *parent) : QObject(parent)
{
    // Creating object not required
}



/*
 *  returns " 07-02-2012 - 01:32:02", adjusted to local time
 *
 *  isoTime = ISO 8601, UTC, like "2012-02-07T01:32:02Z"
 */
QString Timestamp::localTimeDate(QString isoTime)
{
    //qDebug() << "::localTimeDate - isoTime: " << isoTime;
    QDateTime dateTimeConverter = QDateTime::fromString(isoTime, Qt::ISODate).toLocalTime();


    QString localTimeDateString;
    localTimeDateString = dateTimeConverter.date().toString(Qt::DefaultLocaleLongDate);
    localTimeDateString.append(", ");
    localTimeDateString.append(dateTimeConverter.time().toString());


    return localTimeDateString;
}



/*
 *  returns "about an hour ago", etc, based on local time
 *
 *  isoTime = ISO 8601, UTC, like "2012-02-07T01:32:02Z"
 */
QString Timestamp::fuzzyTime(QString isoTime)
{
    if (isoTime.isEmpty())
    {
        return tr("Invalid timestamp!");
    }

    QDateTime dateTimeConverter = QDateTime::fromString(isoTime, Qt::ISODate);

    int timeDifference = QDateTime::currentDateTimeUtc().toTime_t()
                         - dateTimeConverter.toTime_t();
    //qDebug() << "time difference in seconds:" << timeDifference;

    QString localTimeDateString;


    // At this point, timeDifference is in SECONDS

    if (timeDifference < 60) // less than a minute, or FUTURE
    {
        if (timeDifference >= 0)
        {
            localTimeDateString = tr("Just now");
        }
        else  // negative difference, so timestamp is in the future
        {
            localTimeDateString = tr("In the future");
        }
    }
    else
    {
        timeDifference /= 60; // convert to minutes
        if (timeDifference < 60) // less than an hour
        {
            if (timeDifference == 1)
            {
                localTimeDateString = tr("A minute ago");
            }
            else
            {
                localTimeDateString = tr("%1 minutes ago").arg(timeDifference);
            }
        }
        else
        {
            timeDifference /= 60; // convert to hours

            if (timeDifference < 24) // less than a day
            {
                if (timeDifference == 1)
                {
                    localTimeDateString = tr("An hour ago");
                }
                else
                {
                    localTimeDateString = tr("%1 hours ago").arg(timeDifference);
                }
            }
            else
            {
                timeDifference /= 24; // convert to days

                if (timeDifference < 30) // less than a (average) month
                {
                    if (timeDifference == 1)
                    {
                        localTimeDateString = tr("Yesterday");
                    }
                    else
                    {
                        localTimeDateString = tr("%1 days ago").arg(timeDifference);
                    }
                }
                else
                {
                    timeDifference /= 30; // convert to months, approx.

                    if (timeDifference < 12) // less than a year
                    {
                        if (timeDifference == 1)
                        {
                            localTimeDateString = tr("A month ago");
                        }
                        else
                        {
                            localTimeDateString = tr("%1 months ago").arg(timeDifference);
                        }
                    }
                    else
                    {
                        timeDifference /= 12;

                        if (timeDifference == 1)
                        {
                            localTimeDateString = tr("A year ago");
                        }
                        else
                        {
                            localTimeDateString = tr("%1 years ago").arg(timeDifference);
                        }
                    }
                }
            }
        }
    }
    // No, I'm not particularly proud of this code.... oh, well...


    //qDebug() << "Posted:" << localTimeDateString;


    return localTimeDateString;
}
