<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="gl_ES">
<context>
    <name>ASActivity</name>
    <message>
        <location filename="../src/asactivity.cpp" line="89"/>
        <location filename="../src/asactivity.cpp" line="135"/>
        <source>Public</source>
        <translation>Público</translation>
    </message>
    <message>
        <location filename="../src/asactivity.cpp" line="422"/>
        <source>%1 by %2</source>
        <comment>1=kind of object: note, comment, etc; 2=author&apos;s name</comment>
        <translation>%1 por %2</translation>
    </message>
</context>
<context>
    <name>ASObject</name>
    <message>
        <location filename="../src/asobject.cpp" line="275"/>
        <source>Note</source>
        <comment>Noun, an object type</comment>
        <translation>Nota</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="280"/>
        <source>Article</source>
        <comment>Noun, an object type</comment>
        <translation>Artigo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="285"/>
        <source>Image</source>
        <comment>Noun, an object type</comment>
        <translation>Imaxe</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="290"/>
        <source>Audio</source>
        <comment>Noun, an object type</comment>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="295"/>
        <source>Video</source>
        <comment>Noun, an object type</comment>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="300"/>
        <source>File</source>
        <comment>Noun, an object type</comment>
        <translation>Ficheiro</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="305"/>
        <source>Comment</source>
        <comment>Noun, as in object type: a comment</comment>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="310"/>
        <source>Group</source>
        <comment>Noun, an object type</comment>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="315"/>
        <source>Collection</source>
        <comment>Noun, an object type</comment>
        <translation>Colección</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="320"/>
        <source>Other</source>
        <comment>As in: other type of post</comment>
        <translation>Outro</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="376"/>
        <source>No detailed location</source>
        <translation>Non hai ubicación detallada</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="402"/>
        <source>Deleted on %1</source>
        <translation>Borrado en %1</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="578"/>
        <location filename="../src/asobject.cpp" line="648"/>
        <source>and one other</source>
        <translation>e un máis</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="582"/>
        <location filename="../src/asobject.cpp" line="652"/>
        <source>and %1 others</source>
        <translation>e %1 máis</translation>
    </message>
</context>
<context>
    <name>ASPerson</name>
    <message>
        <location filename="../src/asperson.cpp" line="173"/>
        <source>Hometown</source>
        <translation>Cidade</translation>
    </message>
</context>
<context>
    <name>AccountDialog</name>
    <message>
        <location filename="../src/accountdialog.cpp" line="85"/>
        <source>Your Pump.io address:</source>
        <translation>O seu enderezo Pump.io:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="95"/>
        <source>Get &amp;Verifier Code</source>
        <translation>Obter Código de &amp;Verificación</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="128"/>
        <source>Verifier code:</source>
        <translation>Código de verificación:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="131"/>
        <source>Enter or paste the verifier code provided by your Pump server here</source>
        <translation>Introduza ou pegue eiquí o código de verificación que lle proporcionou o seu servidor Pump</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="168"/>
        <source>&amp;Save Details</source>
        <translation>&amp;Gardar Detalles</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="401"/>
        <source>If the browser doesn&apos;t open automatically, copy this address manually</source>
        <translation>Se o navegador non se abre automáticamente, copie este enderezo manualmente</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="25"/>
        <source>Account Configuration</source>
        <translation>Configuración da Conta</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="50"/>
        <source>First, enter your Webfinger ID, your pump.io address.</source>
        <translation>Primeiro, introduza o seu ID Webfinger, o seu enderezo pump.io.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="53"/>
        <source>Your address looks like username@pumpserver.org, and you can find it in your profile, in the web interface.</source>
        <translation>O seu enderezo é algo como nomedeusuario@servidor.org, e pódeo atopar no seu perfil, no interfaz web.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="58"/>
        <source>If your profile is at https://pump.example/yourname, then your address is yourname@pump.example</source>
        <translation>Se o seu perfil está en https://pump.exemplo/seunome, entón o seu enderezo é seunome@pump.exemplo</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="62"/>
        <source>If you don&apos;t have an account yet, you can sign up for one at %1. This link will take you to a random public server.</source>
        <comment>1=link to website</comment>
        <translation>Se aínda non ten unha conta, pode rexistrar unha e %1. Esta ligazón levaralle aleatoriamente a un servidor público.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="70"/>
        <source>If you need help: %1</source>
        <translation>Se necesita axuda: %1</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="73"/>
        <source>Pump.io User Guide</source>
        <translation>Guía de Usuario Pump.io</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="88"/>
        <source>Your address, like username@pumpserver.org</source>
        <translation>O seu enderezo, tal como nomedeusuario@servidorpump.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="98"/>
        <source>After clicking this button, a web browser will open, requesting authorization for Dianara</source>
        <translation>Despois de premer neste botón, abrirase un navegador, solicitando autorización para Dianara</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="109"/>
        <source>Once you have authorized Dianara from your Pump server web interface, you&apos;ll receive a code called VERIFIER.
Copy it and paste it into the field below.</source>
        <comment>Don&apos;t translate the VERIFIER word!</comment>
        <translation>Unha vez teña autorizado a Dianara dene a interfaz web do seu servidor Pump, recibirá un código chamdo VERIFIER.
Copie e pégueo no campo de embaixo.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="141"/>
        <source>&amp;Authorize Application</source>
        <translation>&amp;Autorizar Aplicación</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="176"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="188"/>
        <source>Your account is properly configured.</source>
        <translation>A súa conta está configurada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="192"/>
        <source>Press Unlock if you wish to configure a different account.</source>
        <translation>Prema Desbloquear se desexa configurar unha conta diferente.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="200"/>
        <source>&amp;Unlock</source>
        <translation>&amp;Desbloquear</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="327"/>
        <source>A web browser will start now, where you can get the verifier code</source>
        <translation>Agora arrancará un navegador, onde poderá obter o código de verificación</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="336"/>
        <source>Your Pump address is invalid</source>
        <translation>O seu enderezo Pump non é válido</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="355"/>
        <source>Verifier code is empty</source>
        <translation>O código de verificación está baleiro</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="382"/>
        <source>Dianara is authorized to access your data</source>
        <translation>Dianara está autorizada para acceder aos seus datos</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="410"/>
        <source>Unable to open web browser!</source>
        <translation>No se puido abrir o navegador!</translation>
    </message>
</context>
<context>
    <name>AudienceSelector</name>
    <message>
        <location filename="../src/audienceselector.cpp" line="33"/>
        <source>&apos;To&apos; List</source>
        <translation>Lista &apos;Para&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="37"/>
        <source>&apos;Cc&apos; List</source>
        <translation>Lista &apos;Cc&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="58"/>
        <source>&amp;Add to Selected</source>
        <translation>&amp;Engadir á Selección</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="73"/>
        <source>All Contacts</source>
        <translation>Tódolos Contactos</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="79"/>
        <source>Select people from the list on the left.
You can drag them with the mouse, click or double-click on them, or select them and use the button below.</source>
        <comment>ON THE LEFT should change to ON THE RIGHT in RTL languages</comment>
        <translation>Seleccione xente da lista da esquerda.
Mode arrastralos con rato, facer click ou doble-click neles, ou seleccionalos e usar o botón de embaixo.</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="96"/>
        <source>Clear &amp;List</source>
        <translation>Limpar &amp;Lista</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="120"/>
        <source>&amp;Done</source>
        <translation>&amp;Feito</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="127"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="167"/>
        <location filename="../src/audienceselector.cpp" line="385"/>
        <source>Public</source>
        <translation>Público</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="172"/>
        <location filename="../src/audienceselector.cpp" line="391"/>
        <source>Followers</source>
        <translation>Seguidores</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="177"/>
        <source>Lists</source>
        <translation>Listas</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="187"/>
        <source>People...</source>
        <translation>Xente...</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="108"/>
        <source>Selected People</source>
        <translation>Persoas Seleccionadas</translation>
    </message>
</context>
<context>
    <name>AvatarButton</name>
    <message>
        <location filename="../src/avatarbutton.cpp" line="143"/>
        <source>Open %1&apos;s profile in web browser</source>
        <translation>Abrir o perfil de %1 no navegador</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="139"/>
        <source>Open your profile in web browser</source>
        <translation>Abrir o seu perfil no navegador</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="165"/>
        <source>Send message to %1</source>
        <translation>Enviar mensaxe a %1</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="174"/>
        <source>Browse messages</source>
        <translation>Revisar mensaxes</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="230"/>
        <source>Stop following</source>
        <translation>Deixar de seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="241"/>
        <source>Follow</source>
        <translation>Seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="287"/>
        <source>Stop following?</source>
        <translation>Deixar de seguir?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="288"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Esta vostede seguro de deixar de seguir a %1?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="291"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Sí, deixar de seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="292"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
</context>
<context>
    <name>BannerNotification</name>
    <message>
        <location filename="../src/bannernotification.cpp" line="38"/>
        <source>Timelines were not automatically updated to avoid interruptions.</source>
        <translation>As liñas temporais non se actualizaron automáticamente para evitar interrupcións.</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="47"/>
        <source>This happens when it is time to autoupdate the timelines, but you are not at the top of the first page, to avoid interruptions while you read</source>
        <translation>Isto ocorre cando é o momento de auto actualizar as liñas temporais, pero non está enrriba de todo na primeira páxina, para evitar interrupcións mentres vostede le</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="55"/>
        <source>Update now</source>
        <translation>Actualizar agora</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="65"/>
        <source>Hide this message</source>
        <translation>Ocultar esta mensaxe</translation>
    </message>
</context>
<context>
    <name>CharacterPicker</name>
    <message>
        <location filename="../src/characterpicker.cpp" line="26"/>
        <source>Select a symbol</source>
        <translation>Escolla un símbolo</translation>
    </message>
    <message>
        <location filename="../src/characterpicker.cpp" line="85"/>
        <source>Insert in big size</source>
        <translation>Inserir en tamaño grande</translation>
    </message>
    <message>
        <location filename="../src/characterpicker.cpp" line="129"/>
        <source>&amp;Insert Symbol</source>
        <translation>&amp;Inserir Símbolo</translation>
    </message>
    <message>
        <location filename="../src/characterpicker.cpp" line="136"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../src/colorpicker.cpp" line="35"/>
        <source>Change...</source>
        <translation>Cambiar...</translation>
    </message>
    <message>
        <location filename="../src/colorpicker.cpp" line="114"/>
        <source>Choose a color</source>
        <translation>Elixir unha cor</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../src/comment.cpp" line="251"/>
        <source>Posted on %1</source>
        <translation>Publicado o %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="256"/>
        <source>Modified on %1</source>
        <translation>Modificado o %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="114"/>
        <source>Like or unlike this comment</source>
        <translation>Gostar ou deixar de gostar este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="121"/>
        <source>Quote</source>
        <comment>This is a verb, infinitive</comment>
        <translation>Citar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="129"/>
        <source>Reply quoting this comment</source>
        <translation>Respostar citar este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="136"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="142"/>
        <source>Modify this comment</source>
        <translation>Modificar este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="148"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="154"/>
        <source>Erase this comment</source>
        <translation>Borrar este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="287"/>
        <source>Unlike</source>
        <translation>Deixar de gostar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="291"/>
        <source>Like</source>
        <translation>Gostar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="311"/>
        <source>%1 like this comment</source>
        <comment>Plural: %1=list of people like John, Jane, Smith</comment>
        <translation>%1 lles gosta este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="305"/>
        <source>%1 likes this comment</source>
        <comment>Singular: %1=name of just 1 person</comment>
        <translation>%1 góstalle este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="547"/>
        <source>WARNING: Delete comment?</source>
        <translation>ATENCIÓN: Borrar comentario?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="548"/>
        <source>Are you sure you want to delete this comment?</source>
        <translation>Está vostede seguro de querer borrar este comentario?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, borralo</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
</context>
<context>
    <name>CommenterBlock</name>
    <message>
        <location filename="../src/commenterblock.cpp" line="131"/>
        <source>You can press Control+Enter to send the comment with the keyboard</source>
        <translation>Pode premer Control+Enter para enviar o comentario c teclado</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="42"/>
        <source>Reload comments</source>
        <translation>Recargar comentarios</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="127"/>
        <location filename="../src/commenterblock.cpp" line="528"/>
        <source>Comment</source>
        <comment>Infinitive verb</comment>
        <translation>Comentar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="138"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="141"/>
        <source>Press ESC to cancel the comment if there is no text</source>
        <translation>Prema ESC para cancelar o comentario se non ten texto</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="375"/>
        <source>Check for comments</source>
        <translation>Comprobar se hai comentarios</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="379"/>
        <source>Show all %1 comments</source>
        <translation>Amosar todos os %1 comentarios</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="405"/>
        <source>Comments are not available</source>
        <translation>Comentarios non dispoñibles</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="579"/>
        <source>Error: Already composing</source>
        <translation>Erro: Xa está redactando</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="580"/>
        <source>You can&apos;t edit a comment at this time, because another comment is already being composed.</source>
        <translation>Non pode editar un comentario agora porque xa está redactando outro comentario.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="595"/>
        <source>Editing comment</source>
        <translation>Editando comentario</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="604"/>
        <source>Loading comments...</source>
        <translation>Cargando comentarios...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="671"/>
        <source>Posting comment failed.

Try again.</source>
        <translation>Falllou o comentario da publicación.

Ténteo outra vez.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="688"/>
        <source>An error occurred</source>
        <translation>Ocurríu un erro</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="712"/>
        <source>Sending comment...</source>
        <translation>Enviando comentario...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="717"/>
        <source>Updating comment...</source>
        <translation>Actualizando comentario...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="726"/>
        <source>Comment is empty.</source>
        <translation>O comentario está baleiro.</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../src/composer.cpp" line="198"/>
        <source>Type a message here to post it</source>
        <translation>Escriba unha mensaxe eiquí para publicala</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="37"/>
        <source>Click here or press Control+N to post a note...</source>
        <translation>Prema eiquí o pulse Ctrl+N para escribir unha nota...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="41"/>
        <source>Symbols</source>
        <translation>Sïmbolos</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="59"/>
        <source>Formatting</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="60"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="64"/>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="69"/>
        <source>Italic</source>
        <translation>Itálica</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="74"/>
        <source>Underline</source>
        <translation>Suliñar</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="79"/>
        <source>Strikethrough</source>
        <translation>Tachado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="86"/>
        <source>Header</source>
        <translation>Cabeceiro</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="91"/>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="95"/>
        <source>Table</source>
        <translation>Táboa</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="99"/>
        <source>Preformatted block</source>
        <translation>Bloque preformateado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="103"/>
        <source>Quote block</source>
        <translation>Bloque de Cita</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="112"/>
        <source>Configure spell checking...</source>
        <translation>Configurar comprobación ortográfica...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="120"/>
        <source>Make a link</source>
        <translation>Facer unha ligazón</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="125"/>
        <source>Insert an image from a web site</source>
        <translation>Inserir unha imaxe dende un sitio web</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="130"/>
        <source>Insert line</source>
        <translation>Inserir liña</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="142"/>
        <source>&amp;Format</source>
        <comment>Button for text formatting and related options</comment>
        <translation>&amp;Formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="147"/>
        <source>Text Formatting Options</source>
        <translation>Opcións de Formateo de Texto</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="154"/>
        <source>Insert symbols</source>
        <translation>Inserir símbolos</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="162"/>
        <source>Paste Text Without Formatting</source>
        <translation>Pegar Texto Sen Formatear</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="204"/>
        <source>Type a comment here</source>
        <translation>Teclee un comentario eiquí</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="371"/>
        <source>You can attach only one file.</source>
        <translation>Só pode mandar un ficheiro.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="379"/>
        <source>You cannot drop folders here, only a single file.</source>
        <translation>Vostede non pode arrastrar cartafoles aquí; só un ficheiro.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="529"/>
        <source>Insert as image?</source>
        <translation>Inserir como imaxen?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="530"/>
        <source>The link you are pasting seems to point to an image.</source>
        <translation>A ligazón que está pegando semella apuntar a unha imaxe.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="533"/>
        <source>Insert as visible image</source>
        <translation>Inserir como imaxen visible</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="534"/>
        <source>Insert as link</source>
        <translation>Inserir como ligazón</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="765"/>
        <source>Table Size</source>
        <translation>Tamaño de Táboa</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="769"/>
        <source>How many rows (height)?</source>
        <translation>Cantas filas (altura)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="780"/>
        <source>How many columns (width)?</source>
        <translation>Cantas columnas (anchura)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="871"/>
        <source>Insert a link</source>
        <translation>Insire unha ligazón</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="872"/>
        <source>Type or paste a web address here.
You could also select some text first, to turn it into a link.</source>
        <translation>Teclee ou copie o enderezo web eiquí.
Tamén pode seleccionar antes algo de texto, para convertelo nunha ligazón.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="887"/>
        <source>Make a link from selected text</source>
        <translation>Facer unha ligazón a partires do texto seleccionado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="888"/>
        <source>Type or paste a web address here.
The selected text (%1) will be converted to a link.</source>
        <translation>Teclee ou copie un enderezo web eiquí.
O texto seleccionado (%1) converterase nunha ligazón.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="917"/>
        <source>Invalid link</source>
        <translation>Ligazón non válida</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="918"/>
        <source>The text you entered does not look like a link.</source>
        <translation>O texto que introducíu non semella unha ligazón.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="921"/>
        <source>It should start with one of these types:</source>
        <comment>It = the link, from previous sentence</comment>
        <translation>Esta debería comenzar cun destes tipos:</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="935"/>
        <source>&amp;Use it anyway</source>
        <translation>&amp;Usalo igualmente</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="936"/>
        <source>&amp;Enter it again</source>
        <translation>&amp;Introducilo outra vez</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="937"/>
        <source>&amp;Cancel link</source>
        <translation>&amp;Cancelar ligazón</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="970"/>
        <source>Insert an image from a URL</source>
        <translation>Inserir unha imaxe dende unha URL</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="971"/>
        <source>Type or paste the image address here.
The link must point to the image file directly.</source>
        <translation>Teclee ou pegue a dirección da imaxe eiquí.
A ligazón debe apuntar directamente ao ficheiro da imaxe.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="988"/>
        <source>Error: Invalid URL</source>
        <translation>Erro: URL non válida</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="989"/>
        <source>The address you entered (%1) is not valid.
Image addresses should begin with http:// or https://</source>
        <translation>A dirección que introducíu (%1) non é válida.
As direccións das imaxes deberían comenzar con http:// ou https://</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1104"/>
        <source>Yes, but saving a &amp;draft</source>
        <translatorcomment>Mantengo al atajo de teclado</translatorcomment>
        <translation>Si, mais gardan&amp;do un borrador</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1109"/>
        <source>Cancel message?</source>
        <translation>Cancelar mensaxe?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1110"/>
        <source>Are you sure you want to cancel this message?</source>
        <translation>Está seguro de querer cancelar esta mensaxe?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1112"/>
        <source>&amp;Yes, cancel it</source>
        <translation>&amp;Sí, cancelalo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1114"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="245"/>
        <source>minutes</source>
        <translation>minutos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="251"/>
        <source>Top</source>
        <translation>Enrriba</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="253"/>
        <source>Bottom</source>
        <translation>Abaixo de todo</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="32"/>
        <source>Program Configuration</source>
        <translation>Configuración do Programa</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="289"/>
        <source>Timeline &amp;update interval</source>
        <translation>Intervalo de act&amp;ualización da Liña Temporal</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="464"/>
        <source>&amp;Posts per page, main timeline</source>
        <translation>&amp;Mensaxes por páxina, liña temporal principal</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="407"/>
        <location filename="../src/configdialog.cpp" line="414"/>
        <source>posts</source>
        <comment>This goes after a number, like: 10 posts</comment>
        <translation>publicacións</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="466"/>
        <source>Posts per page, &amp;other timelines</source>
        <translation>Publicacións por páxina; &amp;outras liñas temporais</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="292"/>
        <source>&amp;Tabs position</source>
        <translation>Poisición das Pes&amp;tañas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="294"/>
        <source>&amp;Movable tabs</source>
        <translation>Táboas &amp;Movibeis</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="566"/>
        <source>Public posts as &amp;default</source>
        <translation>Publicacións públicas por &amp;defecto</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="268"/>
        <source>Pro&amp;xy Settings</source>
        <translation>Axustes de Pro&amp;xy</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="297"/>
        <source>Network configuration</source>
        <translation>Configuración de rede</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="275"/>
        <source>Set Up F&amp;ilters</source>
        <translation>Configurar F&amp;iltros</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="299"/>
        <source>Filtering rules</source>
        <translation>Reglas de filtrado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="430"/>
        <source>Highlighted activities, except mine</source>
        <translation>Actividades destacadas, excepto as miñas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="431"/>
        <source>Any highlighted activity</source>
        <translation>Calquera actividade destacada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="432"/>
        <source>Always</source>
        <translation>Sempre</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="433"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="319"/>
        <source>Comments</source>
        <translation>Comentarios</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="255"/>
        <source>Left side</source>
        <comment>tabs on left side/west; RTL not affected</comment>
        <translation>Lado esquerdo</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="258"/>
        <source>Right side</source>
        <comment>tabs on right side/east; RTL not affected</comment>
        <translation>Lado dereito</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="347"/>
        <source>You are among the recipients of the activity, such as a comment addressed to you.</source>
        <translation>Vostede está entre os destinatarios da actividade, tal como un comentario dirixido a vostede.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="351"/>
        <source>Used also when highlighting posts addressed to you in the timelines.</source>
        <translation>Usado tamén cando se resaltan publicacións dirixidas a vostede nas liñas temporais.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="356"/>
        <source>The activity is in reply to something done by you, such as a comment posted in reply to one of your notes.</source>
        <translation>A actividade é en resposta a algo feito por vostede, tal como un comentario publicado en resposta a unha das súas notas.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="362"/>
        <source>You are the object of the activity, such as someone adding you to a list.</source>
        <translation>Vostede é o obxecto da actividade, como cando alguén lle engade a unha lista.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="367"/>
        <source>The activity is related to one of your objects, such as someone liking one of your posts.</source>
        <translation>A actividade está relacionada cun dos seus obxectos, como cando alguén lle gosta unha das súas publicacións.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="371"/>
        <source>Used also when highlighting your own posts in the timelines.</source>
        <translation>Usado tamén cando se resaltan as súas propias publicacións nas liñas temmporais.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="376"/>
        <source>Item highlighted due to filtering rules.</source>
        <translation>Elemento destacado debido ás regras de filtrado.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="381"/>
        <source>Item is new.</source>
        <translation>O elemento é novo.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="475"/>
        <source>Show snippets in minor feeds</source>
        <translation>Amosar anacos en liñas temporais menores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="468"/>
        <source>Show information for deleted posts</source>
        <translation>Amosar información para publicacións borradas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="451"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="452"/>
        <source>Before avatar</source>
        <translation>Antes do avatar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="453"/>
        <source>Before avatar, subtle</source>
        <translation>Antes do avatar, sutil</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="454"/>
        <source>After avatar</source>
        <translation>Despois do avatar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="455"/>
        <source>After avatar, subtle</source>
        <translation>Despois do avatar, sutil</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="470"/>
        <source>Hide duplicated posts</source>
        <translation>Ocultar publicacións duplicadas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="472"/>
        <source>Jump to new posts line on update</source>
        <translation>Saltar á liña de novas publicacións ao actualizar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="479"/>
        <source>Snippet limit when highlighted</source>
        <translation>Límite de anaco cando sexa destacado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="481"/>
        <source>Minor feed avatar sizes</source>
        <translation>Tamaños dos avatares nas liñas temporais menores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="483"/>
        <source>Show activity icons</source>
        <translation>Amosar iconas de actividade</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="527"/>
        <source>Avatar size</source>
        <translation>Tamaño do avatar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="529"/>
        <source>Avatar size in comments</source>
        <translation>Tamaño do avatar nos comentarios</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="531"/>
        <source>Show extended share information</source>
        <translation>Amosar información de compartición extendida</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="533"/>
        <source>Show extra information</source>
        <translation>Amosar información adicional</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="535"/>
        <source>Highlight post author&apos;s comments</source>
        <translation>Destacar os comentarios do autor da publicación</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="537"/>
        <source>Highlight your own comments</source>
        <translation>Destacara os seus propios comentarios</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="539"/>
        <source>Ignore SSL errors in images</source>
        <translation>Ignorar erros SSL nas imaxes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="541"/>
        <source>Show full size images</source>
        <translation>Amosar imaxes en tamaño completo</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="571"/>
        <source>Show character counter</source>
        <translation>Amosar contador de caracteres</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="596"/>
        <source>Don&apos;t inform followers when following someone</source>
        <translation>Non informar aos seguidores cando sigo a alguén</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="599"/>
        <source>Don&apos;t inform followers when handling lists</source>
        <translation>Non informar aos seguidores cando manexo as listas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="619"/>
        <source>As system notifications</source>
        <translation>Como notificacións do sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="621"/>
        <source>Using own notifications</source>
        <translation>Usar notificacións propias</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="623"/>
        <source>Don&apos;t show notifications</source>
        <translation>Non amosar notificacións</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="637"/>
        <source>seconds</source>
        <comment>Next to a duration, in seconds</comment>
        <translation>segundos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="675"/>
        <source>Notification Style</source>
        <translation>Estilo de Notificación</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="679"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="681"/>
        <source>Persistent Notifications</source>
        <translation>Notificacións Persistentes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="683"/>
        <source>Also highlight taskbar entry</source>
        <translation>Suliñar tamén a entrada na barra de tarefas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="689"/>
        <source>Notify when receiving:</source>
        <translation>Notificar ao recibir:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="693"/>
        <source>New posts</source>
        <translation>Novas publicaións</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="695"/>
        <source>Highlighted posts</source>
        <translation>Publicacións destacadas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="697"/>
        <source>New activities in minor feed</source>
        <translation>Novas actividades en liña temporal secundaria</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="699"/>
        <source>Highlighted activities in minor feed</source>
        <translation>Actividades destacadas en liña temporal secundaria</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="701"/>
        <source>Important errors</source>
        <translation>Erros importantes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="716"/>
        <source>Default</source>
        <translation>Por defecto</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="717"/>
        <source>System iconset, if available</source>
        <translation>Conxunto de iconas do sistema, se o hai</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="718"/>
        <source>Show your current avatar</source>
        <translation>Amosar o seu avatar actual</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="719"/>
        <source>Custom icon</source>
        <translation>Icona personalizada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="753"/>
        <source>System Tray Icon &amp;Type</source>
        <translation>&amp;Tipo de Icona de Bandexa do Sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="726"/>
        <source>S&amp;elect...</source>
        <translation>S&amp;eleccionar...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="755"/>
        <source>Custom &amp;Icon</source>
        <translation>&amp;Icona Personalizada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="757"/>
        <source>Hide window on startup</source>
        <translation>Ocultar fiestra no arranque</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="125"/>
        <source>Timelines</source>
        <translation>Liñas temporais</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="129"/>
        <source>Posts</source>
        <translation>Publicacións</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="133"/>
        <source>Composer</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="137"/>
        <source>Privacy</source>
        <translation>Privacidade</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="141"/>
        <source>Notifications</source>
        <translation>Notificacións</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="145"/>
        <source>System Tray</source>
        <translation>Bandexa do Sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="814"/>
        <source>This is a system notification</source>
        <translation>Esta é unha notificación do sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="818"/>
        <source>System notifications are not available!</source>
        <translation>Non están dispoñibeis as notificacións do sistema!</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="820"/>
        <source>Own notifications will be used.</source>
        <translation>Empregaranse as notificacións propias.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="830"/>
        <source>This is a basic notification</source>
        <translation>Esta é una notificación básica</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1027"/>
        <source>Select custom icon</source>
        <translation>Elixir icona personalizada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="313"/>
        <source>Post Titles</source>
        <translation>Títulos das Publicacións</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="438"/>
        <source>characters</source>
        <comment>This is a suffix, after a number</comment>
        <translation>caracteres</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="477"/>
        <source>Snippet limit</source>
        <translation>Límite de anaco</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="316"/>
        <source>Post Contents</source>
        <translation>Contidos das Publicacións</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="322"/>
        <source>Minor Feeds</source>
        <translation>Liñas Temporais Secundarias</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="515"/>
        <source>Only for images inserted from web sites.</source>
        <translation>Só para imaxes inseridas dende sitios web.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="518"/>
        <source>Use with care.</source>
        <translation>Usar con coidado.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="568"/>
        <source>Use attachment filename as initial post title</source>
        <translation>Usar o nome do ficheiro adxunto como título inicial da publicación</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="602"/>
        <source>Inform only the author when liking things</source>
        <translation>Informar só ao autor cando lle gosten cousas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="113"/>
        <source>General Options</source>
        <translation>Opcións Xerais</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="117"/>
        <source>Fonts</source>
        <translation>Fontes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="121"/>
        <source>Colors</source>
        <translation>Cores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="175"/>
        <source>Dianara stores data in this folder:</source>
        <translation>Dianara garda os datos neste cartafol:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="186"/>
        <source>&amp;Save Configuration</source>
        <translation>&amp;Gardar Configuración</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="193"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1029"/>
        <source>Image files</source>
        <translation>Ficheiros de imaxen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1032"/>
        <source>All files</source>
        <translation>Tódolos ficheiros</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1052"/>
        <source>Invalid image</source>
        <translation>Imaxe non válida</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1053"/>
        <source>The selected image is not valid.</source>
        <translation>A imaxe seleccionada non é válida.</translation>
    </message>
</context>
<context>
    <name>ContactCard</name>
    <message>
        <location filename="../src/contactcard.cpp" line="71"/>
        <source>Hometown</source>
        <translation>Cidade</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="78"/>
        <source>Joined: %1</source>
        <translation>Unido: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="86"/>
        <source>Updated: %1</source>
        <translation>Actualizado: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="106"/>
        <source>Bio for %1</source>
        <comment>Abbreviation for Biography, but you can use the full word; %1=contact name</comment>
        <translation>Bio para %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="120"/>
        <source>This user doesn&apos;t have a biography</source>
        <translation>Este usuario non ten unha biografía</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="125"/>
        <source>No biography for %1</source>
        <comment>%1=contact name</comment>
        <translation>Non hai biografía para %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="154"/>
        <source>Open Profile in Web Browser</source>
        <translation>Abrir Perfil no Navegador</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="165"/>
        <source>Send Message</source>
        <translation>Enviar Mensaxe</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="173"/>
        <source>Browse Messages</source>
        <translation>Visualizar Mensaxes</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="197"/>
        <source>User Options</source>
        <translation>Opcións de Usuario</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="230"/>
        <source>Follow</source>
        <translation>Seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="242"/>
        <source>Stop Following</source>
        <translation>Deixar de Seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="306"/>
        <source>Stop following?</source>
        <translation>Deixar de seguir?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="307"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Está seguro de querer deixar de seguir a %1?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="310"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Sí, deixar de seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="311"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <location filename="../src/contactlist.cpp" line="36"/>
        <source>Type a partial name or ID to find a contact...</source>
        <translation>Escriba parte do nome ou o ID para atopar un contacto...</translation>
    </message>
    <message>
        <location filename="../src/contactlist.cpp" line="67"/>
        <source>F&amp;ull List</source>
        <translation>Lista C&amp;ompleta</translation>
    </message>
</context>
<context>
    <name>ContactManager</name>
    <message>
        <location filename="../src/contactmanager.cpp" line="54"/>
        <source>username@server.org or https://server.org/username</source>
        <translation>nomedeusuario@servidor.org ou https://servidor.org/nomedeusuario</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="58"/>
        <source>&amp;Enter address to follow:</source>
        <translation>&amp;Introduza dirección a seguir:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="79"/>
        <source>&amp;Follow</source>
        <translation>&amp;Seguir</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="129"/>
        <source>Reload Followers</source>
        <translation>Recargar Seguidores</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="134"/>
        <source>Reload Following</source>
        <translation>Recargar Seguindo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="140"/>
        <source>Export Followers</source>
        <translation>Exportar Seguidores</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="145"/>
        <source>Export Following</source>
        <translation>Export Seguindo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="151"/>
        <source>Reload Lists</source>
        <translation>Recargar Listas</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="179"/>
        <source>&amp;Neighbors</source>
        <translation>&amp;Veciños</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="208"/>
        <source>Follo&amp;wers</source>
        <translation>&amp;Seguidores</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="213"/>
        <source>Followin&amp;g</source>
        <translation>Se&amp;guindo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="218"/>
        <source>&amp;Lists</source>
        <translation>&amp;Listas</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="233"/>
        <source>Export list of &apos;following&apos; to a file</source>
        <translation>Exportar a un ficheiro a lista &apos;seguindo&apos;</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="234"/>
        <source>Export list of &apos;followers&apos; to a file</source>
        <translation>Exportar a lista de &apos;seguidores&apos; a un ficheiro</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="274"/>
        <source>Cannot export to this file:</source>
        <translation>No podo exportar a este ficheiro:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="278"/>
        <source>Please enter another file name, or choose a different folder.</source>
        <translation>Por favor introduza outro nome de ficheiro, ou elixa un caratafol diferente.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="519"/>
        <source>The server seems to be a Pump server, but the account does not exist.</source>
        <translation>Semella que o servidor é un servidor Pump, pero a conta non existe.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="524"/>
        <source>%1 doesn&apos;t seem to be a Pump server.</source>
        <comment>%1 is a hostname</comment>
        <translation>%1 non semella ser un servidor Pump.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="534"/>
        <source>Following this account at this time will probably not work.</source>
        <translation>Seguir esta conta neste intre probablemente non funcionará.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="543"/>
        <source>The %1 server seems unavailable.</source>
        <comment>%1 is a hostname</comment>
        <translation>O servidor %1 semella non estar dispoñible.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="551"/>
        <source>Unknown</source>
        <comment>Refers to server version</comment>
        <translation>Descoñecida</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="273"/>
        <location filename="../src/contactmanager.cpp" line="556"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="558"/>
        <source>The user address %1 does not exist, or the %2 server is down.</source>
        <translation>A dirección de usuario %1 non existe, ou o servidor %2 está caído.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="566"/>
        <source>Server version</source>
        <translation>Versión do servidor</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="570"/>
        <source>Check the address, and keep in mind that usernames are case-sensitive.</source>
        <translation>Comprobe a dirección, e teña en conta que os nomes de usuario distinguen MAIÚSCULAS e minúsculas.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="573"/>
        <source>Do you want to try following this address anyway?</source>
        <translation>Quere tentar seguir esta dirección igualmente?</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="575"/>
        <source>(not recommended)</source>
        <translation>(non recomendado)</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="578"/>
        <source>Yes, follow anyway</source>
        <translation>Sí, seguir igualmente</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="579"/>
        <source>No, cancel</source>
        <translation>Non, cancelar</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="598"/>
        <source>About to follow %1...</source>
        <translation>Vai seguir a %1...</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../src/downloadwidget.cpp" line="47"/>
        <source>Open</source>
        <comment>Verb, as in: Open the downloaded file</comment>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="58"/>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="61"/>
        <source>Save the attached file to your folders</source>
        <translation>Grabar o ficheiro adxunto nos seus cartafois</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="74"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="144"/>
        <source>Save File As...</source>
        <translation>Gardar Ficheiro Como...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="147"/>
        <source>All files</source>
        <translation>Tódolos ficheiros</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="198"/>
        <source>File not found!</source>
        <translation>Ficheiro non atopado!</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="209"/>
        <source>Abort download?</source>
        <translation>Abortar descarga?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="210"/>
        <source>Do you want to stop downloading the attached file?</source>
        <translation>Quere deixar de descargar o ficheiro adxunto?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="213"/>
        <source>&amp;Yes, stop</source>
        <translation>&amp;Sí, parar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="214"/>
        <source>&amp;No, continue</source>
        <translation>&amp;Non, continuar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="222"/>
        <source>Download aborted</source>
        <translation>Descarga abortada</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="240"/>
        <source>Download completed</source>
        <translation>Descarga completada</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="244"/>
        <source>Attachment downloaded successfully to %1</source>
        <comment>%1 = filename</comment>
        <translation>Adxunto descargado con éxito en %1</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="250"/>
        <source>Open the downloaded attachment with your system&apos;s default program for this type of file.</source>
        <translation>Abrir o adxunto descargado co programa por defecto do seu sistema para este tipo de ficheiro.</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="265"/>
        <source>Download failed</source>
        <translation>Descarga fallida</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="272"/>
        <source>Downloading attachment failed: %1</source>
        <comment>%1 = filename</comment>
        <translation>A descarga do adxunto fallou: %1</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="305"/>
        <source>Downloading %1 KiB...</source>
        <translation>Descargando %1 KiB...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="308"/>
        <source>%1 KiB downloaded</source>
        <translation>%1 KiB descargados</translation>
    </message>
</context>
<context>
    <name>DraftsManager</name>
    <message>
        <location filename="../src/draftsmanager.cpp" line="28"/>
        <source>Draft Manager</source>
        <translation>Xestor de Borradores</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="44"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="53"/>
        <source>Save</source>
        <translation>Gardar</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="62"/>
        <source>Manage drafts...</source>
        <translation>Xestionar borradores...</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="87"/>
        <source>&amp;Delete selected draft</source>
        <translation>Eliminar o borra&amp;dor elixido</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="94"/>
        <source>&amp;Close</source>
        <translation>&amp;Pechar</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="311"/>
        <source>Untitled draft</source>
        <translation>Borrador sen título</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="328"/>
        <source>Delete draft?</source>
        <translation>Eliminar borrador?</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="329"/>
        <source>Are you sure you want to delete this draft?</source>
        <translation>Está vostede seguro de querer eliminar este borrador?</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Si, bórrao</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
</context>
<context>
    <name>EmailChanger</name>
    <message>
        <location filename="../src/emailchanger.cpp" line="28"/>
        <source>Change E-mail Address</source>
        <translation>Cambiar Enderezo Electrónico</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="68"/>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="76"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="97"/>
        <source>E-mail Address:</source>
        <translation>Enderezo Electrónico:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="98"/>
        <source>Again:</source>
        <translation>Outra vez:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="99"/>
        <source>Your Password:</source>
        <translation>O seu Contrasinal:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="150"/>
        <source>E-mail addresses don&apos;t match!</source>
        <translation>Os enderezos electrónicos non coinciden!</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="159"/>
        <source>Password is empty!</source>
        <translation>O contrasinal está baleiro!</translation>
    </message>
</context>
<context>
    <name>FDNotifications</name>
    <message>
        <location filename="../src/notifications.cpp" line="209"/>
        <source>Show</source>
        <translation>Amosar</translation>
    </message>
</context>
<context>
    <name>FilterEditor</name>
    <message>
        <location filename="../src/filtereditor.cpp" line="28"/>
        <source>Filter Editor</source>
        <translation>Filtrar Editor</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="44"/>
        <source>%1 if %2 contains: %3</source>
        <comment>This explains a filter rule, like: Hide if Author ID contains JohnDoe</comment>
        <translation>%1 se %2 contén: %3</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="51"/>
        <source>Here you can set some rules for hiding or highlighting stuff. You can filter by content, author or application.

For instance, you can filter out messages posted by the application Open Farm Game, or which contain the word NSFW in the message. You could also highlight messages that contain your name.</source>
        <translation>Eiquí pode crear algunhas regras para ocultar ou remarcar cousas. Pode filtrar por contido autor ou aplicación.

Por exemplo, pode filtrar as mensaxes publicadas pola aplicación Open Farm Game, ou as que conteñan a palabra NSFW na mensaxe. Tamén podería resaltar as mensaxes que conteñan o seu nome.</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="69"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="72"/>
        <source>Highlight</source>
        <translation>Destacado</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="78"/>
        <source>Post Contents</source>
        <translation>Contidos da Publicación</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="81"/>
        <source>Author ID</source>
        <translation>ID do Autor</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="84"/>
        <source>Application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="87"/>
        <source>Activity Description</source>
        <translation>Descripción da Actividade</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="90"/>
        <source>Keywords...</source>
        <translation>Palabras clave...</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="95"/>
        <source>&amp;Add Filter</source>
        <translation>&amp;Engadir Filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="108"/>
        <source>Filters in use</source>
        <translation>Filtros en uso</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="114"/>
        <source>&amp;Remove Selected Filter</source>
        <translation>&amp;Eliminar Filtro Seleccionado</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="125"/>
        <source>&amp;Save Filters</source>
        <translation>&amp;Gardar Filtros</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="132"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="153"/>
        <source>if</source>
        <translation>se</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="157"/>
        <source>contains</source>
        <translation>contén</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="163"/>
        <source>&amp;New Filter</source>
        <translation>&amp;Novo Filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="170"/>
        <source>C&amp;urrent Filters</source>
        <translation>Filtros Ac&amp;tuais</translation>
    </message>
</context>
<context>
    <name>FilterMatchesWidget</name>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="37"/>
        <source>Content</source>
        <comment>The contents of the post matched</comment>
        <translation>Contidos</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="45"/>
        <source>Author</source>
        <translation>Autor/a</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="52"/>
        <source>App</source>
        <comment>Application, short if possible</comment>
        <translation>App</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="60"/>
        <source>Description</source>
        <translation>Descrición</translation>
    </message>
</context>
<context>
    <name>FirstRunWizard</name>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="30"/>
        <source>Welcome Wizard</source>
        <translation>Asistente de Benvida</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="41"/>
        <source>Welcome to Dianara!</source>
        <translation>Benvido/a a Dianara!</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="44"/>
        <source>This wizard will help you get started.</source>
        <translation>O asistente axudaralle a comezar.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="47"/>
        <source>You can access this window again at any time from the Help menu.</source>
        <translation>Pode volver a acceder a esta ventá en calquera momento dende o menú Axuda.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="50"/>
        <source>The first step is setting up your account, by using the following button:</source>
        <translation>O primeiro paso é configurar a súa conta, usando o seguinte botón:</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="61"/>
        <source>Configure your &amp;account</source>
        <translation>Configurar a súa cont&amp;a</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="69"/>
        <source>Once you have configured your account, it&apos;s recommended that you edit your profile and add an avatar and some other information, if you haven&apos;t done so already.</source>
        <translation>Unha vez que teña configurada a súa conta, é recomendable que edite o seu perfil e engada un avatar e algunha outra información, se aínda non o fixo.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="80"/>
        <source>&amp;Edit your profile</source>
        <translation>&amp;Editar o seu perfil</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="89"/>
        <source>By default, Dianara will post only to your followers, but it&apos;s recommended that you post to Public, at least sometimes.</source>
        <translation>Por defecto, Dianara publicará só para os seus seguidores, pero recoméndase que publique de xeito Público, alomenos algunhas veces.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="97"/>
        <source>Post to &amp;Public by default</source>
        <translation>Publicar en modo &amp;Público por defecto</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="106"/>
        <source>Open general program &amp;help window</source>
        <translation>Abrir fiestra de &amp;axuda xeral do programa</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="114"/>
        <source>&amp;Show this again next time Dianara starts</source>
        <translation>&amp;Amosar isto outra vez cando volva a arrancar Dianara</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="120"/>
        <source>&amp;Close</source>
        <translation>&amp;Pechar</translation>
    </message>
</context>
<context>
    <name>FontPicker</name>
    <message>
        <location filename="../src/fontpicker.cpp" line="50"/>
        <source>Change...</source>
        <translation>Cambiar...</translation>
    </message>
    <message>
        <location filename="../src/fontpicker.cpp" line="97"/>
        <source>Choose a font</source>
        <translation>Elixa unha fonte</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>Basic Help</source>
        <translation>Axuda Básica</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="39"/>
        <source>Getting started</source>
        <translation>Comezando</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="73"/>
        <source>The first time you start Dianara, you should see the Account Configuration dialog. There, enter your Pump.io address as name@server and press the Get Verifier Code button.</source>
        <translation>A primeira vez que arranque Dianara, debería ver un diálogo de Configuración de Conta. Alí, introduza o seu enderezo Pump.io como nome@usuario e prema no botón Obter Código de Verificación.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="78"/>
        <source>Then, your usual web browser should load the authorization page in your Pump.io server. There, you&apos;ll have to copy the full VERIFIER code, and paste it into Dianara&apos;s second field. Then press Authorize Application, and once it&apos;s confirmed, press Save Details.</source>
        <translation>Despois, o seu navegador habitual debería cargar a páxina de autorización no seu servidor Pump.io. Ali, tera que copiar enteiro o código de VERIFICACIÓN, e pegalo no segundo campo de Dianara. Prema entón Autorizar Aplicación, e unha vez sexa confirmado, prema Gardar Detalles.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="84"/>
        <source>At this point, your profile, contact lists and timelines will be loaded.</source>
        <translation>Neste punto, se cargarán o seu perfil, lista de contactos e liñas temporais.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="87"/>
        <source>You should take a look at the Program Configuration window, under the Settings - Configure Dianara menu. There are several interesting options there.</source>
        <translation>Debería botarlle un vistazo á fiestra de Configuración do Programa, no menú Axustes - Configurar Dianara. Hai varias opcións interesantes ahí.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Settings</source>
        <translation>Axustes</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="110"/>
        <source>You can configure several things to your liking in the settings, like the time interval between timeline updates, how many posts per page you want, highlight colors, notifications or how the system tray icon looks.</source>
        <translation>Pode configurar varias cousas ao seu xeito nos axustes, como o intervalo de tempo entre actualizacións das liñas temporais, cántas publicacións por páxina quere, cores de suliñado, notificiacións ou a apariencia da icona da bandexa do sistema.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Timelines</source>
        <translation>Liñas temporais</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation>Contidos</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="91"/>
        <source>Keep in mind that there are a lot of places in Dianara where you can get more information by hovering over some text or button with your mouse, and waiting for the tooltip to appear.</source>
        <translation>Teña en conta que hai un montón de lugares en Dianara onde pode obter máis información movendo o ratón sobre algún texto ou botón, e esperando a que apareza a descripción emerxente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="96"/>
        <source>If you&apos;re new to Pump.io, take a look at this guide:</source>
        <translation>Se vostede é novo en Pump.io, bótelle un vistazo a esta guía:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="115"/>
        <source>Here, you can also activate the option to always publish your posts as Public by default. You can always change that at the moment of posting.</source>
        <translation>Eiquí, tamén pode activar a opción de facer sempre públicas por defecto as súas publicacións. Pode cambialo sempre no momento de publicar.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="131"/>
        <source>The main timeline, where you&apos;ll see all the stuff posted or shared by the people you follow.</source>
        <translation>A liña temporal principal, onde verá todo o publicado ou compartido pola xente que sigue.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="134"/>
        <source>Messages timeline, where you&apos;ll see messages sent to you specifically. These messages might have been sent to other people too.</source>
        <translation>Liña temporal de mensaxes, onde verá as mensaxes que lle enviaron específicamente a vostede. Estas mensaxes tamen poden ter sido enviadas a outras persoas.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="138"/>
        <source>Activity timeline, where you&apos;ll see your own posts, or posts shared by you.</source>
        <translation>Liña temporal de actividade, onde verá as súas propias publicacións, ous as compartidas por vostede.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="141"/>
        <source>Favorites timeline, where you&apos;ll see the posts and comments you&apos;ve liked. This can be used as a bookmark system.</source>
        <translation>Liña temporal de favoritos, onde verá as pubicacións e comentarios que lle gustaron. Isto pode ser empregado como un sistema de marcadores.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="158"/>
        <source>These activities might have a &apos;+&apos; button in them. Press it to open the post they&apos;re referencing. Also, as in many other places, you can hover with your mouse to see relevant information in the tooltip.</source>
        <translation>Estas actividades poderían ter un botón &apos;+&apos; nelas. Prema nel para abrir a publicación á que fan referencia. Asemade, como en moitos outros sitios, pode pasar o ratón por riba para ver información relevante na información emerxente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Posting</source>
        <translation>Publicando</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="163"/>
        <source>New messages appear highlighted in a different color. You can mark them as read just by clicking on any empty parts of the message.</source>
        <translation>As novas mensaxes aparecen resaltadas nunha cor diferente. Pode marcalas como leídas premendo simplemente en calquera parte baleira da mensaxe.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="176"/>
        <source>You can post notes by clicking in the text field at the top of the window or by pressing Control+N. Setting a title for your post is optional, but highly recommended, as it will help to better identify references to your post in the minor feed, e-mail notifications, etc.</source>
        <translation>Pode publicar notas premendo no campo de texto na parte superior da fiestra ou premendo Control+N. Poñerlle un título ás súas publicacións é opcional, mais altamente recomendable, xa que axudará a identificar mellor as referencias á súa publicación na liña temporal secundaria, notificacións por correo electrónico, etc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>It is possible to attach images, audio, video, and general files, like PDF documents, to your post.</source>
        <translation>É posible adxuntar imaxes, audio, vídeo, e ficheiros xenéricos, como documentos PDF, á súa publicación.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="192"/>
        <source>You can use the Format button to add formatting to your text, like bold or italics. Some of these options require text to be selected before they are used.</source>
        <translation>Pode usar o botón Formato para engadir formato ao seu texto, tal como negrita ou itálica. Algunhas destas opcións requiren que haxa texto seleccionado antes de empregalas.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="199"/>
        <source>If you add a specific person to the &apos;To&apos; list, they will receive your message in their direct messages tab.</source>
        <translation>Se engades unha persona en concreto á lista &apos;Para&apos;, esta recibirá a túa mensaxe na súa pestana de mensaxes directas.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="206"/>
        <source>Choose one with the arrow keys and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation>Elixha unha coas flechas e prema Enter para completar o nome. Isto engadirá esa persona á lista de destinatarios.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="210"/>
        <source>You can create private messages by adding specific people to these lists, and unselecting the Followers or the Public options.</source>
        <translation>Pode crear mesaxes privadas engadindo xente concreta a estas listas, e desmarcando as opcións Seguidores ou Público.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Managing contacts</source>
        <translation>Xestionando contactos</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="223"/>
        <source>You can see the lists of people you follow, and who follow you from the Contacts tab.</source>
        <translation>Pode ver as listas de xente que segue, e quén lle segue na pestaña de Contactos.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="226"/>
        <source>There, you can also manage person lists, used mainly to send posts to specific groups of people.</source>
        <translation>Alí, tamén pode manexar listas de personas, usadas principalmente para enviar mensaxes a grupos específicos de xente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="237"/>
        <source>You can click on any avatars in the posts, the comments, and the Meanwhile column, and you will get a menu with several options, one of which is following or unfollowing that person.</source>
        <translation>Pode premer en calquer avatar nas publicacións, nos comentarios e na columna Mentras tanto, e sairalle un menú con varias opcións, unha das cales é seguir ou deixar de seguir a esa persona.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Keyboard controls</source>
        <translation>Controis do teclado</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="98"/>
        <source>Pump.io User Guide</source>
        <translation>Guía de Usuario Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="128"/>
        <source>There are seven timelines:</source>
        <translation>Hai sete liñas temporais:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="146"/>
        <source>The fifth timeline is the minor timeline, also known as the Meanwhile. This is visible on the left side, though it can be hidden. Here you&apos;ll see minor activities done by everyone you follow, such as comment actions, liking posts or following people.</source>
        <comment>LEFT SIDE should change to RIGHT SIDE on RTL languages</comment>
        <translation>A quinta liña temporal é a liña temporal secundaria, tamén coñecida como Mentras tanto. Esta é visible no lado esquerdo, aínda que pode ocultarse. Eiquí verá actividades secundarias realizadas por calquera que vostede siga, tal como accións sobre comentarios, gostar publicacións ou seguir xente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="153"/>
        <source>The sixth and seventh timelines are also minor timelines, similar to the Meanwhile, but containing only activities directly addressed to you (Mentions) and activities done by you (Actions).</source>
        <translation>As liñas temporais sexta e sétima tamén son liñas temporais secundarias, similares ao Mentras tanto, pero conteñen só actividades dirixidas directamente a vostede (Mencións) e actividades feitas por vostede (Accións).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="196"/>
        <source>You can select who will see your post by using the To and Cc buttons.</source>
        <translation>Pode seleccionar quén verá a súa publicación usando os botóns Para e Cc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="202"/>
        <source>You can also type &apos;@&apos; and the first characters of the name of a contact to bring up a popup menu with matching choices.</source>
        <translation>Tamén pode teclear &apos;@&apos; e os primeiros caracteres do nome dun contacto para sacar un menú emerxente con opcións axeitadas.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="229"/>
        <source>There is a text field at the top, where you can directly enter addresses of new contacts to follow them.</source>
        <translation>Hai un campo de texto enriba de todo, onde pode introducir directamente direccións de novos contactos para seguirlles.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="242"/>
        <source>You can also send a direct message (initially private) to that contact from this menu.</source>
        <translation>Tamén pode enviar unha mensaxe directa (inicialmente privada) ao contacto dende este menú.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="245"/>
        <source>You can find a list with some Pump.io users and other information here:</source>
        <translation>Pode atopar unha lista con algúns usuarios Pump.io e outra información eiquí:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="249"/>
        <source>Users by language</source>
        <translation>Usuarios por idioma</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="251"/>
        <source>Followers of Pump.io Community account</source>
        <translation>Seguidores da conta Pump.io Community</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="263"/>
        <source>The most common actions found on the menus have keyboard shortcuts written next to them, like F5 or Control+N.</source>
        <translation>As accións máis comúns atopadas nos menús teñen atallos de teclado escritos ao seu carón, como F5 ou Control+N.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="267"/>
        <source>Besides that, you can use:</source>
        <translation>Aparte diso, pode usar:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="270"/>
        <source>Control+Up/Down/PgUp/PgDown/Home/End to move around the timeline.</source>
        <translation>Control+Up/Down/PgUp/PgDown/Home/End para moverese porla liña temporal.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="273"/>
        <source>Control+Left/Right to jump one page in the timeline.</source>
        <translation>Control+Esquerda/Dereita para saltar unha páxina na liña temporal.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="276"/>
        <source>Control+G to go to any page in the timeline directly.</source>
        <translation>Control+G para ir directamente a calquera páxina na liña temporal.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="279"/>
        <source>Control+1/2/3 to switch between the minor feeds.</source>
        <translation>Control+1/2/3 para cambiar entre liñas temporais secundarias.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="282"/>
        <source>Control+Enter to post, when you&apos;re done composing a note or a comment. If the note is empty, you can cancel it by pressing ESC.</source>
        <translation>Control+Enter para publicar, cando teña rematada a nota ou o comentario. Se a nota está baleira, pódea cancelar premendo ESC.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="316"/>
        <source>Dianara offers a D-Bus interface that allows some control from other applications.</source>
        <translation>Dianara ofrece un interfaz D-Bus que permite algún control dende outras aplicacións.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="319"/>
        <source>The interface is at %1, and you can access it with tools such as %2 or %3. It offers methods like %4 and %5.</source>
        <translation>O interfaz está en %1, e pode acceder a el con ferramentas tales como %2 ou %3. Ofrece métodos como %4 e %5.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation>Opcións en liña de comandos</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="233"/>
        <source>Under the &apos;Neighbors&apos; tab you&apos;ll see some resources to find people, and have the option to browse the latest registered users from your server directly.</source>
        <translation>Debaixo da pestaña &apos;Veciños&apos; verá algúns recursos para atopar xente, e ten a opción de visualizar directamente os últimos usuarios rexistrados no seu servidor.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="286"/>
        <source>While composing a note, press Enter to jump from the title to the message body. Also, pressing the Up arrow while you&apos;re at the start of the message, jumps back to the title.</source>
        <translation>Mentras compón unha nota, prema Enter para saltar dende o título ao corpo da mensaxe. Tamén, premendo a tecla Arriba mentras está no comenzo da mensaxe, lévalle de volta ao título.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="291"/>
        <source>Control+Enter to finish creating a list of recipients for a post, in the &apos;To&apos; or &apos;Cc&apos; lists.</source>
        <translation>Control+Enter para rematar de crear a lista de destinatarios para unha publicación, nas listas &apos;Para&apos; ou &apos;Cc&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="304"/>
        <source>You can use the --config parameter to run the program with a different configuration. This can be useful to use two or more different accounts. You can even run two instances of Dianara at the same time.</source>
        <translation>Pode usar o parámetro --config para executar o programa cunha configuración diferente. Isto poder ser útil para usar dúas ou máis contas diferentes. Incluso pode executar dúas instancias de Dianara ao mesmo tempo.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="309"/>
        <source>Use the --debug parameter to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Use o parámetro --debug para obter información extra na súa fiestra de terminal, acerca de qué está a facer o programa.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="312"/>
        <source>If your server does not support HTTPS, you can use the --nohttps parameter.</source>
        <translation>Se o seu servidor non soporta HTTPS, pode usar o parámetro --nohttps.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="326"/>
        <source>If you use an alternate configuration, with something like &apos;--config otherconf&apos;, then the interface will be at org.nongnu.dianara_otherconf.</source>
        <translation>Se usa unha configuración alternativa, con algo como &apos;--config outraconfig&apos;, entón a interface estará en org.nongnu.dianara_outraconfig.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="343"/>
        <source>&amp;Close</source>
        <translation>&amp;Pechar</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../src/imageviewer.cpp" line="31"/>
        <source>Untitled</source>
        <translation>Sen Título</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="33"/>
        <source>Image</source>
        <translation>Imaxe</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="67"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Gardar Como...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="80"/>
        <source>&amp;Restart</source>
        <comment>Restart animation</comment>
        <translation>&amp;Recomenzar</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="90"/>
        <source>Fit</source>
        <comment>As in: fit image to window</comment>
        <translation>Encaixar</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="137"/>
        <source>Rotate image to the left</source>
        <comment>RTL: This actually means LEFT, anticlockwise</comment>
        <translation>Rotar imáxe á esquerda</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="147"/>
        <source>Rotate image to the right</source>
        <comment>RTL: This actually means RIGHT, clockwise</comment>
        <translation>Rotar imaxe á dereita</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="160"/>
        <source>&amp;Close</source>
        <translation>&amp;Pechar</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="296"/>
        <source>Save Image...</source>
        <translation>Gardar Imaxen...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="336"/>
        <source>Close Viewer</source>
        <translation>Pechar Visor</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="377"/>
        <source>Downloading full image...</source>
        <translation>Descargando imaxe completa...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="523"/>
        <source>Error downloading image!</source>
        <translation>Erro descargando imaxe!</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="525"/>
        <source>Try again later.</source>
        <translation>Ténteo máis tarde.</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="536"/>
        <source>Save Image As...</source>
        <translation>Gardar Imaxen Como...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="539"/>
        <source>Image files</source>
        <translation>Ficheiros de imaxe</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="540"/>
        <source>All files</source>
        <translation>Tódolos ficheiros</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="551"/>
        <source>Error saving image</source>
        <translation>Erro gardando imaxe</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="552"/>
        <source>There was a problem while saving %1.

Filename should end in .jpg or .png extensions.</source>
        <translation>Houbo un problema gardando %1.

O nome do ficheiro debería rematar en extensións .jpg ou .png.</translation>
    </message>
</context>
<context>
    <name>ListsManager</name>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="39"/>
        <source>Members</source>
        <translation>Membros</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="52"/>
        <source>Add Mem&amp;ber</source>
        <translation>Engadir Mem&amp;bro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="60"/>
        <source>&amp;Remove Member</source>
        <translation>&amp;Eliminar Membro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="69"/>
        <source>&amp;Delete Selected List</source>
        <translation>Borrar Lista &amp;Seleccionada</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="77"/>
        <source>Add New &amp;List</source>
        <translation>Engadir Nova &amp;Lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="96"/>
        <source>Create L&amp;ist</source>
        <translation>Crear L&amp;ista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="104"/>
        <source>&amp;Add to List</source>
        <translation>&amp;Engadir a Lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="316"/>
        <source>Are you sure you want to delete %1?</source>
        <comment>1=Name of a person list</comment>
        <translation>Esta vostede seguro de querer borrar %1?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="451"/>
        <source>Remove person from list?</source>
        <translation>Quitar persona da lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="452"/>
        <source>Are you sure you want to remove %1 from the %2 list?</source>
        <comment>1=Name of a person, 2=name of a list</comment>
        <translation>Esta seguro de querer quitar a %1 da lista %2?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sí</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="80"/>
        <source>Type a name for the new list...</source>
        <translation>Escriba un nome para a nova lista...</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="89"/>
        <source>Type an optional description here</source>
        <translation>Escriba eiquí unha descripción opcional</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="315"/>
        <source>WARNING: Delete list?</source>
        <translation>ATENCIÓN: Borrar lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, borralo</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../src/logviewer.cpp" line="25"/>
        <source>Log</source>
        <translation>Rexistro</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="54"/>
        <source>Clear &amp;Log</source>
        <translation>Limpar &amp;Rexistro</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="61"/>
        <source>&amp;Close</source>
        <translation>Pe&amp;char</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="1130"/>
        <source>Status &amp;Bar</source>
        <translation>&amp;Barra de Estado</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2685"/>
        <source>&amp;Timeline</source>
        <translation>Liña &amp;Temporal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2687"/>
        <source>The main timeline</source>
        <translation>A liña temporal principal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2694"/>
        <source>&amp;Messages</source>
        <translation>&amp;Mensaxes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2696"/>
        <source>Messages sent explicitly to you</source>
        <translation>Mensaxes enviadas explícitamente a vostede</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2703"/>
        <source>&amp;Activity</source>
        <translation>&amp;Actividade</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2705"/>
        <source>Your own posts</source>
        <translation>As súas publicacións</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2714"/>
        <source>Your favorited posts</source>
        <translation>A sús publicacións favoritas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="429"/>
        <source>&amp;Contacts</source>
        <translation>&amp;Contactos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="741"/>
        <location filename="../src/mainwindow.cpp" line="1425"/>
        <source>Initializing...</source>
        <translation>Inicializando...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="819"/>
        <source>Your account is not configured yet.</source>
        <translation>A súa conta aínda non está configurada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Minor activities done by everyone, such as replying to posts</source>
        <translation>Actividades secundarias feitas por todo o mundo, tales como respostar ás publicacións</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="184"/>
        <source>Minor activities addressed to you</source>
        <translation>Actividades secundarias dirixidas a vostede</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="201"/>
        <source>Minor activities done by you</source>
        <translation>Actividades secundarias feitas por vostede</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="432"/>
        <source>The people you follow, the ones who follow you, and your person lists</source>
        <translation>A xente á que segue, os que lle seguen a vostede, e as súas listas de personas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="726"/>
        <source>Press F1 for help</source>
        <translation>Prema F1 para axuda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="825"/>
        <source>Click here to configure your account</source>
        <translation>Prema eiquí para configurar a súa conta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="962"/>
        <source>&amp;Session</source>
        <translation>&amp;Sesión</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1060"/>
        <source>Auto-update &amp;Timelines</source>
        <translation>Auto actualizar &amp;Liñas Temporais</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Mark All as Read</source>
        <translation>Marcar Todo Como Leído</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1081"/>
        <source>&amp;Post a Note</source>
        <translation>&amp;Publicar unha Nota</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1092"/>
        <source>&amp;Quit</source>
        <translation>&amp;Sair</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1103"/>
        <source>&amp;View</source>
        <translation>&amp;Vista</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1168"/>
        <source>Locked Panels and Toolbars</source>
        <translation>Paneis e Barras de Ferramentas bloqueadas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>Side Panel</source>
        <translation>Panel Lateral</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>&amp;Toolbar</source>
        <translation>Barra de &amp;Ferramentas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1143"/>
        <source>Full &amp;Screen</source>
        <translation>Pantalla C&amp;ompleta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>&amp;Log</source>
        <translation>&amp;Rexistro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1182"/>
        <source>S&amp;ettings</source>
        <translation>A&amp;xustes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1186"/>
        <source>Edit &amp;Profile</source>
        <translation>Editar &amp;Perfil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <source>&amp;Account</source>
        <translation>&amp;Conta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1231"/>
        <source>Basic &amp;Help</source>
        <translation>&amp;Axuda Básica</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1260"/>
        <source>Report a &amp;Bug</source>
        <translation>Informar dun &amp;Bug</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1272"/>
        <source>Pump.io User &amp;Guide</source>
        <translation>Guía de Usuario &amp;Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1280"/>
        <source>Some Pump.io &amp;Tips</source>
        <translation>Algunhas Pis&amp;tas sobre Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1288"/>
        <source>List of Some Pump.io &amp;Users</source>
        <translation>Lista dalgúns &amp;Usuarios Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1296"/>
        <source>Pump.io &amp;Network Status Website</source>
        <translation>Sitio Web de Estado da &amp;Rede Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1364"/>
        <source>Toolbar</source>
        <translation>Barra de ferramentas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1409"/>
        <source>Open the log viewer</source>
        <translation>Abrir o visor do rexistro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1432"/>
        <source>Auto-updating enabled</source>
        <translation>Auto-actualización habilitada</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1439"/>
        <source>Auto-updating disabled</source>
        <translation>Auto-actualización deshabilitada</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1991"/>
        <source>Proxy password required</source>
        <translation>Requírese contrasinal para o proxy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1992"/>
        <source>You have configured a proxy server with authentication, but the password is not set.</source>
        <translation>Configurou un servidor proxy con autentificación, pero non puxo o contrasinal.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1997"/>
        <source>Enter the password for your proxy server:</source>
        <translation>Introduza o contrasinal para o seu servidor proxy:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2140"/>
        <source>Starting automatic update of timelines, once every %1 minutes.</source>
        <translation>Comenzando actualización automática das liñas temporais, unha vez cada %1 minutos.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2147"/>
        <source>Stopping automatic update of timelines.</source>
        <translation>Parando actualización automática das liñas temporais.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2408"/>
        <source>Received %1 older posts in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of a timeline</comment>
        <translation>Recibida(s) %1 publicación(s) antiga(s) en %2.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2448"/>
        <source>1 highlighted</source>
        <comment>singular, refers to a post</comment>
        <translation>1 destacada</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2453"/>
        <source>%1 highlighted</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 destacada</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2443"/>
        <source>Direct messages</source>
        <translation>Mensaxes directas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2444"/>
        <source>By filters</source>
        <translation>Por filtros</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2485"/>
        <source>1 more pending to receive.</source>
        <comment>singular, one post</comment>
        <translation>1 máis pendente de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2490"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation>%1 máis pendente de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2502"/>
        <location filename="../src/mainwindow.cpp" line="2919"/>
        <source>Also:</source>
        <translation>Tamén:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2624"/>
        <source>Last update: %1</source>
        <translation>Última actualización: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2417"/>
        <location filename="../src/mainwindow.cpp" line="2855"/>
        <source>&apos;%1&apos; updated.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>&apos;%1&apos; actualizada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="767"/>
        <source>%1 started.</source>
        <comment>1=program name and version</comment>
        <translation>%1 arrancada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="770"/>
        <source>Running with Qt v%1 on %2.</source>
        <comment>1=Qt version, 2=OS name</comment>
        <translation>Executándose con Qt v%1 en %2.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="785"/>
        <source>Spell checking has been disabled because there are no Sonnet backends available!</source>
        <translatorcomment>Another option for &quot;backends&quot;: aplicacións</translatorcomment>
        <translation>A comprobación ortográfica non foi habilitada porque non hai motores Sonnet dispoñibles!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="790"/>
        <source>Spell checking available for %1 languages, using %2 by default.</source>
        <comment>%1 is the number of languages, %2 is a language code</comment>
        <translation>Comprobación ortográfica dispoñible para %1 idiomas;emprégase %2 por defecto.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1240"/>
        <source>Show Welcome Wizard</source>
        <translation>Amosar Asistente de Benvida</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2509"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 filtrado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2514"/>
        <source>%1 filtered out.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 filtrado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2527"/>
        <source>1 deleted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 borrado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2532"/>
        <source>%1 deleted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 borrado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2867"/>
        <source>There is 1 new activity.</source>
        <translation>Hai 1 nova actividade.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2871"/>
        <source>There are %1 new activities.</source>
        <translation>Hai %1 novas actividades.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2884"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to an activity</comment>
        <translation>1 destacada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2889"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to activities</comment>
        <translation>%1 destacadas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2923"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to one activity</comment>
        <translation>1 filtrada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2928"/>
        <source>%1 filtered out.</source>
        <comment>plural, several activities</comment>
        <translation>%1 filtradas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2997"/>
        <source>No new activities.</source>
        <translation>Non hai actividades novas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3073"/>
        <source>Error storing image!</source>
        <translation>Erro almacenando imaxe!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3075"/>
        <source>%1 bytes</source>
        <translation>%1 bytes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3138"/>
        <source>Marking everything as read...</source>
        <translation>Marcando todo como leído...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3486"/>
        <source>Dianara is Free Software, licensed under the GNU GPL license, and uses some Oxygen icons under LGPL license.</source>
        <translation>Dianara é Software Libre, licenciada baixo a licenza GNU-GPL, e usa algunas iconas Oxygen, baixo a licenza LGPL.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3503"/>
        <source>This version of Dianara was built with support for some extra features that use %1, also under the GNU LGPL.</source>
        <translation>Esta versión de Dianara programouse con soporte para algunhas características adicionais que empregan %1, tamén baixo a GNU LGPL.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3592"/>
        <source>Closing due to environment shutting down...</source>
        <translation>Pechando porque o entorno estase a apagar...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3615"/>
        <location filename="../src/mainwindow.cpp" line="3720"/>
        <source>Quit?</source>
        <translation>Sair?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3616"/>
        <source>You are composing a note or a comment.</source>
        <translation>Está escrebendo unha nota ou un comentario.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3618"/>
        <source>Do you really want to close Dianara?</source>
        <translation>Realmente quere pechar Dianara?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3619"/>
        <location filename="../src/mainwindow.cpp" line="3728"/>
        <source>&amp;Yes, close the program</source>
        <translation>&amp;Sí, pechar o programa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3619"/>
        <location filename="../src/mainwindow.cpp" line="3728"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3642"/>
        <source>Shutting down Dianara...</source>
        <translation>Pechando Dianara...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3721"/>
        <source>System tray icon is not available.</source>
        <translation>A icona da bandexa do sistema non está dispoñible.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3723"/>
        <source>Dianara cannot be hidden in the system tray.</source>
        <translation>Non se pode agochar Dianara na bandexa do sistema.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3726"/>
        <source>Do you want to close the program completely?</source>
        <translation>Quere pechar o programa completamente?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2423"/>
        <source>Timeline updated at %1.</source>
        <translation>Lilña temporal actualizada ás %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="965"/>
        <source>Update %1</source>
        <translation>Actualización %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2770"/>
        <source>Your Pump.io account is not configured</source>
        <translation>A súa conta Pump.io non está configurada</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3119"/>
        <source>Link to: %1</source>
        <translation>Enlazar a: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3469"/>
        <source>With Dianara you can see your timelines, create new posts, upload pictures and other media, interact with posts, manage your contacts and follow new people.</source>
        <translation>Con Dianara pode ver as súas liñas temporais, crear novas publicacións, subir imaxes ou outros medios, interactuar coas publicacións, xestionar os seus contactos e seguir nova xente.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3475"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Tradución ao galego por EVAnaRkISTO.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1215"/>
        <source>&amp;Configure Dianara</source>
        <translation>&amp;Configurar Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1206"/>
        <source>&amp;Filters and Highlighting</source>
        <translation>&amp;Filtros e Destacados</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1227"/>
        <source>&amp;Help</source>
        <translation>&amp;Axuda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1252"/>
        <source>Visit &amp;Website</source>
        <translation>Visitar Sitio &amp;Web</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1307"/>
        <source>About &amp;Dianara</source>
        <translation>Acerca de &amp;Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2086"/>
        <source>Your biography is empty</source>
        <translation>A súa biografía está baleira</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2117"/>
        <source>Click to edit your profile</source>
        <translation>Prema para editar o seu perfil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2593"/>
        <source>No new posts.</source>
        <translation>Non hai novas publicacións.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2677"/>
        <source>Total posts: %1</source>
        <translation>Publicacións totais: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2712"/>
        <source>Favor&amp;ites</source>
        <translation>Favor&amp;itos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2847"/>
        <source>Received %1 older activities in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation>Recibidas %1 actividades más antigas en %2.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2860"/>
        <source>Minor feed updated at %1.</source>
        <translation>Liña temporal secundaria actualizada ás %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2902"/>
        <source>1 more pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation>1 máis pendente de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2907"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation>%1 máis pendentes de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3533"/>
        <source>&amp;Hide Window</source>
        <translation>Agoc&amp;har Fiestra</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3544"/>
        <source>&amp;Show Window</source>
        <translation>Amo&amp;sar Fiestra</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2429"/>
        <source>There is 1 new post.</source>
        <translation>Hai 1 nova publicación.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2433"/>
        <source>There are %1 new posts.</source>
        <translation>Hai %1 novas publicacions.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3455"/>
        <source>About Dianara</source>
        <translation>Acerca de Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3465"/>
        <source>Dianara is a pump.io social networking client.</source>
        <translation>Dianara é un cliente para a rede social pump.io.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3482"/>
        <source>Thanks to all the testers, translators and packagers, who help make Dianara better!</source>
        <translation>Grazas a todos os probadores, tradutores e empaquetadores, que axudan a mellorar Dianara!</translation>
    </message>
</context>
<context>
    <name>MinorFeed</name>
    <message>
        <location filename="../src/minorfeed.cpp" line="70"/>
        <source>Older Activities</source>
        <translation>Actividades Máis Antigas</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="74"/>
        <source>Get previous minor activities</source>
        <translation>Obter actividades menores previas</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="102"/>
        <source>There are no activities to show yet.</source>
        <translation>Aínda non hai actividades que amosar.</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="452"/>
        <source>Get %1 newer</source>
        <comment>As in: Get 3 newer (activities)</comment>
        <translation>Obter %1 novos</translation>
    </message>
</context>
<context>
    <name>MinorFeedItem</name>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="69"/>
        <source>Using %1</source>
        <comment>Application used to generate this activity</comment>
        <translation>Usando %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="83"/>
        <source>To: %1</source>
        <comment>1=people to whom this activity was sent</comment>
        <translation>A: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="90"/>
        <source>Cc: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation>Cc: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="266"/>
        <source>Open referenced post</source>
        <translation>Abrir a mensaxe referenciada</translation>
    </message>
</context>
<context>
    <name>MiscHelpers</name>
    <message>
        <location filename="../src/mischelpers.cpp" line="315"/>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="671"/>
        <source>Error: Unable to launch browser</source>
        <translation>Erro: Non se puido arrancar o navegador</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="672"/>
        <source>The default system web browser could not be executed.</source>
        <translation>Non se puido executar o navegador por defecto do sistema.</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="675"/>
        <source>You might need to install the XDG utilities.</source>
        <translation>Pode necesitar instalar as utilidades XDG.</translation>
    </message>
</context>
<context>
    <name>PageSelector</name>
    <message>
        <location filename="../src/pageselector.cpp" line="26"/>
        <source>Jump to page</source>
        <translation>Saltar á páxina</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="35"/>
        <source>Page number:</source>
        <translation>Páxina número:</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="50"/>
        <source>&amp;First</source>
        <comment>As in: first page</comment>
        <translation>&amp;Primeira</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="57"/>
        <source>&amp;Last</source>
        <comment>As in: last page</comment>
        <translation>&amp;Derradeira</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="64"/>
        <source>Newer</source>
        <comment>As in: newer pages</comment>
        <translation>Máis novas</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="85"/>
        <source>Older</source>
        <comment>As in: older pages</comment>
        <translation>Máis antigas</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="96"/>
        <source>&amp;Go</source>
        <translation>&amp;Ir</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="105"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>PeopleWidget</name>
    <message>
        <location filename="../src/peoplewidget.cpp" line="40"/>
        <source>&amp;Search:</source>
        <translation>&amp;Procurar:</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="43"/>
        <source>Enter a name here to search for it</source>
        <translation>Introduza eiquí un nome para procuralo</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="102"/>
        <source>Add a contact to a list</source>
        <translation>Engadir á lista de contactos</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="114"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>Post</name>
    <message>
        <location filename="../src/post.cpp" line="205"/>
        <source>Via %1</source>
        <translation>A través de %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="259"/>
        <source>Shared on %1</source>
        <translation>Compartido en %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1155"/>
        <source>Loading image...</source>
        <translation>Cargando imaxe...</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1709"/>
        <source>Edited: %1</source>
        <translation>Editado: %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="942"/>
        <source>Posted on %1</source>
        <comment>1=Date</comment>
        <translation>Publicado o %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1000"/>
        <source>In</source>
        <translation>En</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="215"/>
        <location filename="../src/post.cpp" line="436"/>
        <source>To</source>
        <translation>Para</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="140"/>
        <source>Post</source>
        <comment>Noun, not verb</comment>
        <translation>Mensaxe</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="263"/>
        <location filename="../src/post.cpp" line="952"/>
        <source>Using %1</source>
        <comment>1=Program used for posting or sharing</comment>
        <translation>Usando %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="632"/>
        <source>If you select some text, it will be quoted.</source>
        <translation>Se selecciona algún texto, será citado.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="652"/>
        <source>Share</source>
        <translation>Compartir</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="660"/>
        <source>Unshare</source>
        <translation>Descompartir</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="662"/>
        <source>Unshare this post</source>
        <translation>Descompartir esta publicación</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="678"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="690"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="313"/>
        <source>Open post in web browser</source>
        <translation>Abrir publicación no navegador</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="37"/>
        <source>Click to download the attachment</source>
        <translation>Prema para descargar o adxunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="221"/>
        <location filename="../src/post.cpp" line="453"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="322"/>
        <source>Copy post link to clipboard</source>
        <translation>Copiar ligazón da publicación ao portapapeis</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="341"/>
        <source>Normalize text colors</source>
        <translation>Normalizar colores do texto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="359"/>
        <source>&amp;Close</source>
        <translation>&amp;Pechar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="944"/>
        <source>Type</source>
        <comment>As in: type of object</comment>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="959"/>
        <source>Modified on %1</source>
        <translation>Modificado o %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="522"/>
        <source>Parent</source>
        <comment>As in &apos;Open the parent post&apos;. Try to use the shortest word!</comment>
        <translation>Nai</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="531"/>
        <source>Open the parent post, to which this one replies</source>
        <translation>Abrir a publicación nai, á que ésta resposta</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="627"/>
        <source>Comment</source>
        <comment>verb, for the comment button</comment>
        <translation>Comentar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="630"/>
        <source>Reply to this post.</source>
        <translation>Respostar a esta publicación.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="654"/>
        <source>Share this post with your contacts</source>
        <translation>Compartir esta publicación cos seus contactos</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="681"/>
        <source>Modify this post</source>
        <translation>Modificar esta publicación</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="693"/>
        <source>Erase this post</source>
        <translation>Borrar esta publicación</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="767"/>
        <source>Join Group</source>
        <translation>Unirse a Grupo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="773"/>
        <source>%1 members in the group</source>
        <translation>%1 membros no grupo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1123"/>
        <source>Image is animated. Click on it to play.</source>
        <translation>A imaxe é animada. Prema nela para reproducila.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1133"/>
        <source>Size</source>
        <comment>Image size (resolution)</comment>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1150"/>
        <source>Couldn&apos;t load image!</source>
        <translation>Non puiden cargar a imaxen!</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1173"/>
        <source>Attached Audio</source>
        <translation>Audio Adxunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1183"/>
        <source>Attached Video</source>
        <translation>Vídeo Adxunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1217"/>
        <source>Attached File</source>
        <translation>Ficheiro Adxunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1381"/>
        <source>%1 likes this</source>
        <comment>One person</comment>
        <translation>%1 lle gusta isto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1386"/>
        <source>%1 like this</source>
        <comment>More than one person</comment>
        <translation>%1 lles gusta isto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1407"/>
        <source>1 like</source>
        <translation>1 Góstame</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1412"/>
        <source>%1 likes</source>
        <translation>%1 Góstame</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1476"/>
        <source>1 comment</source>
        <translation>1 comentario</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1481"/>
        <source>%1 comments</source>
        <translation>%1 comentarios</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1537"/>
        <source>%1 shared this</source>
        <comment>%1 = One person name</comment>
        <translation>%1 compartíu isto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1542"/>
        <source>%1 shared this</source>
        <comment>%1 = Names for more than one person</comment>
        <translation>%1 compartiron isto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1571"/>
        <source>Shared once</source>
        <translation>Compratido unha vez</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1576"/>
        <source>Shared %1 times</source>
        <translation>Compartido %1 veces</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1795"/>
        <source>You like this</source>
        <translation>A vostede góstalle isto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1796"/>
        <source>Unlike</source>
        <translation>Non gostar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1801"/>
        <source>Like this post</source>
        <translation>Góstame esta publicación</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1802"/>
        <source>Like</source>
        <translation>Góstame</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1957"/>
        <source>Are you sure you want to share your own post?</source>
        <translation>Está certo de quere compartir a súa propia publicación?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1962"/>
        <source>Share post?</source>
        <translation>Compartir publicación?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1952"/>
        <source>Do you want to share %1&apos;s post?</source>
        <translation>Quere compartir a publicación de %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1965"/>
        <source>&amp;Yes, share it</source>
        <translation>&amp;Sí, compartila</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1966"/>
        <location filename="../src/post.cpp" line="1986"/>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1982"/>
        <source>Unshare post?</source>
        <translation>Deixar de compartir publicación?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1983"/>
        <source>Do you want to unshare %1&apos;s post?</source>
        <translation>Quere deixar de compartir a publicación de %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1985"/>
        <source>&amp;Yes, unshare it</source>
        <translation>&amp;Sí, deixar de compartila</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2027"/>
        <source>WARNING: Delete post?</source>
        <translation>ATENCIÓN: Borrar publicación?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2028"/>
        <source>Are you sure you want to delete this post?</source>
        <translation>Esta seguro de querer borrar esta publicación?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, bórraa</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="36"/>
        <source>Click the image to see it in full size</source>
        <translation>Prema na imaxe para vela a tamaño completo</translation>
    </message>
</context>
<context>
    <name>ProfileEditor</name>
    <message>
        <location filename="../src/profileeditor.cpp" line="27"/>
        <source>Profile Editor</source>
        <translation>Editor do Perfil</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="51"/>
        <source>This is your Pump address</source>
        <translation>Este é o seu enderezo Pump</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="53"/>
        <source>This is the e-mail address associated with your account, for things such as notifications and password recovery</source>
        <translation>Este é o enderezo electrónico asociado á súa conta, para cousas como notificacións e recuperación de contrasinal</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="70"/>
        <source>Change &amp;E-mail...</source>
        <translation>Cambiar &amp;Enderezo electrónico...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="82"/>
        <source>Change &amp;Avatar...</source>
        <translation>Cambiar &amp;Avatar...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="92"/>
        <source>This is your visible name</source>
        <translation>Este é o seu nombe visible</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="109"/>
        <source>Changing your avatar will create a post in your timeline with it.
If you delete that post your avatar will be deleted too.</source>
        <translation>Cambiar o seu avatar creará unha publicación na súa liña temporal con el.
Se borra esa publicación tamén se borrará a seu avatar.</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="126"/>
        <source>&amp;Save Profile</source>
        <translation>&amp;Gardar Perfil</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="133"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="166"/>
        <source>Webfinger ID</source>
        <translation>Webfinger ID</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="167"/>
        <source>E-mail</source>
        <translation>Enderezo electrónico</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="169"/>
        <source>Avatar</source>
        <translation>Avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="171"/>
        <source>Full &amp;Name</source>
        <translation>&amp;Nome Completo</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="172"/>
        <source>&amp;Hometown</source>
        <translation>&amp;Cidade</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="173"/>
        <source>&amp;Bio</source>
        <translation>&amp;Bio</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="211"/>
        <source>Not set</source>
        <comment>In reference to the e-mail not being set for the account</comment>
        <translation>Non especificado</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="284"/>
        <source>Select avatar image</source>
        <translation>Seleccione imaxen para avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="286"/>
        <source>Image files</source>
        <translation>Ficheiros de imaxe</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="289"/>
        <source>All files</source>
        <translation>Tódolos ficheiros</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="312"/>
        <source>Invalid image</source>
        <translation>Imaxe non válida</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="313"/>
        <source>The selected image is not valid.</source>
        <translation>A imaxe seleccionada non é válida.</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../src/proxydialog.cpp" line="30"/>
        <source>Proxy Configuration</source>
        <translation>Configuración do Proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="38"/>
        <source>Do not use a proxy</source>
        <translation>Non usar un proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="55"/>
        <source>Your proxy username</source>
        <translation>O seu nom de usuario no proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="60"/>
        <source>Note: Password is not stored in a secure manner. If you wish, you can leave the field empty, and you&apos;ll be prompted for the password on startup.</source>
        <translation>Nota: O contrasinal non está gardado de xeito seguro. Se o desexa, pode deixar o campo baleiro, e se lle preguntará polo contrasinal no arranque.</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="75"/>
        <source>&amp;Save</source>
        <translation>&amp;Gardar</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="82"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="98"/>
        <source>Proxy &amp;Type</source>
        <translation>&amp;Tipo de Proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="100"/>
        <source>&amp;Hostname</source>
        <translation>Nome de &amp;Host</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="102"/>
        <source>&amp;Port</source>
        <translation>&amp;Porto</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="104"/>
        <source>Use &amp;Authentication</source>
        <translation>Empregar &amp;Autentificación</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="106"/>
        <source>&amp;User</source>
        <translation>&amp;Usuario</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="108"/>
        <source>Pass&amp;word</source>
        <translation>Contrasina&amp;l</translation>
    </message>
</context>
<context>
    <name>Publisher</name>
    <message>
        <location filename="../src/publisher.cpp" line="1054"/>
        <source>Select Picture...</source>
        <translation>Elixa Imaxe...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1056"/>
        <source>Find the picture in your folders</source>
        <translation>Atopar a imaxe nos seus cartafois</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="191"/>
        <source>Select who will see this post</source>
        <translation>Seleccionar quen verá esta mensaxe</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="188"/>
        <source>To...</source>
        <translation>Para...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="69"/>
        <source>Setting a title helps make the Meanwhile feed more informative</source>
        <translation>Escreber un título axuda a que a lista Mentras tanto sexa máis informativa</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="75"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="108"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="111"/>
        <source>Cancel the attachment, and go back to a regular note</source>
        <translation>Cancelar o adxunto, e voltar a unha nota normal</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="178"/>
        <source>Drafts</source>
        <translation>Borradores</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="202"/>
        <source>Select who will get a copy of this post</source>
        <translation>Seleccione quén recibirá unha copia desta publicación</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="232"/>
        <source>Picture</source>
        <translation>Imaxe</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="239"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="246"/>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="253"/>
        <source>Other</source>
        <comment>as in other kinds of files</comment>
        <translation>Outros</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="269"/>
        <source>Ad&amp;d...</source>
        <translation>Enga&amp;dir...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="272"/>
        <source>Upload media, like pictures or videos</source>
        <translation>Subir medios, como imaxes ou vídeos</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="300"/>
        <source>Hit Control+Enter to post with the keyboard</source>
        <translation>Prema Control+Énter para publicar co teclado</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="306"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="309"/>
        <source>Cancel the post</source>
        <translation>Cancelar a mensaxe</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="847"/>
        <source>File not found.</source>
        <translation>Ficheiro non atopado.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="964"/>
        <source>Attachment upload was cancelled.</source>
        <translation>Cancelouse a subida do adxunto.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1049"/>
        <source>Picture not set</source>
        <translation>Imaxen non establecida</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1079"/>
        <source>Select Audio File...</source>
        <translation>Seleccionar Ficheiro de Audio...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1081"/>
        <source>Find the audio file in your folders</source>
        <translation>Atopar o ficheiro de audio nos seus cartafois</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1074"/>
        <source>Audio file not set</source>
        <translation>Ficheiro de audio non establecido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1104"/>
        <source>Select Video...</source>
        <translation>Seleccione Vídeo...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1106"/>
        <source>Find the video in your folders</source>
        <translation>Atopar o vídeo nos seus cartafois</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1099"/>
        <source>Video not set</source>
        <translation>Vídeo non seleccionado</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1128"/>
        <source>Select File...</source>
        <translation>Seleccione Ficheiro...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1130"/>
        <source>Find the file in your folders</source>
        <translation>Atopar o ficheiro nos seus cartafois</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1123"/>
        <source>File not set</source>
        <translation>Ficheiro non seleccionado</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1199"/>
        <location filename="../src/publisher.cpp" line="1243"/>
        <source>Error: Already composing</source>
        <translation>Erro: Xa está a compoñer</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1200"/>
        <source>You can&apos;t edit a post at this time, because a post is already being composed.</source>
        <translation>Non pode editar a mensaxe neste intre porque xa se está compoñendo outra mensaxe.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1218"/>
        <source>Update</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="839"/>
        <source>It is owned by %1.</source>
        <comment>%1 = a username</comment>
        <translation>É propiedade de %1.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1244"/>
        <source>You can&apos;t create a message for %1 at this time, because a post is already being composed.</source>
        <translation>Non pode crear unha mensaxe para %1 neste intre, porque xa se está a compoñer unha mensaxe.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1371"/>
        <source>Draft loaded.</source>
        <translation>Borrador cargado.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1376"/>
        <source>ERROR: Already composing</source>
        <translation>ERRO: Aínda estase a compoñer</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1377"/>
        <source>You can&apos;t load a draft at this time, because a post is already being composed.</source>
        <translation>Non pode cargar un borrador neste intre porque agora estase compoñendo outra mensaxe.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1396"/>
        <source>Draft saved.</source>
        <translation>Borrador gardado.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1433"/>
        <source>Posting failed.

Try again.</source>
        <translation>Publicación fallida.

Ténteo outra vez.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1577"/>
        <source>Warning: You have no followers yet</source>
        <translation>Atención: Vostede aínda non ten seguidores</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1578"/>
        <source>You&apos;re trying to post to your followers only, but you don&apos;t have any followers yet.</source>
        <translation>Está tentando escribirlle só aos seus seguidores, pero aínda non ten seguidores.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1582"/>
        <source>If you post like this, no one will be able to see your message.</source>
        <translation>Se publica deste xeito, ninguén poderá ver a súa mensaxe.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1585"/>
        <source>Do you want to make the post public instead of followers-only?</source>
        <translation>Quere facer pública a publicación en lugar de só para os seus seguidores?</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1588"/>
        <source>&amp;Yes, make it public</source>
        <translation>&amp;Sí, facelo público</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1590"/>
        <source>&amp;Cancel, go back to the post</source>
        <translation>&amp;Cancelar, voltar á publicación</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1643"/>
        <source>Updating...</source>
        <translation>Actualizando...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1654"/>
        <source>Post is empty.</source>
        <translation>A publicación está baleira.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1658"/>
        <source>File not selected.</source>
        <translation>Ficheiro non seleccionado.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="597"/>
        <source>Select one image</source>
        <translation>Seleccione unha imaxe</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="598"/>
        <source>Image files</source>
        <translation>Ficheiros de Imaxe</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="626"/>
        <source>Select one file</source>
        <translation>Seleccione un ficheiro</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="629"/>
        <source>Invalid file</source>
        <translation>Ficheiro inválido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="630"/>
        <source>The file type cannot be detected.</source>
        <translation>Non se pode detectar o tipo de ficheiro.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="642"/>
        <source>All files</source>
        <translation>Tódolos ficheiros</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="659"/>
        <source>Since you&apos;re uploading an image, you could scale it down a little or save it in a more compressed format, like JPG.</source>
        <translation>Xa que está a subir unha imaxe, podería reducirlle a escala ou gardala nun formato máis comprimido, como JPG.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="664"/>
        <source>File is too big</source>
        <translation>O ficheiro é demasiado grande</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="665"/>
        <source>Dianara currently limits file uploads to 10 MiB per post, to prevent possible storage or network problems in the servers.</source>
        <translation>Actualmente Dianara limita a subida de ficheiros a 10 MiB por publicación, para previr posibles problemas de almacenamento ou de rede nos servidores.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="670"/>
        <source>This is a temporary measure, since the servers cannot set their own limits yet.</source>
        <translation>Está é unha medida temporal, xa que os servidores aínda non poden establecer os seus propios límites.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="675"/>
        <source>Sorry for the inconvenience.</source>
        <translation>Pregámoslle disculpas polas molestias.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="850"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="832"/>
        <source>The selected file cannot be accessed:</source>
        <translation>No se pode acceder ao ficheiro seleccionado:</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="842"/>
        <source>You might not have the necessary permissions.</source>
        <translation>Poida que non teña vostede os permisos necesarios.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="704"/>
        <source>Resolution</source>
        <comment>Image resolution (size)</comment>
        <translation>Resolución</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="727"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="729"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1686"/>
        <source>%1 KiB of %2 KiB uploaded</source>
        <translation>%1 KiB de %2 KiB subidos</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="601"/>
        <source>Invalid image</source>
        <translation>Imaxen non válida</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="80"/>
        <source>Add a brief title for the post here (recommended)</source>
        <translation>Engada eiquí un título breve para a publicación (recomendado)</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="199"/>
        <source>Cc...</source>
        <translation>Cc...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="297"/>
        <location filename="../src/publisher.cpp" line="942"/>
        <source>Post</source>
        <comment>verb</comment>
        <translation>Publicar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="427"/>
        <source>Note started from another application.</source>
        <translation>Nota arrancada dende outra aplicación.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="431"/>
        <source>Ignoring new note request from another application.</source>
        <translation>Ignorando petición de nova nota dende outra aplicación.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1229"/>
        <source>Editing post.</source>
        <translation>Editando publicación.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1589"/>
        <source>&amp;No, post to my followers only</source>
        <translation>&amp;Non, publicar só para os meus seguidores</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="602"/>
        <source>The image format cannot be detected.
The extension might be wrong, like a GIF image renamed to image.jpg or similar.</source>
        <translation>Non se pode detectar o formato da imaxe.
A extensión pode ser incorrecta, como unha imaxe GIF renomeada a imaxen.jpg ou similar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="608"/>
        <source>Select one audio file</source>
        <translation>Seleccione un ficheiro de audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="609"/>
        <source>Audio files</source>
        <translation>Ficheiros de audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="612"/>
        <source>Invalid audio file</source>
        <translation>Ficheiro de audio non válido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="613"/>
        <source>The audio format cannot be detected.</source>
        <translation>Non se pode detectar o formato do audio.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="617"/>
        <source>Select one video file</source>
        <translation>Seleccione un ficheiro de vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="618"/>
        <source>Video files</source>
        <translation>Ficheiros de vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="621"/>
        <source>Invalid video file</source>
        <translation>Ficheiro de vídeo non válido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="622"/>
        <source>The video format cannot be detected.</source>
        <translation>Non se pode detectar o formato do vídeo.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1610"/>
        <source>Posting...</source>
        <translation>Publicando...</translation>
    </message>
</context>
<context>
    <name>PumpController</name>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="669"/>
        <source>Creating person list...</source>
        <translation>Creando lista de persoas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="692"/>
        <source>Deleting person list...</source>
        <translation>Borrando lista de persoas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="910"/>
        <source>Getting likes...</source>
        <translation>Recuperando Góstame...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="942"/>
        <source>Getting comments...</source>
        <translation>Obtendo comentarios...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1667"/>
        <source>Error connecting to %1</source>
        <translation>Erro conectando con %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1677"/>
        <source>Unhandled HTTP error code %1</source>
        <translation>Código de erro HTTP %1 non manexado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1947"/>
        <source>Message liked or unliked successfully.</source>
        <translation>Acción de gostar ou deixar de gostar mensaxe completouse con éxito.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1981"/>
        <source>Comment %1 posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>Comentario %1 publicado con éxito.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2164"/>
        <source>Message deleted successfully.</source>
        <translation>Mensaxe borrada con éxito.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1961"/>
        <source>Likes received.</source>
        <translation>Góstame recibidos.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="252"/>
        <source>Authorized to use account %1. Getting initial data.</source>
        <translation>Autorizad a usar a conta %1. Obtendo datos iniciais.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="257"/>
        <source>There is no authorized account.</source>
        <translation>Non hay.unha conta autorizada.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="362"/>
        <source>Updating profile...</source>
        <translation>Actualizando perfil...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="549"/>
        <source>Getting list of &apos;Following&apos;...</source>
        <translation>Obtendo lista de &apos;Seguindo&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="562"/>
        <source>Getting list of &apos;Followers&apos;...</source>
        <translation>Obtendo lista de &apos;Seguidores&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="581"/>
        <source>Getting site users for %1...</source>
        <comment>%1 is a server name</comment>
        <translation>Obtendo usuarios do sitio %1...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="633"/>
        <source>Getting list of person lists...</source>
        <translation>Obtendo lista de listas de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="707"/>
        <source>Getting a person list...</source>
        <translation>Obtendo unha lista de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="751"/>
        <source>Adding person to list...</source>
        <translation>Engadindo persona á lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="792"/>
        <source>Removing person from list...</source>
        <translation>Borrando persona da lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="840"/>
        <source>Creating group...</source>
        <translation>Creando grupo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="870"/>
        <source>Joining group...</source>
        <translation>Uníndose ao grupo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="895"/>
        <source>Leaving group...</source>
        <translation>Abandonando grupo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="998"/>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Obtend %1...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1045"/>
        <source>Timeline</source>
        <translation>Liña temporal</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1049"/>
        <source>Messages</source>
        <translation>Mensaxes</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1075"/>
        <source>User timeline</source>
        <translation>Liña temporal do usuario</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1207"/>
        <source>Uploading %1</source>
        <comment>1=filename</comment>
        <translation>Subindo %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1465"/>
        <source>Error loading timeline!</source>
        <translation>Erro cargando liña temporal!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1476"/>
        <source>Error loading minor feed!</source>
        <translation>Erro cargando liña temporal secundaria!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1488"/>
        <source>Unable to verify the address!</source>
        <translation>Imposible verificar a dirección!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1505"/>
        <source>HTTP error</source>
        <comment>For the following HTTP error codesyou can check http://en.wikipedia.org/wiki/List_of_HTTP_status_codes in your language</comment>
        <translation>Erro HTTP</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1517"/>
        <source>Gateway Timeout</source>
        <comment>HTTP 504 error string</comment>
        <translation>Tempo de espera da pasarela agotado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1527"/>
        <source>Service Unavailable</source>
        <comment>HTTP 503 error string</comment>
        <translation>Servizo Non Dispoñible</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1546"/>
        <source>Bad Gateway</source>
        <comment>HTTP 502 error string</comment>
        <translation>Pasarela incorrecta</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1556"/>
        <source>Not Implemented</source>
        <comment>HTTP 501 error string</comment>
        <translation>Non Implementado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1566"/>
        <source>Internal Server Error</source>
        <comment>HTTP 500 error string</comment>
        <translation>Erro Interno do Servidor</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1586"/>
        <source>Gone</source>
        <comment>HTTP 410 error string</comment>
        <translation>Foise</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1596"/>
        <source>Not Found</source>
        <comment>HTTP 404 error string</comment>
        <translation>Non Atopado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1606"/>
        <source>Forbidden</source>
        <comment>HTTP 403 error string</comment>
        <translation>Prohibido</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1616"/>
        <source>Unauthorized</source>
        <comment>HTTP 401 error string</comment>
        <translation>Non Autorizado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1627"/>
        <source>Bad Request</source>
        <comment>HTTP 400 error string</comment>
        <translation>Mala Petición</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1646"/>
        <source>Moved Temporarily</source>
        <comment>HTTP 302 error string</comment>
        <translation>Movido Temporalmente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1656"/>
        <source>Moved Permanently</source>
        <comment>HTTP 301 error string</comment>
        <translation>Movido Permanentemente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1787"/>
        <source>Server version: %1</source>
        <translation>Versión do servidor: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1790"/>
        <source>Profile received.</source>
        <translation>Perfil recibido.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1792"/>
        <source>Followers</source>
        <translation>Seguidores</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1797"/>
        <source>Following</source>
        <translation>Seguindo</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1813"/>
        <source>Profile updated.</source>
        <translation>Perfil actualizado.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1822"/>
        <source>E-mail updated: %1</source>
        <translation>Enderezo electrónico actualizado: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1850"/>
        <source>%1 published successfully. Updating post content...</source>
        <comment>%1 is the type of object: note, image...</comment>
        <translation>%1 publicado correctamente. Actualizando contido da publicación...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1866"/>
        <source>Untitled post %1 published successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>Publicación sen titular %1 publicada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1870"/>
        <source>Post %1 published successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>Publicación %1 publicada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1887"/>
        <source>Avatar published successfully.</source>
        <translation>Avatar publicado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1919"/>
        <source>Untitled post %1 updated successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translation>Publicación sen título %1 actualizada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1923"/>
        <source>Post %1 updated successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translation>Publicación %1 actualizada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1933"/>
        <source>Comment %1 updated successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>Comentario %1 actualizado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2081"/>
        <source>Adding items...</source>
        <translation>Engadindo elementos...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2192"/>
        <source>Following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Seguindo a %1 (%2) correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2202"/>
        <source>Stopped following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Deixou de seguir a %1 (%2) correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2345"/>
        <source>List of %1 users received.</source>
        <comment>%1 is a server name</comment>
        <translation>Lista de usuarios de %1 recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2369"/>
        <source>Person list &apos;%1&apos; created successfully.</source>
        <translation>Lista de personas &apos;%1&apos; creada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2435"/>
        <source>%1 (%2) added to list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 (%2) engadido á lista correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2455"/>
        <source>%1 (%2) removed from list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 (%2) eliminado da lista correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2466"/>
        <source>Group %1 created successfully.</source>
        <translation>Grupo %1 creado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2477"/>
        <source>Group %1 joined successfully.</source>
        <translation>Grupo %1 unido correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2490"/>
        <source>Left the %1 group successfully.</source>
        <translation>Abandonou o grupo %1 correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2818"/>
        <source>OAuth error while authorizing application.</source>
        <translation>Erro OAuth ao autorizar a aplicación.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2986"/>
        <source>%1 attempts</source>
        <translation>%1 intentos</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2990"/>
        <source>1 attempt</source>
        <translation>1 intento</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2993"/>
        <source>Some initial data was not received. Restarting initialization...</source>
        <translation>Non se recibiron alguns datos iniciais. Reiniciando inicialización...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3411"/>
        <source>Can&apos;t follow %1 at this time.</source>
        <comment>%1 is a user ID</comment>
        <translation>Non pode seguir a %1 neste intre.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3415"/>
        <source>Trying to follow %1.</source>
        <comment>%1 is a user ID</comment>
        <translation>Tentando seguir a %1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3435"/>
        <source>Checking address %1 before following...</source>
        <translation>Comprobando dirección %1 antes de seguir...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2252"/>
        <source>List of &apos;following&apos; completely received.</source>
        <translation>Lista de &apos;seguindo&apos; recibida completamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="937"/>
        <source>The comments for this post cannot be loaded due to missing data on the server.</source>
        <translation>Os comentarios para esta publicación non se poden cargar porque hai datos que faltan no servidor.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1053"/>
        <source>Activity</source>
        <translation>Actividade</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1057"/>
        <source>Favorites</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1062"/>
        <source>Meanwhile</source>
        <translation>Mentras tanto</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1066"/>
        <source>Mentions</source>
        <translation>Mencións</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1070"/>
        <source>Actions</source>
        <translation>Accións</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2006"/>
        <source>1 comment received.</source>
        <translation>1 comentario recibido.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2010"/>
        <source>%1 comments received.</source>
        <translation>%1 comentarios recibidos.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2037"/>
        <source>Post by %1 shared successfully.</source>
        <comment>1=author of the post we are sharing</comment>
        <translation>Publicación de %1 compartida correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2068"/>
        <source>Received &apos;%1&apos;.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Recibido &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2259"/>
        <source>Partial list of &apos;following&apos; received.</source>
        <translation>Recibida lista parcial de &apos;seguindo&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2282"/>
        <source>List of &apos;followers&apos; completely received.</source>
        <translation>Lista de &apos;seguidores&apos; recibida completamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2288"/>
        <source>Partial list of &apos;followers&apos; received.</source>
        <translation>Recibida lista parcial de &apos;seguidores&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2325"/>
        <source>List of &apos;lists&apos; received.</source>
        <translation>Recibida lista de &apos;listas&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2380"/>
        <source>Person list deleted successfully.</source>
        <translation>Lista de personas borrada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2400"/>
        <source>Person list received.</source>
        <translation>Lista de personas recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2554"/>
        <source>File uploaded successfully. Posting message...</source>
        <translation>Ficheiro subido correctamente. Publicando mensaxe...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2563"/>
        <source>Avatar uploaded.</source>
        <translation>Avatar subido.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2607"/>
        <source>SSL errors in connection to %1!</source>
        <translation>Erros SSL na conexión con %1!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2619"/>
        <source>Loading external image from %1 regardless of SSL errors, as configured...</source>
        <comment>%1 is a hostname</comment>
        <translation>Cargando imaxen externa dende %1 a pesares dos erros SSL, tal como está configurado...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2648"/>
        <source>The application is not registered with your server yet. Registering...</source>
        <translation>A aplicación aínda non está rexistrada co seu servidor. Rexistrando...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2686"/>
        <source>Getting OAuth token...</source>
        <translation>Obtendo o token OAuth...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2707"/>
        <source>OAuth support error</source>
        <translation>Erro de soporte OAuth</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2708"/>
        <source>Your installation of QOAuth, a library used by Dianara, doesn&apos;t seem to have HMAC-SHA1 support.</source>
        <translation>A súa instalación de QOAuth, a libraría empregada por Dianara, non semella ter soporte HMAC-SHA1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2712"/>
        <source>You probably need to install the OpenSSL plugin for QCA: %1, %2 or similar.</source>
        <translation>Probablemente necesite instalar o plugin OpenSSL para QCA: %1, %2 ou similar.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2766"/>
        <location filename="../src/pumpcontroller.cpp" line="2822"/>
        <source>Authorization error</source>
        <translation>Erro de autorización</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2767"/>
        <source>There was an OAuth error while trying to get the authorization token.</source>
        <translation>Houbo un erro OAuth mentres se tentaba obter o token de autorización.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2770"/>
        <source>QOAuth error %1</source>
        <translation>QOAuth erro %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2802"/>
        <source>Application authorized successfully.</source>
        <translation>Aplicación autorizada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2845"/>
        <source>Waiting for proxy password...</source>
        <translation>Agardando por contrasinal do proxy...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2874"/>
        <source>Still waiting for profile. Trying again...</source>
        <translation>Aínda esperando polo perfil. Tentándoo outra vez...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3000"/>
        <source>Some initial data was not received after several attempts. Something might be wrong with your server. You might still be able to use the service normally.</source>
        <translation>Algúns datos iniciais non se recibiron tras varios intentos. Algo podería ir mal no seu servidor. Aínda así debería poder usar o servicio con normalidade.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3010"/>
        <source>All initial data received. Initialization complete.</source>
        <translation>Recibidos todos os datos iniciais. Inicialización completada.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3023"/>
        <source>Ready.</source>
        <translation>Listo.</translation>
    </message>
</context>
<context>
    <name>SiteUsersList</name>
    <message>
        <location filename="../src/siteuserslist.cpp" line="30"/>
        <source>You can get a list of the newest users registered on your server by clicking the button below.</source>
        <translation>Pode obter unha lista dos novos usuarios rexistrados no seu servidor premendo no botón de embaixo.</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="47"/>
        <source>More resources to find users:</source>
        <translation>Máis recursos para atopar usuarios:</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="51"/>
        <source>Wiki page &apos;Users by language&apos;</source>
        <translation>Páxina Wiki &apos;Usuarios por idioma&apos;</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="56"/>
        <source>PPump user search service at inventati.org</source>
        <translation>Servicio PPump de procura de usuario en inventati.org</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="61"/>
        <source>List of Followers for the Pump.io Community account</source>
        <translation>Lista de Seguidores para a conta de Comunidade Pump.io</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="41"/>
        <source>Get list of users from your server</source>
        <translation>Obter lista de usuario dende o seu servidor</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="87"/>
        <source>Close list</source>
        <translation>Pechar lista</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="143"/>
        <source>Loading...</source>
        <translation>Cargando...</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="154"/>
        <source>%1 users in %2</source>
        <comment>%1 = user count, %2 = server name</comment>
        <translation>%1 usuarios en %2</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/timeline.cpp" line="76"/>
        <source>Welcome to Dianara</source>
        <translation>Benvido/a a Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="78"/>
        <source>Dianara is a &lt;b&gt;Pump.io&lt;/b&gt; client.</source>
        <translation>Dianara é un cliente &lt;b&gt;Pump.io&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="81"/>
        <source>If you don&apos;t have a Pump account yet, you can get one at the following address, for instance:</source>
        <translation>Se aínda non ten unha conta Pump, pode obter unha no seguinte enderezo, por exemplo:</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="88"/>
        <source>Press &lt;b&gt;F1&lt;/b&gt; if you want to open the Help window.</source>
        <translation>Prema &lt;b&gt;F1&lt;/b&gt; se quere abrir a fiestra de Axuda.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="91"/>
        <source>First, configure your account from the &lt;b&gt;Settings - Account&lt;/b&gt; menu.</source>
        <translation>Primeiro, configure a súa conta dende o menú &lt;b&gt; Conta - Axustes&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="94"/>
        <source>After the process is done, your profile and timelines should update automatically.</source>
        <translation>Unha vez rematado o proceso, o seu perfil e as liñas temporais deberían actualizarse automáticamente.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="98"/>
        <source>Take a moment to look around the menus and the Configuration window.</source>
        <translation>Tome uns momentos para revisar os menús e a fiestra de Configuración.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="102"/>
        <source>You can also set your profile data and picture from the &lt;b&gt;Settings - Edit Profile&lt;/b&gt; menu.</source>
        <translation>Tamén pode establecer os datos do seu perfil e a imaxe no menú &lt;b&gt;Axustes - Edictar Perfil&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="106"/>
        <source>There are tooltips everywhere, so if you hover over a button or a text field with your mouse, you&apos;ll probably see some extra information.</source>
        <translation>Hai pistas emerxentes por todas partes, de xeito que se move o ratón sobre un botón ou un campo de texto, probablemente verá algo de información extra.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="113"/>
        <source>Dianara&apos;s blog</source>
        <translation>Blog de Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="115"/>
        <source>Pump.io User Guide</source>
        <translation>Guía de Usuario Pump.io</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="126"/>
        <source>Direct Messages Timeline</source>
        <translation>Liña Temporal de Mensaxes Directas</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="127"/>
        <source>Here, you&apos;ll see posts specifically directed to you.</source>
        <translation>Eiquí verá as publicacións dirixidas específicamente a vostede.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="137"/>
        <source>Activity Timeline</source>
        <translation>Liña Temporal de Actividade</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="138"/>
        <source>You&apos;ll see your own posts here.</source>
        <translation>Eiquí verá as súas propias publicacións.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="147"/>
        <source>Favorites Timeline</source>
        <translation>Liña Temporal de Favoritos</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="148"/>
        <source>Posts and comments you&apos;ve liked.</source>
        <translation>Publicacións e comentarios que lle gustaron.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="231"/>
        <source>Newest</source>
        <translation>As máis novas</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="240"/>
        <source>Newer</source>
        <translation>Máis novo</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="263"/>
        <source>Older</source>
        <translation>Máis antigas</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="335"/>
        <source>Requesting...</source>
        <translation>Solicitando...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="506"/>
        <source>Loading...</source>
        <translation>Cargando...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="615"/>
        <source>Page %1 of %2.</source>
        <translation>Páxina %1 de %2.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="619"/>
        <source>Showing %1 posts per page.</source>
        <translation>Amosando %1 publicacións por páxina.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="622"/>
        <source>%1 posts in total.</source>
        <translation>%1 publicacións en total.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="626"/>
        <source>Click here or press Control+G to jump to a specific page</source>
        <translation>Prema eiquí ou pulse Control+G para saltar a unha páxina específica</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="740"/>
        <source>&apos;%1&apos; cannot be updated because a comment is currently being composed.</source>
        <comment>%1 = feed&apos;s name</comment>
        <translation>Non se pode actualizar %1 porque estase a compoñer unha mensaxe neste intre.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="848"/>
        <source>%1 more posts pending for next update.</source>
        <translation>%1 máis publicacións pendentes para a seguinte actualización.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="854"/>
        <source>Click here to receive them now.</source>
        <translation>Prema eiquí para recibilos agora.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="1139"/>
        <source>There are no posts</source>
        <translation>Non hai publicacións</translation>
    </message>
</context>
<context>
    <name>Timestamp</name>
    <message>
        <location filename="../src/timestamp.cpp" line="61"/>
        <source>Invalid timestamp!</source>
        <translation>Marca de tempo non válida!</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="93"/>
        <source>A minute ago</source>
        <translation>Hai un minuto</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="97"/>
        <source>%1 minutes ago</source>
        <translation>Hai %1 minutos</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="108"/>
        <source>An hour ago</source>
        <translation>Hai unha hora</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="112"/>
        <source>%1 hours ago</source>
        <translation>Hai %1 horas</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="79"/>
        <source>Just now</source>
        <translation>Agora mesmo</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="83"/>
        <source>In the future</source>
        <translation>No futuro</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="123"/>
        <source>Yesterday</source>
        <translation>Onte</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="127"/>
        <source>%1 days ago</source>
        <translation>Hai %1 días</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="138"/>
        <source>A month ago</source>
        <translation>Hai un mes</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="142"/>
        <source>%1 months ago</source>
        <translation>Hai %1 meses</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="151"/>
        <source>A year ago</source>
        <translation>Hai un ano</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="155"/>
        <source>%1 years ago</source>
        <translation>Hai %1 anos</translation>
    </message>
</context>
<context>
    <name>UserPosts</name>
    <message>
        <location filename="../src/userposts.cpp" line="39"/>
        <source>Posts by %1</source>
        <translation>Mensaxes de %1</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="64"/>
        <source>Loading...</source>
        <translation>Cargando...</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="69"/>
        <source>&amp;Close</source>
        <translation>&amp;Pechar</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="153"/>
        <source>Received &apos;%1&apos;.</source>
        <translation>Recibido: &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="160"/>
        <source>%1 posts</source>
        <translation>%1 mensaxes</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="169"/>
        <source>Error loading the timeline</source>
        <translation>Erro cargando a liña temporal</translation>
    </message>
</context>
</TS>
