﻿<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>ASActivity</name>
    <message>
        <location filename="../src/asactivity.cpp" line="89"/>
        <location filename="../src/asactivity.cpp" line="135"/>
        <source>Public</source>
        <translation>Pubblico</translation>
    </message>
    <message>
        <location filename="../src/asactivity.cpp" line="422"/>
        <source>%1 by %2</source>
        <comment>1=kind of object: note, comment, etc; 2=author&apos;s name</comment>
        <translation>%1 di %2</translation>
    </message>
</context>
<context>
    <name>ASObject</name>
    <message>
        <location filename="../src/asobject.cpp" line="275"/>
        <source>Note</source>
        <comment>Noun, an object type</comment>
        <translation>Nota</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="280"/>
        <source>Article</source>
        <comment>Noun, an object type</comment>
        <translation>Articolo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="285"/>
        <source>Image</source>
        <comment>Noun, an object type</comment>
        <translation>Immagine</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="290"/>
        <source>Audio</source>
        <comment>Noun, an object type</comment>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="295"/>
        <source>Video</source>
        <comment>Noun, an object type</comment>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="300"/>
        <source>File</source>
        <comment>Noun, an object type</comment>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="305"/>
        <source>Comment</source>
        <comment>Noun, as in object type: a comment</comment>
        <translation>Commento</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="310"/>
        <source>Group</source>
        <comment>Noun, an object type</comment>
        <translation>Gruppo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="315"/>
        <source>Collection</source>
        <comment>Noun, an object type</comment>
        <translation>Collezione</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="320"/>
        <source>Other</source>
        <comment>As in: other type of post</comment>
        <translation>Altro</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="376"/>
        <source>No detailed location</source>
        <translation>Nessuna località dettagliata</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="402"/>
        <source>Deleted on %1</source>
        <translation>Eliminato il %1</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="578"/>
        <location filename="../src/asobject.cpp" line="648"/>
        <source>and one other</source>
        <translation>e nessun altro</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="582"/>
        <location filename="../src/asobject.cpp" line="652"/>
        <source>and %1 others</source>
        <translation>e %1 altri</translation>
    </message>
</context>
<context>
    <name>ASPerson</name>
    <message>
        <location filename="../src/asperson.cpp" line="173"/>
        <source>Hometown</source>
        <translation>Città</translation>
    </message>
</context>
<context>
    <name>AccountDialog</name>
    <message>
        <location filename="../src/accountdialog.cpp" line="85"/>
        <source>Your Pump.io address:</source>
        <translation>Il tuo indirizzo Pump.io:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="95"/>
        <source>Get &amp;Verifier Code</source>
        <translation>Ottieni il Codice di &amp;Verifica</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="128"/>
        <source>Verifier code:</source>
        <translation>Codice di Verifica:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="131"/>
        <source>Enter or paste the verifier code provided by your Pump server here</source>
        <translation>Inserisci o incolla il codice di verifica fornito dal tuo server Pump qui</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="168"/>
        <source>&amp;Save Details</source>
        <translation>&amp;Salva i Dati</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="401"/>
        <source>If the browser doesn&apos;t open automatically, copy this address manually</source>
        <translation>Se il browser non si apre automaticamente, copia questo indirizzo manualmente</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="25"/>
        <source>Account Configuration</source>
        <translation>Configurazione Account</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="50"/>
        <source>First, enter your Webfinger ID, your pump.io address.</source>
        <translation>Prima inserisci il tuo Webfinger ID, il tuo indirizzo Pump.io.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="53"/>
        <source>Your address looks like username@pumpserver.org, and you can find it in your profile, in the web interface.</source>
        <translation>Il tuo indirizzo assomiglia a nomeutente@pumpserver.org, e lo puoi trovare sul tuo profilo, nell&apos;interfaccia web.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="58"/>
        <source>If your profile is at https://pump.example/yourname, then your address is yourname@pump.example</source>
        <translation>Se il tuo profilo, per esempio, è https://pumpserver.org/nomeutente, di conseguenza il tuo indirizzo è nomeutente@pumpserver.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="62"/>
        <source>If you don&apos;t have an account yet, you can sign up for one at %1. This link will take you to a random public server.</source>
        <comment>1=link to website</comment>
        <translation>Se non hai ancora un account, puoi iscriverti su %1. Questo link ti porterà su un server pubblico casuale.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="70"/>
        <source>If you need help: %1</source>
        <translation>Se hai bisogno di aiuto: %1</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="73"/>
        <source>Pump.io User Guide</source>
        <translation>Guida Utente Pump.io</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="88"/>
        <source>Your address, like username@pumpserver.org</source>
        <translation>Il tuo indirizzo, tipo nomeutente@serverpump.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="98"/>
        <source>After clicking this button, a web browser will open, requesting authorization for Dianara</source>
        <translation>Dopo aver cliccato questo bottone, il browser web si dovrebbe aprire, richiedendo l&apos;autorizzazione per Dianara</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="109"/>
        <source>Once you have authorized Dianara from your Pump server web interface, you&apos;ll receive a code called VERIFIER.
Copy it and paste it into the field below.</source>
        <comment>Don&apos;t translate the VERIFIER word!</comment>
        <translation>Una volta autorizzato Dianara dall&apos;interfaccia web del tuo server Pump.io, riceverai un codice chiamato VERIFIER. Copialo e incollalo nel campo qui sotto.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="141"/>
        <source>&amp;Authorize Application</source>
        <translation>&amp;Autorizza Applicazione</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="176"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="188"/>
        <source>Your account is properly configured.</source>
        <translation>Il tuo account è configurato correttamente.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="192"/>
        <source>Press Unlock if you wish to configure a different account.</source>
        <translation>Premi Sblocca se vuoi configurare un altro account.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="200"/>
        <source>&amp;Unlock</source>
        <translation>&amp;Sblocca</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="327"/>
        <source>A web browser will start now, where you can get the verifier code</source>
        <translation>Ora si avviera il browser web, dove potrai ottenere il codice di verifica</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="336"/>
        <source>Your Pump address is invalid</source>
        <translation>Il tuo indirizzo Pump.io non è valido</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="355"/>
        <source>Verifier code is empty</source>
        <translation>Il campo del codice di verifica è vuoto</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="382"/>
        <source>Dianara is authorized to access your data</source>
        <translation>Dianara è autorizzato ad acceder ai tuoi dati</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="410"/>
        <source>Unable to open web browser!</source>
        <translation>Impossibile aprire il browser Web!</translation>
    </message>
</context>
<context>
    <name>AudienceSelector</name>
    <message>
        <location filename="../src/audienceselector.cpp" line="33"/>
        <source>&apos;To&apos; List</source>
        <translation>Lista &apos;A&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="37"/>
        <source>&apos;Cc&apos; List</source>
        <translation>Lista &apos;Cc&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="58"/>
        <source>&amp;Add to Selected</source>
        <translation>&amp;Aggiungi ai Selezionati</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="73"/>
        <source>All Contacts</source>
        <translation>Tutti i Contatti</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="79"/>
        <source>Select people from the list on the left.
You can drag them with the mouse, click or double-click on them, or select them and use the button below.</source>
        <comment>ON THE LEFT should change to ON THE RIGHT in RTL languages</comment>
        <translation type="unfinished">Seleziona le persone dalla lista a sinistra.
Puoi trascinarle con il mouse, click o doppio click su di esse, o selezionarle e usare il bottone qui sotto.</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="96"/>
        <source>Clear &amp;List</source>
        <translation>Pulisci la &amp;Lista</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="120"/>
        <source>&amp;Done</source>
        <translation>&amp;Fatto</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="127"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="167"/>
        <location filename="../src/audienceselector.cpp" line="385"/>
        <source>Public</source>
        <translation>Pubblico</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="172"/>
        <location filename="../src/audienceselector.cpp" line="391"/>
        <source>Followers</source>
        <translation>Followers</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="177"/>
        <source>Lists</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="187"/>
        <source>People...</source>
        <translation>Persone...</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="108"/>
        <source>Selected People</source>
        <translation>Persone Selezionate</translation>
    </message>
</context>
<context>
    <name>AvatarButton</name>
    <message>
        <location filename="../src/avatarbutton.cpp" line="143"/>
        <source>Open %1&apos;s profile in web browser</source>
        <translation>Apri il profilo di %1 nel browser web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="139"/>
        <source>Open your profile in web browser</source>
        <translation>Apri il tuo profilo nel browser web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="165"/>
        <source>Send message to %1</source>
        <translation>Invia messaggio a %1</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="174"/>
        <source>Browse messages</source>
        <translation>Sfoglia messaggi</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="230"/>
        <source>Stop following</source>
        <translation>Smetti di seguire</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="241"/>
        <source>Follow</source>
        <translation>Segui</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="287"/>
        <source>Stop following?</source>
        <translation>Smettere di seguire?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="288"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Sei sicuro di voler smettere di seguire %1?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="291"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Si, smetti di seguire</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="292"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>BannerNotification</name>
    <message>
        <location filename="../src/bannernotification.cpp" line="38"/>
        <source>Timelines were not automatically updated to avoid interruptions.</source>
        <translation>Le tempistiche non sono state aggiornate automaticamente per evitare interruzioni</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="47"/>
        <source>This happens when it is time to autoupdate the timelines, but you are not at the top of the first page, to avoid interruptions while you read</source>
        <translation>Questo succede quando è il momento di aggiornare automaticamente le linee temporali, ma non sei nella parte superiore della prima pagina, per evitare interruzioni mentre leggi</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="55"/>
        <source>Update now</source>
        <translation>Aggiorna ora</translation>
    </message>
    <message>
        <location filename="../src/bannernotification.cpp" line="65"/>
        <source>Hide this message</source>
        <translation>Nascond questo messaggio</translation>
    </message>
</context>
<context>
    <name>CharacterPicker</name>
    <message>
        <location filename="../src/characterpicker.cpp" line="26"/>
        <source>Select a symbol</source>
        <translation>Seleziona un simbolo</translation>
    </message>
    <message>
        <location filename="../src/characterpicker.cpp" line="85"/>
        <source>Insert in big size</source>
        <translation>Inserisci in una grande dimensione</translation>
    </message>
    <message>
        <location filename="../src/characterpicker.cpp" line="129"/>
        <source>&amp;Insert Symbol</source>
        <translation>&amp;Inserisci Simbolo</translation>
    </message>
    <message>
        <location filename="../src/characterpicker.cpp" line="136"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../src/colorpicker.cpp" line="35"/>
        <source>Change...</source>
        <translation>Cambia...</translation>
    </message>
    <message>
        <location filename="../src/colorpicker.cpp" line="114"/>
        <source>Choose a color</source>
        <translation>Scegli un colore</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../src/comment.cpp" line="251"/>
        <source>Posted on %1</source>
        <translation>Pubblicato il %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="256"/>
        <source>Modified on %1</source>
        <translation>Modificato il %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="114"/>
        <source>Like or unlike this comment</source>
        <translation>Decidi se ti piace o non ti piace questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="121"/>
        <source>Quote</source>
        <comment>This is a verb, infinitive</comment>
        <translation>Quotare</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="129"/>
        <source>Reply quoting this comment</source>
        <translation>Rispondi quotando questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="136"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="142"/>
        <source>Modify this comment</source>
        <translation>Modifica questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="148"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="154"/>
        <source>Erase this comment</source>
        <translation>Elimina questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="287"/>
        <source>Unlike</source>
        <translation>Non mi piace</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="291"/>
        <source>Like</source>
        <translation>Mi piace</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="311"/>
        <source>%1 like this comment</source>
        <comment>Plural: %1=list of people like John, Jane, Smith</comment>
        <translation>A %1 piace questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="305"/>
        <source>%1 likes this comment</source>
        <comment>Singular: %1=name of just 1 person</comment>
        <translation>A %1 piace questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="547"/>
        <source>WARNING: Delete comment?</source>
        <translation>ATTENZIONE: Cancellare il commento?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="548"/>
        <source>Are you sure you want to delete this comment?</source>
        <translation>Sei sicuro di voler cancellare questo commento?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Si, cancellalo</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="550"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>CommenterBlock</name>
    <message>
        <location filename="../src/commenterblock.cpp" line="131"/>
        <source>You can press Control+Enter to send the comment with the keyboard</source>
        <translation>Premi Control+Invio per inviare il commento con la tastiera</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="42"/>
        <source>Reload comments</source>
        <translation>Ricarica commenti</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="127"/>
        <location filename="../src/commenterblock.cpp" line="528"/>
        <source>Comment</source>
        <comment>Infinitive verb</comment>
        <translation>Commentare</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="138"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="141"/>
        <source>Press ESC to cancel the comment if there is no text</source>
        <translation>Premi ESC per annullare il commento, se non c&apos;è testo</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="375"/>
        <source>Check for comments</source>
        <translation>Controlla i commenti</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="379"/>
        <source>Show all %1 comments</source>
        <translation>Mostra tutti i %1 commenti</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="405"/>
        <source>Comments are not available</source>
        <translation>I commenti non sono disponibili</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="579"/>
        <source>Error: Already composing</source>
        <translation>Errore: stai già scrivendo</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="580"/>
        <source>You can&apos;t edit a comment at this time, because another comment is already being composed.</source>
        <translation>Non puoi modificare ora il commento, perchè ne stai già scrivendo un altro.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="595"/>
        <source>Editing comment</source>
        <translation>Modificando il commento</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="604"/>
        <source>Loading comments...</source>
        <translation>Caricamento commenti...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="671"/>
        <source>Posting comment failed.

Try again.</source>
        <translation>Invio del commento fallito.

Prova di nuovo.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="688"/>
        <source>An error occurred</source>
        <translation>Si è verificato un errore</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="712"/>
        <source>Sending comment...</source>
        <translation>Inviando il commento...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="717"/>
        <source>Updating comment...</source>
        <translation>Aggiornando il commento...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="726"/>
        <source>Comment is empty.</source>
        <translation>Il commento è vuoto.</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../src/composer.cpp" line="198"/>
        <source>Type a message here to post it</source>
        <translation>Scrivi qui un messaggio per pubblicarlo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="37"/>
        <source>Click here or press Control+N to post a note...</source>
        <translation>Clicca qui o premi Control+N per pubblicare una nota...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="41"/>
        <source>Symbols</source>
        <translation>Simboli</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="59"/>
        <source>Formatting</source>
        <translation>Formattazione</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="60"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="64"/>
        <source>Bold</source>
        <translation>Grassetto</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="69"/>
        <source>Italic</source>
        <translation>Corsivo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="74"/>
        <source>Underline</source>
        <translation>Sottolineato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="79"/>
        <source>Strikethrough</source>
        <translation>Barrato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="86"/>
        <source>Header</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="91"/>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="95"/>
        <source>Table</source>
        <translation>Tabella</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="99"/>
        <source>Preformatted block</source>
        <translation>Blocco preformattato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="103"/>
        <source>Quote block</source>
        <translation>Blocco citazioni</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="112"/>
        <source>Configure spell checking...</source>
        <translation>Configura il controllo ortografico ...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="120"/>
        <source>Make a link</source>
        <translation>Crea un link</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="125"/>
        <source>Insert an image from a web site</source>
        <translation>Inserisci un&apos;immagine da un sito web</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="130"/>
        <source>Insert line</source>
        <translation>Inserisci una linea</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="154"/>
        <source>Insert symbols</source>
        <translation>Inserisci simboli</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="371"/>
        <source>You can attach only one file.</source>
        <translation>È possibile allegare un solo file.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="379"/>
        <source>You cannot drop folders here, only a single file.</source>
        <translation>Non puoi trascinare le cartelle qui, solo un singolo file.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="529"/>
        <source>Insert as image?</source>
        <translation>Inserire come immagine?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="530"/>
        <source>The link you are pasting seems to point to an image.</source>
        <translation>Il collegamento che stai incollando sembra puntare ad un&apos;immagine.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="533"/>
        <source>Insert as visible image</source>
        <translation>Inserisci come immagine visibile</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="534"/>
        <source>Insert as link</source>
        <translation>Inserisci come collegamento</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="765"/>
        <source>Table Size</source>
        <translation>Dimensioni Tabella</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="769"/>
        <source>How many rows (height)?</source>
        <translation>Quante righe (altezza)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="780"/>
        <source>How many columns (width)?</source>
        <translation>Quante colonne (larghezza)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="872"/>
        <source>Type or paste a web address here.
You could also select some text first, to turn it into a link.</source>
        <translation>Scrivi o incolla un indirizzo web qui.
Puoi anche selezionare del testo, per convertirlo poi in un link.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="917"/>
        <source>Invalid link</source>
        <translation>Link non valido</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="918"/>
        <source>The text you entered does not look like a link.</source>
        <translation>Il testo che hai inserito non sembra un collegamento.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="921"/>
        <source>It should start with one of these types:</source>
        <comment>It = the link, from previous sentence</comment>
        <translation>Dovrebbe iniziare con uno di questi tipi:</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="935"/>
        <source>&amp;Use it anyway</source>
        <translation>&amp;Usalo comunque</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="936"/>
        <source>&amp;Enter it again</source>
        <translation>Inserisci di nuovo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="937"/>
        <source>&amp;Cancel link</source>
        <translation>&amp;Cancella link</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="971"/>
        <source>Type or paste the image address here.
The link must point to the image file directly.</source>
        <translation>Scrivi o incolla l&apos;indirizzo dell&apos;immagine qui.
Il link deve puntare direttamente all&apos;immagine.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1104"/>
        <source>Yes, but saving a &amp;draft</source>
        <translation>Sì,ma salvando una &amp;bozza</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="142"/>
        <source>&amp;Format</source>
        <comment>Button for text formatting and related options</comment>
        <translation>&amp;Formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="147"/>
        <source>Text Formatting Options</source>
        <translation>Opzioni di formattazione del testo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="162"/>
        <source>Paste Text Without Formatting</source>
        <translation>Incolla testo senza formattazione</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="204"/>
        <source>Type a comment here</source>
        <translation>Scrivi un commento qui</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="871"/>
        <source>Insert a link</source>
        <translation>Inserisci un link</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="887"/>
        <source>Make a link from selected text</source>
        <translation>Crea un link con il testo selezionato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="888"/>
        <source>Type or paste a web address here.
The selected text (%1) will be converted to a link.</source>
        <translation>Digita o incolla un indirizzo web qui.
Il testo selezionato (%1) sarà convertito in un link.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="970"/>
        <source>Insert an image from a URL</source>
        <translation>Inserisci un&apos;immagine da un URL</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="988"/>
        <source>Error: Invalid URL</source>
        <translation>Errore: URL non valido</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="989"/>
        <source>The address you entered (%1) is not valid.
Image addresses should begin with http:// or https://</source>
        <translation>L&apos;indirizzo inserito (%1) non è valido. Gli indirizzi di un&apos;immagine dovrebbero iniziare con http:// o https://</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1109"/>
        <source>Cancel message?</source>
        <translation>Cancellare il messaggio?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1110"/>
        <source>Are you sure you want to cancel this message?</source>
        <translation>Sei sicuro di voler cancellare questo messaggio?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1112"/>
        <source>&amp;Yes, cancel it</source>
        <translation>&amp;Si, cancellalo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="1114"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="245"/>
        <source>minutes</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="251"/>
        <source>Top</source>
        <translation>In alto</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="253"/>
        <source>Bottom</source>
        <translation>In basso</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="32"/>
        <source>Program Configuration</source>
        <translation>Configurazione del programma</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="289"/>
        <source>Timeline &amp;update interval</source>
        <translation>Intervallo di &amp;aggiornamento della timeline</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="292"/>
        <source>&amp;Tabs position</source>
        <translation>Posizione delle &amp;tab</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="294"/>
        <source>&amp;Movable tabs</source>
        <translation>&amp;Tab mobili</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="322"/>
        <source>Minor Feeds</source>
        <translation>Feed Minori</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="464"/>
        <source>&amp;Posts per page, main timeline</source>
        <translation>&amp;Elementi per pagina, timeline principale</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="407"/>
        <location filename="../src/configdialog.cpp" line="414"/>
        <source>posts</source>
        <comment>This goes after a number, like: 10 posts</comment>
        <translation>elementi</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="466"/>
        <source>Posts per page, &amp;other timelines</source>
        <translation>Elementi per pagina, &amp;altre timeline</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="539"/>
        <source>Ignore SSL errors in images</source>
        <translation>Ignora errori SSL nelle immagini</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="566"/>
        <source>Public posts as &amp;default</source>
        <translation>Messaggi pubblici come &amp;default</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="268"/>
        <source>Pro&amp;xy Settings</source>
        <translation>Impostazioni del Pro&amp;xy</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="297"/>
        <source>Network configuration</source>
        <translation>Configurazione di Rete</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="275"/>
        <source>Set Up F&amp;ilters</source>
        <translation>Imposta F&amp;iltri</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="299"/>
        <source>Filtering rules</source>
        <translation>Regole di filtraggio</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="430"/>
        <source>Highlighted activities, except mine</source>
        <translation>Attività selezionate, escluse le mie</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="431"/>
        <source>Any highlighted activity</source>
        <translation>Qualsiasi attività selezionata</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="432"/>
        <source>Always</source>
        <translation>Sempre</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="433"/>
        <source>Never</source>
        <translation>Mai</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="438"/>
        <source>characters</source>
        <comment>This is a suffix, after a number</comment>
        <translation>caratteri</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="477"/>
        <source>Snippet limit</source>
        <translation>Limite Snippet</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="313"/>
        <source>Post Titles</source>
        <translation>Titoli dei Post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="316"/>
        <source>Post Contents</source>
        <translation>Contenuto dei post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="319"/>
        <source>Comments</source>
        <translation>Commenti</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="347"/>
        <source>You are among the recipients of the activity, such as a comment addressed to you.</source>
        <translation>Sei tra i destinatari dell&apos;attività, per esempio un commento indirizzato a te.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="351"/>
        <source>Used also when highlighting posts addressed to you in the timelines.</source>
        <translation>Usato anche quando i post in evidenza sono indirizzati a te nelle timeline.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="356"/>
        <source>The activity is in reply to something done by you, such as a comment posted in reply to one of your notes.</source>
        <translation>L&apos;attività è in risposta a qualcosa fatto da te, come un commento postato in risposta a una delle tue note.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="362"/>
        <source>You are the object of the activity, such as someone adding you to a list.</source>
        <translation>Sei l&apos;oggetto dell&apos;attività, per esempio qualcuno ti ha aggiunto ad una lista.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="367"/>
        <source>The activity is related to one of your objects, such as someone liking one of your posts.</source>
        <translation>L&apos;attività è legata a uno dei tuoi oggetti, per esempio a qualcuno piace un tuo post.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="371"/>
        <source>Used also when highlighting your own posts in the timelines.</source>
        <translation>Usato anche per evidenziare i tuoi post nelle timeline.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="376"/>
        <source>Item highlighted due to filtering rules.</source>
        <translation>Oggetto evidenziato in base alle regole dei filtri.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="381"/>
        <source>Item is new.</source>
        <translation>Nuovo Elemento.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="475"/>
        <source>Show snippets in minor feeds</source>
        <translation>Mostra snippets nei feed minori</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="470"/>
        <source>Hide duplicated posts</source>
        <translation>Nascondi post duplicati</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="451"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="452"/>
        <source>Before avatar</source>
        <translation>Prima avatar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="453"/>
        <source>Before avatar, subtle</source>
        <translation>Prima dell'avatar, sottotitolo</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="454"/>
        <source>After avatar</source>
        <translation>Dopo avatar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="455"/>
        <source>After avatar, subtle</source>
        <translation>Dopo dell'avatar,sottotitolo</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="479"/>
        <source>Snippet limit when highlighted</source>
        <translation>Limite dello snippet quando evidenziato</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="483"/>
        <source>Show activity icons</source>
        <translation>Mostra icone attività</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="527"/>
        <source>Avatar size</source>
        <translation>Dimensione avatar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="531"/>
        <source>Show extended share information</source>
        <translation>Mostra informazioni di condivisione estese</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="533"/>
        <source>Show extra information</source>
        <translation>Mostra informazioni extra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="535"/>
        <source>Highlight post author&apos;s comments</source>
        <translation>Evidenzia i commenti dell&apos;autore del post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="537"/>
        <source>Highlight your own comments</source>
        <translation>Evidenza i tuoi commenti</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="571"/>
        <source>Show character counter</source>
        <translation>Mostra contatore di caratteri</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="596"/>
        <source>Don&apos;t inform followers when following someone</source>
        <translation>Non&apos;informare i follower quando seguono qualcuno</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="599"/>
        <source>Don&apos;t inform followers when handling lists</source>
        <translation>Non&apos;informare i follower durante la gestione degli elenchi</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="619"/>
        <source>As system notifications</source>
        <translation>Come notifiche di sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="621"/>
        <source>Using own notifications</source>
        <translation>Usando proprie notifiche</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="623"/>
        <source>Don&apos;t show notifications</source>
        <translation>Non mostrare notifiche</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="637"/>
        <source>seconds</source>
        <comment>Next to a duration, in seconds</comment>
        <translation>Accanto a una durata, in secondi</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="675"/>
        <source>Notification Style</source>
        <translation>Stile di notifica</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="679"/>
        <source>Duration</source>
        <translation>Durata</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="681"/>
        <source>Persistent Notifications</source>
        <translation>Notifiche persistenti</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="683"/>
        <source>Also highlight taskbar entry</source>
        <translation>Evidenzia anche la voce sulla barra delle applicazioni</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="689"/>
        <source>Notify when receiving:</source>
        <translation>Notifica quando ricevi:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="693"/>
        <source>New posts</source>
        <translation>Nuovi post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="695"/>
        <source>Highlighted posts</source>
        <translation>Post in evidenza</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="697"/>
        <source>New activities in minor feed</source>
        <translation>Nuove attività nel feed secondario</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="699"/>
        <source>Highlighted activities in minor feed</source>
        <translation>Attività in evidenza nel feed secondario</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="701"/>
        <source>Important errors</source>
        <translation>Errori importanti</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="716"/>
        <source>Default</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="717"/>
        <source>System iconset, if available</source>
        <translation>Icone di sistema, se disponibili</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="718"/>
        <source>Show your current avatar</source>
        <translation>Mostra il tuo avatar attuale</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="719"/>
        <source>Custom icon</source>
        <translation>Icona personalizzata</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="753"/>
        <source>System Tray Icon &amp;Type</source>
        <translation>&amp;Tipo di Icona di Sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="757"/>
        <source>Hide window on startup</source>
        <translation>Nascondi la finestra all'avvio</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="113"/>
        <source>General Options</source>
        <translation>Opzioni Generali</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="117"/>
        <source>Fonts</source>
        <translation>Font</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="121"/>
        <source>Colors</source>
        <translation>Colori</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="125"/>
        <source>Timelines</source>
        <translation>Timeline</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="129"/>
        <source>Posts</source>
        <translation>Post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="133"/>
        <source>Composer</source>
        <translation>Componi</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="137"/>
        <source>Privacy</source>
        <translation>Privacy</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="141"/>
        <source>Notifications</source>
        <translation>Notifiche</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="145"/>
        <source>System Tray</source>
        <translation>Icone di Sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="481"/>
        <source>Minor feed avatar sizes</source>
        <translation>Dimensioni avatar per feed minori</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="529"/>
        <source>Avatar size in comments</source>
        <translation>Dimensione dell'avatar nei commenti</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="726"/>
        <source>S&amp;elect...</source>
        <translation>S&amp;eleziona...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="755"/>
        <source>Custom &amp;Icon</source>
        <translation>&amp;Icona Personalizzata</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1027"/>
        <source>Select custom icon</source>
        <translation>Seleziona un&apos;icona personalizzata</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="175"/>
        <source>Dianara stores data in this folder:</source>
        <translation>Dianara salva i dati in questa cartella:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="255"/>
        <source>Left side</source>
        <comment>tabs on left side/west; RTL not affected</comment>
        <translation>A sinistra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="258"/>
        <source>Right side</source>
        <comment>tabs on right side/east; RTL not affected</comment>
        <translation>A destra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="468"/>
        <source>Show information for deleted posts</source>
        <translation>Mostra le informazioni per i post eliminati</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="472"/>
        <source>Jump to new posts line on update</source>
        <translation>Passa alla nuova riga di post sull'aggiornamento</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="515"/>
        <source>Only for images inserted from web sites.</source>
        <translation>Solo per immagini inserite da siti Web.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="518"/>
        <source>Use with care.</source>
        <translation>Usare con cura.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="541"/>
        <source>Show full size images</source>
        <translation>Mostra immagini a dimensione intera</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="568"/>
        <source>Use attachment filename as initial post title</source>
        <translation>Usa il nome del file dell&apos;allegato come titolo iniziale del post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="602"/>
        <source>Inform only the author when liking things</source>
        <translation>Informare solo l'autore quando si amano le cose</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="186"/>
        <source>&amp;Save Configuration</source>
        <translation>&amp;Salva Configurazione</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="193"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="814"/>
        <source>This is a system notification</source>
        <translation>Questa è una notifica di sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="818"/>
        <source>System notifications are not available!</source>
        <translation>Le notifiche di sistema non sono disponibili!</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="820"/>
        <source>Own notifications will be used.</source>
        <translation>Verranno utilizzate le proprie notifiche.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="830"/>
        <source>This is a basic notification</source>
        <translation>Questa è una notifica di base</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1029"/>
        <source>Image files</source>
        <translation>File immagine</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1032"/>
        <source>All files</source>
        <translation>Tutti i files</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1052"/>
        <source>Invalid image</source>
        <translation>Immagine non valida</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="1053"/>
        <source>The selected image is not valid.</source>
        <translation>L&apos;immagine selezionata non è valida.</translation>
    </message>
</context>
<context>
    <name>ContactCard</name>
    <message>
        <location filename="../src/contactcard.cpp" line="71"/>
        <source>Hometown</source>
        <translation>Città</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="78"/>
        <source>Joined: %1</source>
        <translation>Membro dal: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="86"/>
        <source>Updated: %1</source>
        <translation>Aggiornato: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="106"/>
        <source>Bio for %1</source>
        <comment>Abbreviation for Biography, but you can use the full word; %1=contact name</comment>
        <translation>Bio di %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="120"/>
        <source>This user doesn&apos;t have a biography</source>
        <translation>Questo utente non ha una biografia</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="125"/>
        <source>No biography for %1</source>
        <comment>%1=contact name</comment>
        <translation>Nessuna biografia per %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="154"/>
        <source>Open Profile in Web Browser</source>
        <translation>Apri il profilo nel browser web</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="165"/>
        <source>Send Message</source>
        <translation>Invia Messaggio</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="173"/>
        <source>Browse Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="197"/>
        <source>User Options</source>
        <translation>Opzioni utente</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="230"/>
        <source>Follow</source>
        <translation>Segui</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="242"/>
        <source>Stop Following</source>
        <translation>Smetti di seguire</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="306"/>
        <source>Stop following?</source>
        <translation>Smettere di seguire?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="307"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Sei sicuro di voler smettere di seguire %1?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="310"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Si, smetti di seguire</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="311"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <location filename="../src/contactlist.cpp" line="36"/>
        <source>Type a partial name or ID to find a contact...</source>
        <translation>Scrivi parzialmente un nome o l&apos;ID per trovare un contatto...</translation>
    </message>
    <message>
        <location filename="../src/contactlist.cpp" line="67"/>
        <source>F&amp;ull List</source>
        <translation>Lista Com&amp;pleta</translation>
    </message>
</context>
<context>
    <name>ContactManager</name>
    <message>
        <location filename="../src/contactmanager.cpp" line="54"/>
        <source>username@server.org or https://server.org/username</source>
        <translation>nomeutente@pumpserver.org o https://pumpserver.org/nomeutente</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="58"/>
        <source>&amp;Enter address to follow:</source>
        <translation>&amp;Inserisci l&apos;indirizzo da seguire:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="79"/>
        <source>&amp;Follow</source>
        <translation>&amp;Segui</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="129"/>
        <source>Reload Followers</source>
        <translation>Ricarica Followers</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="134"/>
        <source>Reload Following</source>
        <translation>Ricarica Following</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="140"/>
        <source>Export Followers</source>
        <translation>Esporta Followers</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="145"/>
        <source>Export Following</source>
        <translation>Esporta Following</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="151"/>
        <source>Reload Lists</source>
        <translation>Ricarica Liste</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="179"/>
        <source>&amp;Neighbors</source>
        <translation>&amp;Vicini</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="208"/>
        <source>Follo&amp;wers</source>
        <translation>Follo&amp;wers</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="213"/>
        <source>Followin&amp;g</source>
        <translation>Foll&amp;owing</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="218"/>
        <source>&amp;Lists</source>
        <translation>&amp;Liste</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="233"/>
        <source>Export list of &apos;following&apos; to a file</source>
        <translation>Esporta la lista dei &apos;Following&apos; in un file</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="234"/>
        <source>Export list of &apos;followers&apos; to a file</source>
        <translation>Esporta la lista dei &apos;Followers&apos; in un file</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="274"/>
        <source>Cannot export to this file:</source>
        <translation>Impossibile esportare in questo file:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="278"/>
        <source>Please enter another file name, or choose a different folder.</source>
        <translation>Inserisci un altro nome file o scegli una cartella diversa.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="519"/>
        <source>The server seems to be a Pump server, but the account does not exist.</source>
        <translation>Il server sembra essere un server Pump, ma l'account non esiste.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="524"/>
        <source>%1 doesn&apos;t seem to be a Pump server.</source>
        <comment>%1 is a hostname</comment>
        <translation>%1 non&apos;sembra essere un server Pump.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="534"/>
        <source>Following this account at this time will probably not work.</source>
        <translation>Seguire questo account in questo momento probabilmente non funzionerà.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="543"/>
        <source>The %1 server seems unavailable.</source>
        <comment>%1 is a hostname</comment>
        <translation>Il %1 server sembra non disponibile.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="551"/>
        <source>Unknown</source>
        <comment>Refers to server version</comment>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="273"/>
        <location filename="../src/contactmanager.cpp" line="556"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="558"/>
        <source>The user address %1 does not exist, or the %2 server is down.</source>
        <translation>L'indirizzo utente %1 non esiste oppure il %2 server non è attivo.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="566"/>
        <source>Server version</source>
        <translation>Versione Server</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="570"/>
        <source>Check the address, and keep in mind that usernames are case-sensitive.</source>
        <translation>Controlla l'indirizzo e tieni presente che i nomi utente fanno distinzione tra maiuscole e minuscole.</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="573"/>
        <source>Do you want to try following this address anyway?</source>
        <translation>Vuoi provare a seguire questo indirizzo comunque?</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="575"/>
        <source>(not recommended)</source>
        <translation>(non consigliato)</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="578"/>
        <source>Yes, follow anyway</source>
        <translation>Sì, segui comunque</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="579"/>
        <source>No, cancel</source>
        <translation>No, cancella</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="598"/>
        <source>About to follow %1...</source>
        <translation>Informazioni da seguire %1...</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../src/downloadwidget.cpp" line="47"/>
        <source>Open</source>
        <comment>Verb, as in: Open the downloaded file</comment>
        <translation>Apri</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="58"/>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="61"/>
        <source>Save the attached file to your folders</source>
        <translation>Salva il file allegato nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="74"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="144"/>
        <source>Save File As...</source>
        <translation>Salva File Come...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="147"/>
        <source>All files</source>
        <translation>Tutti i files</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="198"/>
        <source>File not found!</source>
        <translation>File non trovato!</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="209"/>
        <source>Abort download?</source>
        <translation>Fermare download?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="210"/>
        <source>Do you want to stop downloading the attached file?</source>
        <translation>Vuoi fermare il download del file allegato?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="213"/>
        <source>&amp;Yes, stop</source>
        <translation>&amp;Si, ferma</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="214"/>
        <source>&amp;No, continue</source>
        <translation>&amp;No, continua</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="222"/>
        <source>Download aborted</source>
        <translation>Download fermato</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="240"/>
        <source>Download completed</source>
        <translation>Download completato</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="244"/>
        <source>Attachment downloaded successfully to %1</source>
        <comment>%1 = filename</comment>
        <translation>Allegato scaricato correttamente su %1</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="250"/>
        <source>Open the downloaded attachment with your system&apos;s default program for this type of file.</source>
        <translation>Apri l'allegato scaricato con il programma predefinito del tuo sistema&apos;per questo tipo di file.</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="265"/>
        <source>Download failed</source>
        <translation>Download fallito</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="272"/>
        <source>Downloading attachment failed: %1</source>
        <comment>%1 = filename</comment>
        <translation>Download allegato non riuscito: %1</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="305"/>
        <source>Downloading %1 KiB...</source>
        <translation>Scaricando %1 KiB...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="308"/>
        <source>%1 KiB downloaded</source>
        <translation>%1 KiB scaricati</translation>
    </message>
</context>
<context>
    <name>DraftsManager</name>
    <message>
        <location filename="../src/draftsmanager.cpp" line="28"/>
        <source>Draft Manager</source>
        <translation>Responsabile del progetto</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="44"/>
        <source>Load</source>
        <translation>Carica</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="53"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="62"/>
        <source>Manage drafts...</source>
        <translation>Gestisci bozze...</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="87"/>
        <source>&amp;Delete selected draft</source>
        <translation>&amp;Cancella bozza selezionata</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="94"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="311"/>
        <source>Untitled draft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="328"/>
        <source>Delete draft?</source>
        <translation>Cancella bozza?</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="329"/>
        <source>Are you sure you want to delete this draft?</source>
        <translation>Sei sicuro di voler eliminare questa bozza?</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Si, cancella</translation>
    </message>
    <message>
        <location filename="../src/draftsmanager.cpp" line="331"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>EmailChanger</name>
    <message>
        <location filename="../src/emailchanger.cpp" line="28"/>
        <source>Change E-mail Address</source>
        <translation>Cambia indirizzo email</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="68"/>
        <source>Change</source>
        <translation>Cambia</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="76"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="97"/>
        <source>E-mail Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="98"/>
        <source>Again:</source>
        <translation>Ancora:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="99"/>
        <source>Your Password:</source>
        <translation>Tua password:</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="150"/>
        <source>E-mail addresses don&apos;t match!</source>
        <translation>Gli indirizzi e-mail non&apos;corrispondono!</translation>
    </message>
    <message>
        <location filename="../src/emailchanger.cpp" line="159"/>
        <source>Password is empty!</source>
        <translation>La password è vuota</translation>
    </message>
</context>
<context>
    <name>FDNotifications</name>
    <message>
        <location filename="../src/notifications.cpp" line="209"/>
        <source>Show</source>
        <translation>Mostra</translation>
    </message>
</context>
<context>
    <name>FilterEditor</name>
    <message>
        <location filename="../src/filtereditor.cpp" line="28"/>
        <source>Filter Editor</source>
        <translation>Modifica Filtri</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="84"/>
        <source>Application</source>
        <translation>Applicazione</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="44"/>
        <source>%1 if %2 contains: %3</source>
        <comment>This explains a filter rule, like: Hide if Author ID contains JohnDoe</comment>
        <translation>%1 se %2 contiene: %3</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="51"/>
        <source>Here you can set some rules for hiding or highlighting stuff. You can filter by content, author or application.

For instance, you can filter out messages posted by the application Open Farm Game, or which contain the word NSFW in the message. You could also highlight messages that contain your name.</source>
        <translation>Qui puoi impostare regole per nascondere o mettere in evidenza le attività. Puoi filtrare per contenuto, autore o applicazione.

Per esempio, puoi filtrare messaggi postati dall&apos;applicazione Open Farm Game, o quelli che contengono la parola NSFW nel messaggio. Puoi anche evidenziare i messaggi che contengono il tuo nome.</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="69"/>
        <source>Hide</source>
        <translation>Nascondi</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="72"/>
        <source>Highlight</source>
        <translation>Evidenzia</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="78"/>
        <source>Post Contents</source>
        <translation>Contenuto del post</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="81"/>
        <source>Author ID</source>
        <translation>ID dell&apos;autore</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="87"/>
        <source>Activity Description</source>
        <translation>Descrizione dell&apos;attività</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="90"/>
        <source>Keywords...</source>
        <translation>Parole chiave...</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="95"/>
        <source>&amp;Add Filter</source>
        <translation>&amp;Aggiungi Filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="108"/>
        <source>Filters in use</source>
        <translation>Filtri in uso</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="114"/>
        <source>&amp;Remove Selected Filter</source>
        <translation>&amp;Rimuovi i Filtri Selezionati</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="125"/>
        <source>&amp;Save Filters</source>
        <translation>&amp;Salva Filtri</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="132"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="153"/>
        <source>if</source>
        <translation>se</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="157"/>
        <source>contains</source>
        <translation>contiene</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="163"/>
        <source>&amp;New Filter</source>
        <translation>&amp;Nuovo Filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="170"/>
        <source>C&amp;urrent Filters</source>
        <translation>Filt&amp;ri Salvati</translation>
    </message>
</context>
<context>
    <name>FilterMatchesWidget</name>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="37"/>
        <source>Content</source>
        <comment>The contents of the post matched</comment>
        <translation>I contenuti del post corrispondono</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="45"/>
        <source>Author</source>
        <translation>Autore</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="52"/>
        <source>App</source>
        <comment>Application, short if possible</comment>
        <translation>App</translation>
    </message>
    <message>
        <location filename="../src/filtermatcheswidget.cpp" line="60"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
</context>
<context>
    <name>FirstRunWizard</name>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="30"/>
        <source>Welcome Wizard</source>
        <translation>Procedura guidata di benvenuto</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="41"/>
        <source>Welcome to Dianara!</source>
        <translation>Benvenuti a Dianara!</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="44"/>
        <source>This wizard will help you get started.</source>
        <translation>Questa procedura guidata ti aiuterà ad iniziare.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="47"/>
        <source>You can access this window again at any time from the Help menu.</source>
        <translation>È possibile accedere nuovamente a questa finestra in qualsiasi momento dal menu Guida.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="50"/>
        <source>The first step is setting up your account, by using the following button:</source>
        <translation>Il primo passo è configurare il tuo account, usando il seguente pulsante:</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="61"/>
        <source>Configure your &amp;account</source>
        <translation>Configura il tuo &amp;account</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="69"/>
        <source>Once you have configured your account, it&apos;s recommended that you edit your profile and add an avatar and some other information, if you haven&apos;t done so already.</source>
        <translation>Dopo aver configurato il tuo account, &apos; ti consigliamo di modificare il tuo profilo e aggiungere un avatar e alcune altre informazioni, se non l'hai&apos; già fatto.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="80"/>
        <source>&amp;Edit your profile</source>
        <translation>&amp;Modifica il tuo profilo</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="89"/>
        <source>By default, Dianara will post only to your followers, but it&apos;s recommended that you post to Public, at least sometimes.</source>
        <translation>Per impostazione predefinita, Dianara pubblicherà solo i tuoi follower, ma ti&apos; consiglia di pubblicare in pubblico, almeno a volte.</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="97"/>
        <source>Post to &amp;Public by default</source>
        <translation>Posta come &amp;Publico di default</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="106"/>
        <source>Open general program &amp;help window</source>
        <translation>Aprire il programma generale&amp;finestra di aiuto</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="114"/>
        <source>&amp;Show this again next time Dianara starts</source>
        <translation>&amp;Mostra di nuovo la prossima volta che Dianara inizia</translation>
    </message>
    <message>
        <location filename="../src/firstrunwizard.cpp" line="120"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
</context>
<context>
    <name>FontPicker</name>
    <message>
        <location filename="../src/fontpicker.cpp" line="50"/>
        <source>Change...</source>
        <translation>Cambia...</translation>
    </message>
    <message>
        <location filename="../src/fontpicker.cpp" line="97"/>
        <source>Choose a font</source>
        <translation>Scegli un font</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>Basic Help</source>
        <translation>Aiuto di Base</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="39"/>
        <source>Getting started</source>
        <translation>Per iniziare</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="73"/>
        <source>The first time you start Dianara, you should see the Account Configuration dialog. There, enter your Pump.io address as name@server and press the Get Verifier Code button.</source>
        <translation>La prima volta che fai partire Dianara, dovresti vedere la finestra di Configurazione dell&apos;Account. Inserisci il tuo indirizzo Pump.io come nome@server e premi Ottieni Codice di Verifica.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="78"/>
        <source>Then, your usual web browser should load the authorization page in your Pump.io server. There, you&apos;ll have to copy the full VERIFIER code, and paste it into Dianara&apos;s second field. Then press Authorize Application, and once it&apos;s confirmed, press Save Details.</source>
        <translation>Poi, il tuo browser predefinito dovrebbe caricare la pagina di autorizzazione nel tuo server Pump.io. Qui, dovrai copiare il VERIFIER code, e incollarlo nel secondo campo di Dianara. Quindi premi Autorizza Applicazione e, una volta confermato, premi Salva Dettagli.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="84"/>
        <source>At this point, your profile, contact lists and timelines will be loaded.</source>
        <translation>A questo punto il tuo profilo, le liste contatti e le timeline saranno caricate.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="87"/>
        <source>You should take a look at the Program Configuration window, under the Settings - Configure Dianara menu. There are several interesting options there.</source>
        <translation>Dovresti dare uno sguardo alla finestra di Configurazione del Programma, sotto Impostazioni - Configura Dianara. Ci sono diverse opzioni interessanti là.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="110"/>
        <source>You can configure several things to your liking in the settings, like the time interval between timeline updates, how many posts per page you want, highlight colors, notifications or how the system tray icon looks.</source>
        <translation>Puoi configurare diverse cose secondo le tue preferenze, come il tempo di aggiornamento delle timeline, quanti post per pagina vuoi vedere, colori di evidenziazione, notifiche o come visualizzare l&apos;icona di sistema.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Timelines</source>
        <translation>Timeline</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation>Contenuti</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="91"/>
        <source>Keep in mind that there are a lot of places in Dianara where you can get more information by hovering over some text or button with your mouse, and waiting for the tooltip to appear.</source>
        <translation>Tieni presente che ci sono molti posti in Dianara dove puoi ottenere più informazioni fermandoti sopra alcuni testi o bottoni col mouse, e aspettare che il suggerimento appaia.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="115"/>
        <source>Here, you can also activate the option to always publish your posts as Public by default. You can always change that at the moment of posting.</source>
        <translation>Qui puoi anche attivare l&apos;opzione di pubblicare sempre i tuoi post pubblici per default. Puoi sempre cambiare questa preferenza al momento di postare.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="131"/>
        <source>The main timeline, where you&apos;ll see all the stuff posted or shared by the people you follow.</source>
        <translation>La timeline principale, dove puoi vedere tutti gli elementi postati o condivisi da persone che segui.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="134"/>
        <source>Messages timeline, where you&apos;ll see messages sent to you specifically. These messages might have been sent to other people too.</source>
        <translation>La timeline dei messaggi, dove vedrai messaggi inviati a te specificatamente. Questi messaggi potrebbero essere stati inviati anche ad altre persone.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="138"/>
        <source>Activity timeline, where you&apos;ll see your own posts, or posts shared by you.</source>
        <translation>La timeli delle attività, dove vedrai i tuoi post e i post che hai condiviso.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="141"/>
        <source>Favorites timeline, where you&apos;ll see the posts and comments you&apos;ve liked. This can be used as a bookmark system.</source>
        <translation>La timeline dei preferiti, dove vedrai i post e i commenti che ti piacciono. Può essere usata come sistema di bookmark.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="158"/>
        <source>These activities might have a &apos;+&apos; button in them. Press it to open the post they&apos;re referencing. Also, as in many other places, you can hover with your mouse to see relevant information in the tooltip.</source>
        <translation>Queste attività potrebbero avere un &apos;+&apos; vicino. Premilo per aprire il post al quale sono riferite. Anche qui potrai avere ulteriori informazioni se rimani fermo col mouse sul pulsante.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="233"/>
        <source>Under the &apos;Neighbors&apos; tab you&apos;ll see some resources to find people, and have the option to browse the latest registered users from your server directly.</source>
        <translation>Nella &apos;scheda Vicini&apos; vedrai alcune risorse per trovare persone e avrai la possibilità di sfogliare direttamente gli ultimi utenti registrati dal tuo server.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="316"/>
        <source>Dianara offers a D-Bus interface that allows some control from other applications.</source>
        <translation>Dianara offre un'interfaccia D-Bus che consente un certo controllo da altre applicazioni.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="319"/>
        <source>The interface is at %1, and you can access it with tools such as %2 or %3. It offers methods like %4 and %5.</source>
        <translation>L'interfaccia è su %1 e puoi accedervi con strumenti come %2 o%3.Offre metodi come %4 e %5.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Posting</source>
        <translation>Postare</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="96"/>
        <source>If you&apos;re new to Pump.io, take a look at this guide:</source>
        <translation>Se sei nuovo su Pump.io, dai uno sguardo a questa guida:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="163"/>
        <source>New messages appear highlighted in a different color. You can mark them as read just by clicking on any empty parts of the message.</source>
        <translation>Nuovi messaggi appaiono evidenziati in un colore diffeerente. Puoi segnarli come già letti cliccando in una parte vuota del messaggio.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="176"/>
        <source>You can post notes by clicking in the text field at the top of the window or by pressing Control+N. Setting a title for your post is optional, but highly recommended, as it will help to better identify references to your post in the minor feed, e-mail notifications, etc.</source>
        <translation>Puoi postare note cliccando il campo di testo in alto oppure premendo Control+N. Impostare un titolo per il post è opzionale, ma caldamente consigliato, perchè aiuta gli altri a identificarlo nella timeline laterale, nelle notifiche mail, ecc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>It is possible to attach images, audio, video, and general files, like PDF documents, to your post.</source>
        <translation>Allegare immagini, video o audio ed altri file come PDF è ora possibile.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="192"/>
        <source>You can use the Format button to add formatting to your text, like bold or italics. Some of these options require text to be selected before they are used.</source>
        <translation>Puoi usare il pulsante Formattazione per formattare il tuo testo, per esempio renderlo grassetto o corsivo. Alcune di queste opzioni richiedono che del testo sia selezionato prima che siano usate.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="199"/>
        <source>If you add a specific person to the &apos;To&apos; list, they will receive your message in their direct messages tab.</source>
        <translation>Se aggiungi specifiche persone alla lista A, riceveranno una notifica del tuo messaggio nella tab dei messaggi diretti.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="210"/>
        <source>You can create private messages by adding specific people to these lists, and unselecting the Followers or the Public options.</source>
        <translation>Puoi creare messaggi privati aggiungendo specifiche persone a queste liste e rimuovendo Followers e Pubblico.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Managing contacts</source>
        <translation>Gestire i contatti</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="223"/>
        <source>You can see the lists of people you follow, and who follow you from the Contacts tab.</source>
        <translation>Puoi vedere le liste di persone che segui e che ti seguono dalla tab Contatti.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="226"/>
        <source>There, you can also manage person lists, used mainly to send posts to specific groups of people.</source>
        <translation>Qui puoi anche gestire liste di persone, usate principalmente per inviare post a specifici gruppi.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="237"/>
        <source>You can click on any avatars in the posts, the comments, and the Meanwhile column, and you will get a menu with several options, one of which is following or unfollowing that person.</source>
        <translation>Puoi cliccare su qualsiasi avatar nei post, nei commenti, e nella timeline laterale e comparirà un menù con diverse opzioni, una di queste serve per seguire o smettere di seguire quella persona.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Keyboard controls</source>
        <translation>Controlli da Tastiera</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="98"/>
        <source>Pump.io User Guide</source>
        <translation>Guida Utente Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="128"/>
        <source>There are seven timelines:</source>
        <translation>Ci sono sette timeline:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="153"/>
        <source>The sixth and seventh timelines are also minor timelines, similar to the Meanwhile, but containing only activities directly addressed to you (Mentions) and activities done by you (Actions).</source>
        <translation>La sesta e la settima timeline sono timeline minori o secondarie, simili a &quot;Nel Frattempo&quot;, ma contiene solo attività indirizzate direttamente a te (Menzioni) e attività create da te (Azioni).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="196"/>
        <source>You can select who will see your post by using the To and Cc buttons.</source>
        <translation>Puoi selezionare chi vedrà il tuo post usando i pulsanti A e Cc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="202"/>
        <source>You can also type &apos;@&apos; and the first characters of the name of a contact to bring up a popup menu with matching choices.</source>
        <translation>Puoi anche digitare &apos;@&apos; e il primo carattere del nome di un contatto per far apparire un popup con le scelte corrispondenti.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="242"/>
        <source>You can also send a direct message (initially private) to that contact from this menu.</source>
        <translation>Puoi anche inviare un messaggio diretto (inizialmente privato) al contatto da questo menu.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="245"/>
        <source>You can find a list with some Pump.io users and other information here:</source>
        <translation>Puoi trovare una lista con alcuni utenti Pump.io ed altre informazioni qui:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="249"/>
        <source>Users by language</source>
        <translation>Utenti per lingua</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="251"/>
        <source>Followers of Pump.io Community account</source>
        <translation>Followers dell'account Community Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="263"/>
        <source>The most common actions found on the menus have keyboard shortcuts written next to them, like F5 or Control+N.</source>
        <translation>Le azioni più comuni che si trovano nei menù hanno scorciatorie da tastiera scritte vicino a loro, come F5 o Control+N.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="267"/>
        <source>Besides that, you can use:</source>
        <translation>Oltre a quello, puoi usare:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="270"/>
        <source>Control+Up/Down/PgUp/PgDown/Home/End to move around the timeline.</source>
        <translation>Control+Su/Giù/PgSu/PgGiù/Home/Fine per muoverti nelle timeline.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="273"/>
        <source>Control+Left/Right to jump one page in the timeline.</source>
        <translation>Control+Sinistra/Destra per saltare da una pagina all&apos;altra nella timeline.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="276"/>
        <source>Control+G to go to any page in the timeline directly.</source>
        <translation>Control+G va in qualsiasi pagina della timeline direttamente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="279"/>
        <source>Control+1/2/3 to switch between the minor feeds.</source>
        <translation>Control+1/2/3 per spostarti tra i feed minori.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="282"/>
        <source>Control+Enter to post, when you&apos;re done composing a note or a comment. If the note is empty, you can cancel it by pressing ESC.</source>
        <translation>Control+Invio per inviare il post quando hai terminato di comporre la nota o il commento. Se la nota è vuota, puoi cancellarla premendo ESC.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation>Opzioni da riga di comando</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="146"/>
        <source>The fifth timeline is the minor timeline, also known as the Meanwhile. This is visible on the left side, though it can be hidden. Here you&apos;ll see minor activities done by everyone you follow, such as comment actions, liking posts or following people.</source>
        <comment>LEFT SIDE should change to RIGHT SIDE on RTL languages</comment>
        <translation type="unfinished">La quinta timeline è la timeline minore, conosciuta anche come Meanwhile (nel frattempo). Questa è visibile a sinistra, ma può anche essere nascosta. Qui vedrai attività di tutte le persone che segui, come commenti, mi piace e segui persone.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="206"/>
        <source>Choose one with the arrow keys and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation>Scegline uno con i tasti freccia e premi Invio per completare un nome. Questo aggiungerà quella persona alla lista di destinatari.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="229"/>
        <source>There is a text field at the top, where you can directly enter addresses of new contacts to follow them.</source>
        <translation>C'è un campo di testo in alto, dove puoi inserire direttamente gli indirizzi di nuovi contatti per seguirli.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="286"/>
        <source>While composing a note, press Enter to jump from the title to the message body. Also, pressing the Up arrow while you&apos;re at the start of the message, jumps back to the title.</source>
        <translation>Mentre componi una nota, premi Invio per saltare dal titolo al messaggio. Premendo la freccia Su mentre sei all&apos;inizio del messaggio, torni al titolo.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="291"/>
        <source>Control+Enter to finish creating a list of recipients for a post, in the &apos;To&apos; or &apos;Cc&apos; lists.</source>
        <translation>Control+Invio per terminare la creazione della lista di destinatari per il post, per le liste &apos;A&apos; o &apos;Cc&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="304"/>
        <source>You can use the --config parameter to run the program with a different configuration. This can be useful to use two or more different accounts. You can even run two instances of Dianara at the same time.</source>
        <translation>Puoi usare il parametro --config per avviare il programma con una configurazione differente. Questo può essere utile per usare due o più account. Puoi anche avviare due istanze di Dianara contemporaneamente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="309"/>
        <source>Use the --debug parameter to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Usa il parametro --debug per avere informazioni extra nel tuo terminale, su quello che il programma sta facendo.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="312"/>
        <source>If your server does not support HTTPS, you can use the --nohttps parameter.</source>
        <translation>Se il tuo server non supporta HTTPS, puoi utilizzare il parametro --nohttps.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="326"/>
        <source>If you use an alternate configuration, with something like &apos;--config otherconf&apos;, then the interface will be at org.nongnu.dianara_otherconf.</source>
        <translation>Se usi una configurazione alternativa, con qualcosa come -configura altre configurazioni, l'interfaccia sarà su org.nongnu.dianara_otherconf.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="343"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../src/imageviewer.cpp" line="33"/>
        <source>Image</source>
        <translation>Immagine</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="31"/>
        <source>Untitled</source>
        <translation>Senza titolo</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="67"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Salva come...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="80"/>
        <source>&amp;Restart</source>
        <comment>Restart animation</comment>
        <translation>&amp;Riavvia</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="90"/>
        <source>Fit</source>
        <comment>As in: fit image to window</comment>
        <translation>Adatta</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="137"/>
        <source>Rotate image to the left</source>
        <comment>RTL: This actually means LEFT, anticlockwise</comment>
        <translation>Ruota immagine a sinistra</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="147"/>
        <source>Rotate image to the right</source>
        <comment>RTL: This actually means RIGHT, clockwise</comment>
        <translation>Ruota immagine a destra</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="160"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="296"/>
        <source>Save Image...</source>
        <translation>Salvataggio Immagine...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="336"/>
        <source>Close Viewer</source>
        <translation>Chiudi Visualizzatore</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="377"/>
        <source>Downloading full image...</source>
        <translation>Download immagine completa...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="523"/>
        <source>Error downloading image!</source>
        <translation>Errore durante il download dell'immagine!</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="525"/>
        <source>Try again later.</source>
        <translation>Riprova più tardi.</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="536"/>
        <source>Save Image As...</source>
        <translation>Salva Immagine Come...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="539"/>
        <source>Image files</source>
        <translation>Files Immagine</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="540"/>
        <source>All files</source>
        <translation>Tutti i Files</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="551"/>
        <source>Error saving image</source>
        <translation>Errore durante il salvataggio dell&apos;immagine</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="552"/>
        <source>There was a problem while saving %1.

Filename should end in .jpg or .png extensions.</source>
        <translation>C&apos;è stato un problema salvando %1.

L&apos;estensione del file dovrebbe essere .jpg o .png.</translation>
    </message>
</context>
<context>
    <name>ListsManager</name>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="39"/>
        <source>Members</source>
        <translation>Membri</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="52"/>
        <source>Add Mem&amp;ber</source>
        <translation>Aggiungi Mem&amp;bro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="60"/>
        <source>&amp;Remove Member</source>
        <translation>&amp;Rimuovi Membro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="69"/>
        <source>&amp;Delete Selected List</source>
        <translation>&amp;Cancella la Lista Selezionata</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="77"/>
        <source>Add New &amp;List</source>
        <translation>Aggiungi Nuova &amp;Lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="96"/>
        <source>Create L&amp;ist</source>
        <translation>Crea L&amp;ista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="104"/>
        <source>&amp;Add to List</source>
        <translation>&amp;Aggiungi alla Lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="316"/>
        <source>Are you sure you want to delete %1?</source>
        <comment>1=Name of a person list</comment>
        <translation>Sei sicuro di voler cancellare %1?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="451"/>
        <source>Remove person from list?</source>
        <translation>Rimuovere la persona dalla lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="452"/>
        <source>Are you sure you want to remove %1 from the %2 list?</source>
        <comment>1=Name of a person, 2=name of a list</comment>
        <translation>Sei sicuro di voler rimuovere %1 dalla lista %2?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;Yes</source>
        <translation>&amp;Si</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="80"/>
        <source>Type a name for the new list...</source>
        <translation>Digita un nome per la nuova lista...</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="89"/>
        <source>Type an optional description here</source>
        <translation>Digita una descrizione opzionale qui</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="315"/>
        <source>WARNING: Delete list?</source>
        <translation>ATTENZIONE: Cancellare la lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Si, cancellala</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="319"/>
        <location filename="../src/listsmanager.cpp" line="457"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../src/logviewer.cpp" line="25"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="54"/>
        <source>Clear &amp;Log</source>
        <translation>Pulisci &amp;Log</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="61"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="1130"/>
        <source>Status &amp;Bar</source>
        <translation>&amp;Barra di stato</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2677"/>
        <source>Total posts: %1</source>
        <translation>Post totali: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2685"/>
        <source>&amp;Timeline</source>
        <translation>T&amp;imeline</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2687"/>
        <source>The main timeline</source>
        <translation>Timeline principale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2703"/>
        <source>&amp;Activity</source>
        <translation>&amp;Attività</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2705"/>
        <source>Your own posts</source>
        <translation>I tuoi messaggi</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2714"/>
        <source>Your favorited posts</source>
        <translation>I tuoi messaggi preferiti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2694"/>
        <source>&amp;Messages</source>
        <translation>&amp;Messaggi</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2696"/>
        <source>Messages sent explicitly to you</source>
        <translation>Messaggi inviati esplicitamente a te</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="429"/>
        <source>&amp;Contacts</source>
        <translation>&amp;Contatti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="741"/>
        <location filename="../src/mainwindow.cpp" line="1425"/>
        <source>Initializing...</source>
        <translation>Inizializzando...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="819"/>
        <source>Your account is not configured yet.</source>
        <translation>Il tuo account non è ancora stato configurato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="432"/>
        <source>The people you follow, the ones who follow you, and your person lists</source>
        <translation>Le persone che segui, quelle che ti seguono, e le tue liste di persone</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="962"/>
        <source>&amp;Session</source>
        <translation>&amp;Sessione</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1060"/>
        <source>Auto-update &amp;Timelines</source>
        <translation>Auto-aggiorna &amp;Timeline</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Mark All as Read</source>
        <translation>Segna tutti come letti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1081"/>
        <source>&amp;Post a Note</source>
        <translation>&amp;Pubblica una nota</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1092"/>
        <source>&amp;Quit</source>
        <translation>&amp;E&amp;sci</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1103"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizzazione</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1168"/>
        <source>Locked Panels and Toolbars</source>
        <translation>Pannelli e barre degli strumenti bloccati</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1106"/>
        <source>Side Panel</source>
        <translation>Pannello laterale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="767"/>
        <source>%1 started.</source>
        <comment>1=program name and version</comment>
        <translation>%1 è partito.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="770"/>
        <source>Running with Qt v%1 on %2.</source>
        <comment>1=Qt version, 2=OS name</comment>
        <translatorcomment>FIXME</translatorcomment>
        <translation>In funzione con Qt v%1, %2.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="785"/>
        <source>Spell checking has been disabled because there are no Sonnet backends available!</source>
        <translation>Il controllo ortografico è stato disabilitato perché non sono disponibili backend Sonnet!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="790"/>
        <source>Spell checking available for %1 languages, using %2 by default.</source>
        <comment>%1 is the number of languages, %2 is a language code</comment>
        <translation>Controllo ortografico disponibile per %1 lingue, usando %2 per impostazione predefinita.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1143"/>
        <source>Full &amp;Screen</source>
        <translation>Sc&amp;hermo Intero</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1155"/>
        <source>&amp;Log</source>
        <translation>&amp;Log</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1182"/>
        <source>S&amp;ettings</source>
        <translation>Configura&amp;zione</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1186"/>
        <source>Edit &amp;Profile</source>
        <translation>Modifica &amp;profilo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <source>&amp;Account</source>
        <translation>A&amp;ccount</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1231"/>
        <source>Basic &amp;Help</source>
        <translation>Ai&amp;uto di Base</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1260"/>
        <source>Report a &amp;Bug</source>
        <translation>Riporta un &amp;Bug</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1272"/>
        <source>Pump.io User &amp;Guide</source>
        <translation>&amp;Guida Utente Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1288"/>
        <source>List of Some Pump.io &amp;Users</source>
        <translation>Elenco di alcuni &amp;Utenti Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1296"/>
        <source>Pump.io &amp;Network Status Website</source>
        <translation>Sito Web &amp;sullo stato della rete Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1432"/>
        <source>Auto-updating enabled</source>
        <translation>Auto-aggiornamenti abilitati</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1439"/>
        <source>Auto-updating disabled</source>
        <translation>Autp-aggiornamenti disabilitati</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1991"/>
        <source>Proxy password required</source>
        <translation>Password Proxy Richiesta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1992"/>
        <source>You have configured a proxy server with authentication, but the password is not set.</source>
        <translation>Hai configurato un server proxy con autenticazione, ma la password non è impostata.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1997"/>
        <source>Enter the password for your proxy server:</source>
        <translation>Inserisci la password del tuo server proxy:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2086"/>
        <source>Your biography is empty</source>
        <translation>La tua biografia è vuota</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2117"/>
        <source>Click to edit your profile</source>
        <translation>Clicca per modificare il tuo profilo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2140"/>
        <source>Starting automatic update of timelines, once every %1 minutes.</source>
        <translation>Attiva l'&apos;aggiornamento automatico delle timeline, ogni %1 minuti.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2147"/>
        <source>Stopping automatic update of timelines.</source>
        <translation>Ferma l&apos;aggiornamento automatico delle timeline.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2408"/>
        <source>Received %1 older posts in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of a timeline</comment>
        <translation>Hai ricevuto %1 post precedenti in &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2448"/>
        <source>1 highlighted</source>
        <comment>singular, refers to a post</comment>
        <translation>1 evidenziato</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2453"/>
        <source>%1 highlighted</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 evidenziati</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2443"/>
        <source>Direct messages</source>
        <translation>Messaggi diretti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2444"/>
        <source>By filters</source>
        <translation>Dai filtri</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2485"/>
        <source>1 more pending to receive.</source>
        <comment>singular, one post</comment>
        <translation>1 in attesa di ricezione.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2490"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation>%1 in attesa di ricezione.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2502"/>
        <location filename="../src/mainwindow.cpp" line="2919"/>
        <source>Also:</source>
        <translation>Anche:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2624"/>
        <source>Last update: %1</source>
        <translation>Ultimo aggiornamento: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2417"/>
        <location filename="../src/mainwindow.cpp" line="2855"/>
        <source>&apos;%1&apos; updated.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>&apos;%1&apos; aggiornato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2847"/>
        <source>Received %1 older activities in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation>Ricevute %1 attività precedenti in &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2902"/>
        <source>1 more pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation>1 in attesa di ricezione.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2907"/>
        <source>%1 more pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation>%1 in attesa di ricezione.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3486"/>
        <source>Dianara is Free Software, licensed under the GNU GPL license, and uses some Oxygen icons under LGPL license.</source>
        <translation type="unfinished">Dianara è un software libero, concesso in licenza con licenza GNU GPL, e utilizza alcune icone di Oxigen con licenza LGPL.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3642"/>
        <source>Shutting down Dianara...</source>
        <translation>Arresto di Dianara...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3721"/>
        <source>System tray icon is not available.</source>
        <translation>L&apos;icona di sistema non è disponibile.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3723"/>
        <source>Dianara cannot be hidden in the system tray.</source>
        <translation>Dianara non può essere nascosto nella barra delle notifiche di sistema.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3726"/>
        <source>Do you want to close the program completely?</source>
        <translation>Vuoi chiudere completamente il programma?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2423"/>
        <source>Timeline updated at %1.</source>
        <translation>Timeline aggiornata alle %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="726"/>
        <source>Press F1 for help</source>
        <translation>Premi F1 per aiuto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="825"/>
        <source>Click here to configure your account</source>
        <translation >Fai clic qui per configurare il tuo account</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="965"/>
        <source>Update %1</source>
        <translation>Aggiorna %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2593"/>
        <source>No new posts.</source>
        <translation>Nessun nuovo post.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2770"/>
        <source>Your Pump.io account is not configured</source>
        <translation>Il tuo account Pump.io non è configurato</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2867"/>
        <source>There is 1 new activity.</source>
        <translation>C&apos;è una nuova attività.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2871"/>
        <source>There are %1 new activities.</source>
        <translation>Ci sono %1 nuove attività.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2884"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to an activity</comment>
        <translation>1 evidenziato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2889"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to activities</comment>
        <translation>%1 evidenziati.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2923"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to one activity</comment>
        <translation>1 filtrato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2928"/>
        <source>%1 filtered out.</source>
        <comment>plural, several activities</comment>
        <translation>%1 filtrati.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2997"/>
        <source>No new activities.</source>
        <translation>Nessuna nuova attività.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3073"/>
        <source>Error storing image!</source>
        <translation>Errore durante la memorizzazione dell'immagine!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3075"/>
        <source>%1 bytes</source>
        <translation>%1 bytes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3119"/>
        <source>Link to: %1</source>
        <translation>Link a: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3138"/>
        <source>Marking everything as read...</source>
        <translation>Contrassegnare tutto come letto...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3503"/>
        <source>This version of Dianara was built with support for some extra features that use %1, also under the GNU LGPL.</source>
        <translation>Questa versione di Dianara è stata costruita con il supporto di alcune funzionalità extra che utilizzano %1, anche sotto GNU LGPL.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3592"/>
        <source>Closing due to environment shutting down...</source>
        <translation >Chiusura dovuta alla chiusura dell'ambiente...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3615"/>
        <location filename="../src/mainwindow.cpp" line="3720"/>
        <source>Quit?</source>
        <translation>Chiudere?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3616"/>
        <source>You are composing a note or a comment.</source>
        <translation>Stai componendo una nota o un commento.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3618"/>
        <source>Do you really want to close Dianara?</source>
        <translation>Vuoi veramente chiudere Dianara?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3619"/>
        <location filename="../src/mainwindow.cpp" line="3728"/>
        <source>&amp;Yes, close the program</source>
        <translation>&amp;Si, chiudi il programma</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3619"/>
        <location filename="../src/mainwindow.cpp" line="3728"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1215"/>
        <source>&amp;Configure Dianara</source>
        <translation>Configura &amp;Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <source>Minor activities done by everyone, such as replying to posts</source>
        <translation>Attività minori fatte da chiunque, come rispondere ai post</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="184"/>
        <source>Minor activities addressed to you</source>
        <translation>Attività minori indirizzate a te</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="201"/>
        <source>Minor activities done by you</source>
        <translation>Attività minori create da te</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1120"/>
        <source>&amp;Toolbar</source>
        <translation>Barra degli Strumen&amp;ti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1206"/>
        <source>&amp;Filters and Highlighting</source>
        <translation>&amp;Filtri e Evidenziazione</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1227"/>
        <source>&amp;Help</source>
        <translation>Ai&amp;uto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1240"/>
        <source>Show Welcome Wizard</source>
        <translation>Mostra procedura guidata di benvenuto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1252"/>
        <source>Visit &amp;Website</source>
        <translation>Visita il sito &amp;web</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1280"/>
        <source>Some Pump.io &amp;Tips</source>
        <translation>Alcuni consigli per &amp;Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1307"/>
        <source>About &amp;Dianara</source>
        <translation>Informazioni su &amp;Dianara </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1364"/>
        <source>Toolbar</source>
        <translation>Barra degli Strumenti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1409"/>
        <source>Open the log viewer</source>
        <translation>Apri il visualizzatore del log</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2509"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 filtrato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2514"/>
        <source>%1 filtered out.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 filtrati.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2527"/>
        <source>1 deleted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 cancellato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2532"/>
        <source>%1 deleted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 cancellati.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2712"/>
        <source>Favor&amp;ites</source>
        <translation>Prefer&amp;iti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2860"/>
        <source>Minor feed updated at %1.</source>
        <translation>Timeline laterale aggiornata %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3469"/>
        <source>With Dianara you can see your timelines, create new posts, upload pictures and other media, interact with posts, manage your contacts and follow new people.</source>
        <translation>Con Dianara puoi vedere le tue timeline, creare nuovi post, caricare immagini e altri media, interagire con post, organizzare i tuoi contatti e seguire nuove persone.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3475"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traduzione italiana a cura di Howcanuhavemyusername (or Metal Biker) (howcanuhavemyusername@microca.st). Albano Battistella (albano_battistella@hotmail.com)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3533"/>
        <source>&amp;Hide Window</source>
        <translation>&amp;Nascondi Finestra</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3544"/>
        <source>&amp;Show Window</source>
        <translation>&amp;Mostra la finestra</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2429"/>
        <source>There is 1 new post.</source>
        <translation>C&apos;è 1 nuovo messaggio.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2433"/>
        <source>There are %1 new posts.</source>
        <translation>Ci sono %1 nuovi messaggi.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3455"/>
        <source>About Dianara</source>
        <translation>Informazioni su Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3465"/>
        <source>Dianara is a pump.io social networking client.</source>
        <translation>Dianara è un client per il social network pump.io.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3482"/>
        <source>Thanks to all the testers, translators and packagers, who help make Dianara better!</source>
        <translation>Grazie a tutti i tester, i traduttori e i manutentori dei pacchetti, che hanno aiutato Dianara a migliorare!</translation>
    </message>
</context>
<context>
    <name>MinorFeed</name>
    <message>
        <location filename="../src/minorfeed.cpp" line="70"/>
        <source>Older Activities</source>
        <translation>Attività più vecchie</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="74"/>
        <source>Get previous minor activities</source>
        <translation>Ottieni attività precedenti</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="102"/>
        <source>There are no activities to show yet.</source>
        <translation>Non ci sono ancora attività da mostrare.</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="452"/>
        <source>Get %1 newer</source>
        <comment>As in: Get 3 newer (activities)</comment>
        <translation>Scarica %1 nuove</translation>
    </message>
</context>
<context>
    <name>MinorFeedItem</name>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="69"/>
        <source>Using %1</source>
        <comment>Application used to generate this activity</comment>
        <translation>Via %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="83"/>
        <source>To: %1</source>
        <comment>1=people to whom this activity was sent</comment>
        <translation>A: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="90"/>
        <source>Cc: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation>Cc: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="266"/>
        <source>Open referenced post</source>
        <translation>Apri l&apos;elemento relativo</translation>
    </message>
</context>
<context>
    <name>MiscHelpers</name>
    <message>
        <location filename="../src/mischelpers.cpp" line="315"/>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="671"/>
        <source>Error: Unable to launch browser</source>
        <translation>Errore: impossibile avviare il browser</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="672"/>
        <source>The default system web browser could not be executed.</source>
        <translation>Non è stato possibile eseguire il browser Web di sistema predefinito.</translation>
    </message>
    <message>
        <location filename="../src/mischelpers.cpp" line="675"/>
        <source>You might need to install the XDG utilities.</source>
        <translation>Potrebbe essere necessario installare i programmi di utilità XDG.</translation>
    </message>
</context>
<context>
    <name>PageSelector</name>
    <message>
        <location filename="../src/pageselector.cpp" line="26"/>
        <source>Jump to page</source>
        <translation>Salta alla pagina</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="35"/>
        <source>Page number:</source>
        <translation>Pagina numero:</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="50"/>
        <source>&amp;First</source>
        <comment>As in: first page</comment>
        <translation>&amp;Primo</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="57"/>
        <source>&amp;Last</source>
        <comment>As in: last page</comment>
        <translation>&amp;Ultimo</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="64"/>
        <source>Newer</source>
        <comment>As in: newer pages</comment>
        <translation>Più recenti</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="85"/>
        <source>Older</source>
        <comment>As in: older pages</comment>
        <translation>Più vecchi</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="96"/>
        <source>&amp;Go</source>
        <translation>&amp;Vai</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="105"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
</context>
<context>
    <name>PeopleWidget</name>
    <message>
        <location filename="../src/peoplewidget.cpp" line="40"/>
        <source>&amp;Search:</source>
        <translation>&amp;Cerca:</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="43"/>
        <source>Enter a name here to search for it</source>
        <translation>Inserisci qui un nome per cercarlo</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="102"/>
        <source>Add a contact to a list</source>
        <translation>Aggiungi un contatto ad una lista</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="114"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
</context>
<context>
    <name>Post</name>
    <message>
        <location filename="../src/post.cpp" line="1802"/>
        <source>Like</source>
        <translation>Mi piace</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1801"/>
        <source>Like this post</source>
        <translation>Dì che ti piace questo messaggio</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="37"/>
        <source>Click to download the attachment</source>
        <translation>Clicca pe rscaricare l&apos;allegato</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="140"/>
        <source>Post</source>
        <comment>Noun, not verb</comment>
        <translation>Pubblica</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="263"/>
        <location filename="../src/post.cpp" line="952"/>
        <source>Using %1</source>
        <comment>1=Program used for posting or sharing</comment>
        <translation>Via %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="359"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="944"/>
        <source>Type</source>
        <comment>As in: type of object</comment>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="959"/>
        <source>Modified on %1</source>
        <translation>Modificato il %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="522"/>
        <source>Parent</source>
        <comment>As in &apos;Open the parent post&apos;. Try to use the shortest word!</comment>
        <translation>Padre</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="531"/>
        <source>Open the parent post, to which this one replies</source>
        <translation>Apri il post padre, quello al quale questo post risponde</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="681"/>
        <source>Modify this post</source>
        <translation>Modifica questo post</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="767"/>
        <source>Join Group</source>
        <translation>Unisciti al Gruppo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="773"/>
        <source>%1 members in the group</source>
        <translation>%1 membri nel gruppo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1123"/>
        <source>Image is animated. Click on it to play.</source>
        <translation>L&apos;immagine è animata. Clicca per riprodurre.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1155"/>
        <source>Loading image...</source>
        <translation>Caricando l&apos;immagine...</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1407"/>
        <source>1 like</source>
        <translation>1 mi piace</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1476"/>
        <source>1 comment</source>
        <translation>1 commento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1576"/>
        <source>Shared %1 times</source>
        <translation>Condiviso %1 volte</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="259"/>
        <source>Shared on %1</source>
        <translation>Condiviso su %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1709"/>
        <source>Edited: %1</source>
        <translation>Modificato: %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1000"/>
        <source>In</source>
        <translation>In</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="652"/>
        <source>Share</source>
        <translation>Condividi</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="678"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1412"/>
        <source>%1 likes</source>
        <translation>%1 mi piace</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1481"/>
        <source>%1 comments</source>
        <translation>%1 commenti</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="690"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="205"/>
        <source>Via %1</source>
        <translation>Via %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="942"/>
        <source>Posted on %1</source>
        <comment>1=Date</comment>
        <translation>Pubblicato il %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="215"/>
        <location filename="../src/post.cpp" line="436"/>
        <source>To</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="632"/>
        <source>If you select some text, it will be quoted.</source>
        <translation>Se selezioni del testo, verrà quotato.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="660"/>
        <source>Unshare</source>
        <translation>Rimuovi condivisione</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="662"/>
        <source>Unshare this post</source>
        <translation>Rimuovi la condivisione di questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="313"/>
        <source>Open post in web browser</source>
        <translation>Apri il messaggio nel browser web</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="221"/>
        <location filename="../src/post.cpp" line="453"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="322"/>
        <source>Copy post link to clipboard</source>
        <translation>Copia il link al messaggio negli appunti</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="341"/>
        <source>Normalize text colors</source>
        <translation>Normalizza i colori del testo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="627"/>
        <source>Comment</source>
        <comment>verb, for the comment button</comment>
        <translation>Commenta</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="630"/>
        <source>Reply to this post.</source>
        <translation>Rispondi a questo post.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="654"/>
        <source>Share this post with your contacts</source>
        <translation>Condividi questo post con i tuoi contatti</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="693"/>
        <source>Erase this post</source>
        <translation>Cancella questo post</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1133"/>
        <source>Size</source>
        <comment>Image size (resolution)</comment>
        <translation>Dimensioni</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1150"/>
        <source>Couldn&apos;t load image!</source>
        <translation>Impossibile&apos; caricare l'immagine!</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1173"/>
        <source>Attached Audio</source>
        <translation>Audio allegato</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1183"/>
        <source>Attached Video</source>
        <translation>Video allegato</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1217"/>
        <source>Attached File</source>
        <translation>File allegato</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1381"/>
        <source>%1 likes this</source>
        <comment>One person</comment>
        <translation>A %1 piace questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1386"/>
        <source>%1 like this</source>
        <comment>More than one person</comment>
        <translation>A %1 piace questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1537"/>
        <source>%1 shared this</source>
        <comment>%1 = One person name</comment>
        <translation>%1 ha condiviso questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1542"/>
        <source>%1 shared this</source>
        <comment>%1 = Names for more than one person</comment>
        <translation>%1 hanno condiviso questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1571"/>
        <source>Shared once</source>
        <translation>Condiviso una volta</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1795"/>
        <source>You like this</source>
        <translation>Ti piace</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1796"/>
        <source>Unlike</source>
        <translation>Non mi piace più</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1957"/>
        <source>Are you sure you want to share your own post?</source>
        <translation>Sei sicuro di voler condividere il tuo post?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1962"/>
        <source>Share post?</source>
        <translation>Condividi il messaggio?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1952"/>
        <source>Do you want to share %1&apos;s post?</source>
        <translation>Vuoi condividere il messaggio di %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1965"/>
        <source>&amp;Yes, share it</source>
        <translation>&amp;Si, condividilo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1966"/>
        <location filename="../src/post.cpp" line="1986"/>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1982"/>
        <source>Unshare post?</source>
        <translation>Rimuovere la condivisione di questo elemento?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1983"/>
        <source>Do you want to unshare %1&apos;s post?</source>
        <translation>Vuoi rimuovere la condivisione dell&apos;elemento di %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1985"/>
        <source>&amp;Yes, unshare it</source>
        <translation>&amp;Si, non condividerlo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2027"/>
        <source>WARNING: Delete post?</source>
        <translation>ATTENZIONE: Eliminare il messaggio?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2028"/>
        <source>Are you sure you want to delete this post?</source>
        <translation>Sei sicuro di voler eliminare questo messaggio?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="2030"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Si, eliminalo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="36"/>
        <source>Click the image to see it in full size</source>
        <translation>Clicca l&apos;immagine per vederla nelle dimensioni originali</translation>
    </message>
</context>
<context>
    <name>ProfileEditor</name>
    <message>
        <location filename="../src/profileeditor.cpp" line="27"/>
        <source>Profile Editor</source>
        <translation>Editor del profilo</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="51"/>
        <source>This is your Pump address</source>
        <translation>Questo è il tuo indirizzo Pump.io</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="53"/>
        <source>This is the e-mail address associated with your account, for things such as notifications and password recovery</source>
        <translation>Questa è l&apos;indirizzo e-mail associato al tuo accout, per inviare notifiche e recuperare la password</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="70"/>
        <source>Change &amp;E-mail...</source>
        <translation>Cambia &amp;e-mail...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="82"/>
        <source>Change &amp;Avatar...</source>
        <translation>Cambia &amp;avatar...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="92"/>
        <source>This is your visible name</source>
        <translation>Questo è il tuo nome visibile</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="109"/>
        <source>Changing your avatar will create a post in your timeline with it.
If you delete that post your avatar will be deleted too.</source>
        <translation>La modifica del tuo avatar creerà un post nella tua sequenza temporale con esso.
Se elimini quel post, anche il tuo avatar verrà eliminato.</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="126"/>
        <source>&amp;Save Profile</source>
        <translation>&amp;Salva profilo</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="133"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="166"/>
        <source>Webfinger ID</source>
        <translation>Webfinger ID</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="167"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="169"/>
        <source>Avatar</source>
        <translation>Avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="171"/>
        <source>Full &amp;Name</source>
        <translation>&amp;Nome Completo</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="172"/>
        <source>&amp;Hometown</source>
        <translation>Ci&amp;ttà</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="173"/>
        <source>&amp;Bio</source>
        <translation>&amp;Bio</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="211"/>
        <source>Not set</source>
        <comment>In reference to the e-mail not being set for the account</comment>
        <translation>Non impostata</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="284"/>
        <source>Select avatar image</source>
        <translation>Scegli l&apos;immagine per l&apos;avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="286"/>
        <source>Image files</source>
        <translation>Files immagine</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="289"/>
        <source>All files</source>
        <translation>Tutti i files</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="312"/>
        <source>Invalid image</source>
        <translation>Immagine non valida</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="313"/>
        <source>The selected image is not valid.</source>
        <translation>L&apos;immagine selezionata non è valida.</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../src/proxydialog.cpp" line="30"/>
        <source>Proxy Configuration</source>
        <translation>Configurazione Proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="38"/>
        <source>Do not use a proxy</source>
        <translation>Non usare un proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="55"/>
        <source>Your proxy username</source>
        <translation>Il tuo username del proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="60"/>
        <source>Note: Password is not stored in a secure manner. If you wish, you can leave the field empty, and you&apos;ll be prompted for the password on startup.</source>
        <translation>Nota: la password non è salvata in modo sicuro. Se lo desideri, puoi lasciare questo campo vuoto e ti verrà chiesta la password all&apos;avvio.</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="75"/>
        <source>&amp;Save</source>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="82"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="98"/>
        <source>Proxy &amp;Type</source>
        <translation>&amp;Tipo di Proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="100"/>
        <source>&amp;Hostname</source>
        <translation>&amp;Hostname</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="102"/>
        <source>&amp;Port</source>
        <translation>&amp;Porta</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="104"/>
        <source>Use &amp;Authentication</source>
        <translation>Usa Autenti&amp;cazione</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="106"/>
        <source>&amp;User</source>
        <translation>&amp;Utente</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="108"/>
        <source>Pass&amp;word</source>
        <translation>Pass&amp;word</translation>
    </message>
</context>
<context>
    <name>Publisher</name>
    <message>
        <location filename="../src/publisher.cpp" line="75"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="80"/>
        <source>Add a brief title for the post here (recommended)</source>
        <translatorcomment>(Jan) This one should be updated with the &quot;here&quot;</translatorcomment>
        <translation>Aggiungi un breve titolo per il post (raccomandato)</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="232"/>
        <source>Picture</source>
        <translation>Immagine</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="239"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="246"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="269"/>
        <source>Ad&amp;d...</source>
        <translation>Aggiun&amp;gi...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="272"/>
        <source>Upload media, like pictures or videos</source>
        <translation>Carica media, come immagini o video</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1054"/>
        <source>Select Picture...</source>
        <translation>Selezionare l'&apos;immagine...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1056"/>
        <source>Find the picture in your folders</source>
        <translation>Trova l'&apos;immagine nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="178"/>
        <source>Drafts</source>
        <translation>Bozze</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="188"/>
        <source>To...</source>
        <translation>A...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="202"/>
        <source>Select who will get a copy of this post</source>
        <translation>Scegli chi riceverà una copia di questo messaggio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="253"/>
        <source>Other</source>
        <comment>as in other kinds of files</comment>
        <translation>Altro</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="306"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="309"/>
        <source>Cancel the post</source>
        <translation>Cancella il messaggio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1049"/>
        <source>Picture not set</source>
        <translation>Immagine non selezionata</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1079"/>
        <source>Select Audio File...</source>
        <translation>Seleziona File Audio...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1081"/>
        <source>Find the audio file in your folders</source>
        <translation>Cerca il file audio nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1074"/>
        <source>Audio file not set</source>
        <translation>Il file audio non è impostato</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1104"/>
        <source>Select Video...</source>
        <translation>Seleziona Video...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1106"/>
        <source>Find the video in your folders</source>
        <translation>Cerca il file video nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1099"/>
        <source>Video not set</source>
        <translation>Video non impostato</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1128"/>
        <source>Select File...</source>
        <translation>Seleziona File...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1130"/>
        <source>Find the file in your folders</source>
        <translation>Trova il file nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1123"/>
        <source>File not set</source>
        <translation>File non impostato</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1199"/>
        <location filename="../src/publisher.cpp" line="1243"/>
        <source>Error: Already composing</source>
        <translation>Errore: stai già scrivendo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1200"/>
        <source>You can&apos;t edit a post at this time, because a post is already being composed.</source>
        <translation>Non puoi modificare il messaggio ora, perchè stai già scrivendo un messaggio.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1218"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1244"/>
        <source>You can&apos;t create a message for %1 at this time, because a post is already being composed.</source>
        <translation>Non puoi creare un messaggio per %1 al momento, perche un post è già in fase di composizione.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1371"/>
        <source>Draft loaded.</source>
        <translation>Bozza caricata</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1376"/>
        <source>ERROR: Already composing</source>
        <translation>ERRORE: già composto</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1377"/>
        <source>You can&apos;t load a draft at this time, because a post is already being composed.</source>
        <translation>Non puoi caricare una bozza in questo momento, perché un post è già in fase di composizione.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1396"/>
        <source>Draft saved.</source>
        <translation>Bozza salvata</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1433"/>
        <source>Posting failed.

Try again.</source>
        <translation>Invio fallito.

Prova di nuovo.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1577"/>
        <source>Warning: You have no followers yet</source>
        <translation>Avviso: non hai ancora followers</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1578"/>
        <source>You&apos;re trying to post to your followers only, but you don&apos;t have any followers yet.</source>
        <translation>Stai cercando di postare solo per i tuoi followers, ma non ne hai ancora.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1582"/>
        <source>If you post like this, no one will be able to see your message.</source>
        <translation>Se posti con queste impostazioni, nessuno sarà in grado di leggere il tuo messaggio.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1585"/>
        <source>Do you want to make the post public instead of followers-only?</source>
        <translation>Vuoi rendere il post pubblico invece che renderlo visibile solo ai tuoi followers?</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1588"/>
        <source>&amp;Yes, make it public</source>
        <translation>&amp;Si, rendilo pubblico</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1590"/>
        <source>&amp;Cancel, go back to the post</source>
        <translation>&amp;Annulla e torna al post</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1643"/>
        <source>Updating...</source>
        <translation>Aggiornando...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1654"/>
        <source>Post is empty.</source>
        <translation>Il messaggio è vuoto.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1658"/>
        <source>File not selected.</source>
        <translation>File non selezionato.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="597"/>
        <source>Select one image</source>
        <translation>Scegli un&apos;immagine</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="598"/>
        <source>Image files</source>
        <translation>Files immagine</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="626"/>
        <source>Select one file</source>
        <translation>Seleziona un file</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="629"/>
        <source>Invalid file</source>
        <translation>File non valido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="630"/>
        <source>The file type cannot be detected.</source>
        <translation>Il tipo di file non può essere identificato.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="642"/>
        <source>All files</source>
        <translation>Tutti i files</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="659"/>
        <source>Since you&apos;re uploading an image, you could scale it down a little or save it in a more compressed format, like JPG.</source>
        <translation>Visto che stai caricando un&apos;immagine, potresti ridurre la sua risoluzione o salvarla in un formato più compresso, come JPG.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="664"/>
        <source>File is too big</source>
        <translation>File troppo grande</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="665"/>
        <source>Dianara currently limits file uploads to 10 MiB per post, to prevent possible storage or network problems in the servers.</source>
        <translation>Dianara limita la dimensione dei file caricati a 10MiB per post, in modo da prevenire problemi di spazio e rete dei server.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="670"/>
        <source>This is a temporary measure, since the servers cannot set their own limits yet.</source>
        <translation>Questa è una misura temporanea, in quanto i server non possono ancora impostare i loro limiti.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="675"/>
        <source>Sorry for the inconvenience.</source>
        <translation>Ci scusiamo per il disagio.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="847"/>
        <source>File not found.</source>
        <translation>File non trovato</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="850"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="832"/>
        <source>The selected file cannot be accessed:</source>
        <translation>Non è possibile accedere al file selezionato:</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="839"/>
        <source>It is owned by %1.</source>
        <comment>%1 = a username</comment>
        <translation>È di proprietà di %1.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="842"/>
        <source>You might not have the necessary permissions.</source>
        <translation>Potresti non avere le autorizzazioni necessarie.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="704"/>
        <source>Resolution</source>
        <comment>Image resolution (size)</comment>
        <translation>Risoluzione</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="727"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="729"/>
        <source>Size</source>
        <translation>Dimensioni</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1686"/>
        <source>%1 KiB of %2 KiB uploaded</source>
        <translation>%1 KiB di %2 KiB caricati</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="601"/>
        <source>Invalid image</source>
        <translation>Immagine non valida</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="69"/>
        <source>Setting a title helps make the Meanwhile feed more informative</source>
        <translation>Impostare un titolo rende il feed laterale più informativo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="108"/>
        <source>Remove</source>
        <translation>Rimuovi</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="111"/>
        <source>Cancel the attachment, and go back to a regular note</source>
        <translation>Rimuovi l&apos;allegato e torna al post regolare</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="199"/>
        <source>Cc...</source>
        <translation>Cc...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="297"/>
        <location filename="../src/publisher.cpp" line="942"/>
        <source>Post</source>
        <comment>verb</comment>
        <translation>Pubblica</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="427"/>
        <source>Note started from another application.</source>
        <translation>Nota avviata da un'altra applicazione.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="431"/>
        <source>Ignoring new note request from another application.</source>
        <translation>Ignorando la nuova richiesta di nota da un'altra applicazione.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="964"/>
        <source>Attachment upload was cancelled.</source>
        <translation>Il caricamento dell'allegato è stato annullato.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1229"/>
        <source>Editing post.</source>
        <translation>Modifica il messaggio.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1589"/>
        <source>&amp;No, post to my followers only</source>
        <translation>&amp;No, rendilo visibile solo ai miei follower</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="602"/>
        <source>The image format cannot be detected.
The extension might be wrong, like a GIF image renamed to image.jpg or similar.</source>
        <translation>Il formato dell&apos;immagine non è stato riconosciuto. L&apos;estensione può essere sbagliata, come un&apos;immagine GIF rinominata in immagine.jpg o altro.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="608"/>
        <source>Select one audio file</source>
        <translation>Seleziona un file audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="609"/>
        <source>Audio files</source>
        <translation>Files audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="612"/>
        <source>Invalid audio file</source>
        <translation>File audio non valido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="613"/>
        <source>The audio format cannot be detected.</source>
        <translation>Il formato del file audio non può essere rilevato.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="617"/>
        <source>Select one video file</source>
        <translation>Seleziona un file video</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="618"/>
        <source>Video files</source>
        <translation>Files Video</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="621"/>
        <source>Invalid video file</source>
        <translation>File video non valido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="622"/>
        <source>The video format cannot be detected.</source>
        <translation>Il formato del file video non può essere rilevato.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1610"/>
        <source>Posting...</source>
        <translation>Pubblicando...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="191"/>
        <source>Select who will see this post</source>
        <translation>Scegli chi potrà vedere il messaggio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="300"/>
        <source>Hit Control+Enter to post with the keyboard</source>
        <translation>Premi Control+Invio per pubblicare con la tastiera</translation>
    </message>
</context>
<context>
    <name>PumpController</name>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="669"/>
        <source>Creating person list...</source>
        <translation>Creando la lista delle persone...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="692"/>
        <source>Deleting person list...</source>
        <translation>Eliminando la lista delle persone...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="707"/>
        <source>Getting a person list...</source>
        <translation>Ottenendo la lista della persona...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="751"/>
        <source>Adding person to list...</source>
        <translation>Aggiungendo una persona alla lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="792"/>
        <source>Removing person from list...</source>
        <translation>Rimuovendo una persona dalla lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="840"/>
        <source>Creating group...</source>
        <translation>Creando un gruppo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="870"/>
        <source>Joining group...</source>
        <translation>Unendosi al gruppo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="895"/>
        <source>Leaving group...</source>
        <translation>Lasciando il gruppo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="910"/>
        <source>Getting likes...</source>
        <translation>Ricevendo i &quot;mi piace&quot;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="942"/>
        <source>Getting comments...</source>
        <translation>Ricevendo i commenti...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="998"/>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Ottenendo &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1045"/>
        <source>Timeline</source>
        <translation>Timeline</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1049"/>
        <source>Messages</source>
        <translation>Messaggi</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1207"/>
        <source>Uploading %1</source>
        <comment>1=filename</comment>
        <translation>Caricando %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1505"/>
        <source>HTTP error</source>
        <comment>For the following HTTP error codesyou can check http://en.wikipedia.org/wiki/List_of_HTTP_status_codes in your language</comment>
        <translation>Errore HTTP</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1517"/>
        <source>Gateway Timeout</source>
        <comment>HTTP 504 error string</comment>
        <translation>Gateway Timeout</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1527"/>
        <source>Service Unavailable</source>
        <comment>HTTP 503 error string</comment>
        <translation>Servizio non Disponibile</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1556"/>
        <source>Not Implemented</source>
        <comment>HTTP 501 error string</comment>
        <translation>Non implementato</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1566"/>
        <source>Internal Server Error</source>
        <comment>HTTP 500 error string</comment>
        <translation>Errore Interno del Server</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1586"/>
        <source>Gone</source>
        <comment>HTTP 410 error string</comment>
        <translation>Sparito</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1596"/>
        <source>Not Found</source>
        <comment>HTTP 404 error string</comment>
        <translation>Non Trovato</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1606"/>
        <source>Forbidden</source>
        <comment>HTTP 403 error string</comment>
        <translation>Proibito</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1616"/>
        <source>Unauthorized</source>
        <comment>HTTP 401 error string</comment>
        <translation>Non Autorizzato</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1627"/>
        <source>Bad Request</source>
        <comment>HTTP 400 error string</comment>
        <translation>Richiesta Errata</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1646"/>
        <source>Moved Temporarily</source>
        <comment>HTTP 302 error string</comment>
        <translation>Spostato Temporaneamente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1656"/>
        <source>Moved Permanently</source>
        <comment>HTTP 301 error string</comment>
        <translation>Spostato Permanentemente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1667"/>
        <source>Error connecting to %1</source>
        <translation>Errore durante la connessione a %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1677"/>
        <source>Unhandled HTTP error code %1</source>
        <translation>Codice di errore HTTP non gestito: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1790"/>
        <source>Profile received.</source>
        <translation>Profilo ricevuto.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1792"/>
        <source>Followers</source>
        <translation>Followers</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1797"/>
        <source>Following</source>
        <translation>Following</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1813"/>
        <source>Profile updated.</source>
        <translation>Profilo aggiornato.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1887"/>
        <source>Avatar published successfully.</source>
        <translation>Avatar pubblicato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2006"/>
        <source>1 comment received.</source>
        <translation>1 commento ricevuto.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2010"/>
        <source>%1 comments received.</source>
        <translation>%1 commenti ricevuti.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2037"/>
        <source>Post by %1 shared successfully.</source>
        <comment>1=author of the post we are sharing</comment>
        <translation>Post di %1 condiviso con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2068"/>
        <source>Received &apos;%1&apos;.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Ricevuto &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2081"/>
        <source>Adding items...</source>
        <translation>Aggiungendo oggetti...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2607"/>
        <source>SSL errors in connection to %1!</source>
        <translation>Errori SSL durante la connessione a %1!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2818"/>
        <source>OAuth error while authorizing application.</source>
        <translation>Errore OAuth in fase di autorizzazione dell&apos;appliazione.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1053"/>
        <source>Activity</source>
        <translation>Attività</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1057"/>
        <source>Favorites</source>
        <translation>Preferiti</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1062"/>
        <source>Meanwhile</source>
        <translation>Nel frattempo</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1066"/>
        <source>Mentions</source>
        <translation>Menzioni</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1070"/>
        <source>Actions</source>
        <translation>Azioni</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2252"/>
        <source>List of &apos;following&apos; completely received.</source>
        <translation>Lista dei &apos;following&apos; ricevuta completamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2259"/>
        <source>Partial list of &apos;following&apos; received.</source>
        <translation>Lista dei &apos;following&apos; ricevuta parzialmente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2282"/>
        <source>List of &apos;followers&apos; completely received.</source>
        <translation>Lista dei &apos;followers&apos; ricevuta completamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2288"/>
        <source>Partial list of &apos;followers&apos; received.</source>
        <translation>Lista dei &apos;followers&apos; ricevuta parzialmente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2380"/>
        <source>Person list deleted successfully.</source>
        <translation>Lista di persone eliminata correttamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2400"/>
        <source>Person list received.</source>
        <translation>Lista di persone ricevuta.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2554"/>
        <source>File uploaded successfully. Posting message...</source>
        <translation>File caricato con successo. Pubblicando il messaggio...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="252"/>
        <source>Authorized to use account %1. Getting initial data.</source>
        <translation>Autorizzato ad utilizzare l&apos;account %1. Ricevendo i dati iniziali.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="257"/>
        <source>There is no authorized account.</source>
        <translation>Non ci sono account autorizzati.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="362"/>
        <source>Updating profile...</source>
        <translation>Aggiornamento profilo ...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="549"/>
        <source>Getting list of &apos;Following&apos;...</source>
        <translation>Ricevendo la lista dei &apos;Following&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="562"/>
        <source>Getting list of &apos;Followers&apos;...</source>
        <translation>Ricevendo la lista dei &apos;Followers&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="581"/>
        <source>Getting site users for %1...</source>
        <comment>%1 is a server name</comment>
        <translation>Ottenere gli utenti del sito per %1...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="633"/>
        <source>Getting list of person lists...</source>
        <translation>Rocevendo le liste delle persone...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="937"/>
        <source>The comments for this post cannot be loaded due to missing data on the server.</source>
        <translation>I commenti per questo post non possono essere caricati a causa della mancanza di dati sul server.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1075"/>
        <source>User timeline</source>
        <translation>Timeline utente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1465"/>
        <source>Error loading timeline!</source>
        <translation>Errore durante il caricamento della timeline!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1476"/>
        <source>Error loading minor feed!</source>
        <translation>Errore durante il caricamento del feed secondario!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1488"/>
        <source>Unable to verify the address!</source>
        <translation>Impossibile verificare l'indirizzo!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1546"/>
        <source>Bad Gateway</source>
        <comment>HTTP 502 error string</comment>
        <translation>Bad Gateway</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1787"/>
        <source>Server version: %1</source>
        <translation>Versione server: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1822"/>
        <source>E-mail updated: %1</source>
        <translation>Email aggiornata: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1850"/>
        <source>%1 published successfully. Updating post content...</source>
        <comment>%1 is the type of object: note, image...</comment>
        <translation>%1 pubblicato correttamente. Aggiornamento del contenuto dei post...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1866"/>
        <source>Untitled post %1 published successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translatorcomment>(Jan) I tried my luck adjusting this one</translatorcomment>
        <translation type="unfinished">Messaggio senza titolo %1 pubblicato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1870"/>
        <source>Post %1 published successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translatorcomment>(Jan) I tried my luck adjusting this one</translatorcomment>
        <translation>Messaggio %1 pubblicato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1919"/>
        <source>Untitled post %1 updated successfully.</source>
        <comment>%1 is a piece of the post</comment>
        <translatorcomment>(Jan) I tried my luck adjusting this one</translatorcomment>
        <translation>Messaggio senza titolo %1 aggiornato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1923"/>
        <source>Post %1 updated successfully.</source>
        <comment>%1 is the title of the post</comment>
        <translatorcomment>(Jan) I tried my luck adjusting this one</translatorcomment>
        <translation>Messaggio %1 aggiornato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1933"/>
        <source>Comment %1 updated successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translatorcomment>(Jan) I tried my luck adjusting this one</translatorcomment>
        <translation>Commento %1 aggiornato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1947"/>
        <source>Message liked or unliked successfully.</source>
        <translation>Messaggio aggiunto o rimosso dai &quot;Mi piace&quot; correttamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1961"/>
        <source>Likes received.</source>
        <translation>&quot;Mi piace&quot; ricevuto.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1981"/>
        <source>Comment %1 posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translatorcomment>(Jan) I tried my luck adjusting this one</translatorcomment>
        <translation>Commento %1 pubblicato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2164"/>
        <source>Message deleted successfully.</source>
        <translation>Messaggio cancellato correttamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2192"/>
        <source>Following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Segui %1 (%2) con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2202"/>
        <source>Stopped following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Hai smesso di seguire %1 (%2) con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2325"/>
        <source>List of &apos;lists&apos; received.</source>
        <translation>Lista delle &apos;Liste&apos; ricevuta.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2345"/>
        <source>List of %1 users received.</source>
        <comment>%1 is a server name</comment>
        <translation>Elenco di %1 utenti ricevuti.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2369"/>
        <source>Person list &apos;%1&apos; created successfully.</source>
        <translation>Lista di persone &apos;%1&apos; creata con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2435"/>
        <source>%1 (%2) added to list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 (%2) aggiunto alla lista con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2455"/>
        <source>%1 (%2) removed from list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 (%2) rimosso dalla lista con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2466"/>
        <source>Group %1 created successfully.</source>
        <translation>Gruppo %1 creato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2477"/>
        <source>Group %1 joined successfully.</source>
        <translation>Unito al gruppo %1 con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2490"/>
        <source>Left the %1 group successfully.</source>
        <translation>Lasciato il gruppo %1 con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2563"/>
        <source>Avatar uploaded.</source>
        <translation>Avatar caricato.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2619"/>
        <source>Loading external image from %1 regardless of SSL errors, as configured...</source>
        <comment>%1 is a hostname</comment>
        <translation>Caricamento immagine esterna da %1 indipendentemente dagli errori SSL, come configurato...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2648"/>
        <source>The application is not registered with your server yet. Registering...</source>
        <translation>L&apos;applicazione non è ancora autorizzata con il tuo server. Registrazione...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2686"/>
        <source>Getting OAuth token...</source>
        <translation>Ricevendo il token OAuth...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2707"/>
        <source>OAuth support error</source>
        <translation>Errore supporto OAuth</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2708"/>
        <source>Your installation of QOAuth, a library used by Dianara, doesn&apos;t seem to have HMAC-SHA1 support.</source>
        <translation>La tua installazione di QOAuth, libreria usata da Dianara, sembra che non abbia il supporto HMAC-SHA1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2712"/>
        <source>You probably need to install the OpenSSL plugin for QCA: %1, %2 or similar.</source>
        <translation>Probabilmente hai bisogno di installare il plugin OpenSSL per QCA: %1, %2 o simili.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2766"/>
        <location filename="../src/pumpcontroller.cpp" line="2822"/>
        <source>Authorization error</source>
        <translation>Errore di Autorizzazione</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2767"/>
        <source>There was an OAuth error while trying to get the authorization token.</source>
        <translation>C&apos;è stato un errore OAuth nel tentativo di ottenere il token di autorizzazione.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2770"/>
        <source>QOAuth error %1</source>
        <translation>Errore QOAuth %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2802"/>
        <source>Application authorized successfully.</source>
        <translation>Applicazione autorizzata con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2845"/>
        <source>Waiting for proxy password...</source>
        <translation>In attesa della password del proxy...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2874"/>
        <source>Still waiting for profile. Trying again...</source>
        <translation>Aspettando ancora il tuo profilo. Provo ancora...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2986"/>
        <source>%1 attempts</source>
        <translation>%1 tentativi</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2990"/>
        <source>%1 attempt</source>
        <translation>%1 tentativo</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2993"/>
        <source>Some initial data was not received. Restarting initialization...</source>
        <translation>Alcuni dati iniziali non sono stati ricevuti. Riavvio inizializzazione...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3411"/>
        <source>Can&apos;t follow %1 at this time.</source>
        <comment>%1 is a user ID</comment>
        <translation type="unfinished">Impossibile&apos; seguire %1 in questo momento.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3415"/>
        <source>Trying to follow %1.</source>
        <comment>%1 is a user ID</comment>
        <translation>Prova a seguire %1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3435"/>
        <source>Checking address %1 before following...</source>
        <translation>Verifica indirizzo %1 prima di seguire...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3000"/>
        <source>Some initial data was not received after several attempts. Something might be wrong with your server. You might still be able to use the service normally.</source>
        <translation>Alcuni dati iniziali non sono stati ricevuti anche dopo diversi tentativi. Qualcosa potrebbe non funzionare sul tuo server. Potresti essere comunque in grado di usare il servizio normalmente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3010"/>
        <source>All initial data received. Initialization complete.</source>
        <translation>Tutti i dati iniziali ricevuti. Inizializzazione completa.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="3023"/>
        <source>Ready.</source>
        <translation>Pronto.</translation>
    </message>
</context>
<context>
    <name>SiteUsersList</name>
    <message>
        <location filename="../src/siteuserslist.cpp" line="30"/>
        <source>You can get a list of the newest users registered on your server by clicking the button below.</source>
        <translation>Puoi ottenere un elenco degli utenti più recenti registrati sul tuo server facendo clic sul pulsante in basso.</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="47"/>
        <source>More resources to find users:</source>
        <translation>Altre risorse per trovare utenti:</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="51"/>
        <source>Wiki page &apos;Users by language&apos;</source>
        <translation>Pagina Wiki&apos;Utenti per lingua&apos;</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="56"/>
        <source>PPump user search service at inventati.org</source>
        <translation>Servizio di ricerca utenti PPump su inventati.org</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="61"/>
        <source>List of Followers for the Pump.io Community account</source>
        <translation>Elenco dei follower per l'account Community Pump.io</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="41"/>
        <source>Get list of users from your server</source>
        <translation>Ottieni un elenco di utenti dal tuo server</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="87"/>
        <source>Close list</source>
        <translation>Chiudi lista</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="143"/>
        <source>Loading...</source>
        <translation>Caricamento in corso...</translation>
    </message>
    <message>
        <location filename="../src/siteuserslist.cpp" line="154"/>
        <source>%1 users in %2</source>
        <comment>%1 = user count, %2 = server name</comment>
        <translation>%1 utenti in %2</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/timeline.cpp" line="76"/>
        <source>Welcome to Dianara</source>
        <translation>Benvenuto in Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="78"/>
        <source>Dianara is a &lt;b&gt;Pump.io&lt;/b&gt; client.</source>
        <translation>Dianara è un client &lt;b&gt;Pump.io&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="88"/>
        <source>Press &lt;b&gt;F1&lt;/b&gt; if you want to open the Help window.</source>
        <translation>Premi &lt;b&gt;F1&lt;/b&gt; se vuoi aprire la finestra di Aiuto.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="91"/>
        <source>First, configure your account from the &lt;b&gt;Settings - Account&lt;/b&gt; menu.</source>
        <translation>Prima di tutto, configura il tuo account nel menù &lt;b&gt;Configurazione - Account&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="94"/>
        <source>After the process is done, your profile and timelines should update automatically.</source>
        <translation>Completato il processo, il tuo profilo e le timelines dovrebbero aggiornarsi automaticamente.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="98"/>
        <source>Take a moment to look around the menus and the Configuration window.</source>
        <translation>Prenditi un momento per dare uno sguardo ai menù ed alla finestra di Configurazione.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="102"/>
        <source>You can also set your profile data and picture from the &lt;b&gt;Settings - Edit Profile&lt;/b&gt; menu.</source>
        <translation>Puoi anche impostare i dettagli e la foto del tuo profilo nel menu &lt;b&gt;Configurazione - Modifica profilo&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="113"/>
        <source>Dianara&apos;s blog</source>
        <translation>Blog di Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="231"/>
        <source>Newest</source>
        <translation>Ultimi</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="240"/>
        <source>Newer</source>
        <translation>Più recenti</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="263"/>
        <source>Older</source>
        <translation>Più vecchi</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="335"/>
        <source>Requesting...</source>
        <translation>Richiesta...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="506"/>
        <source>Loading...</source>
        <translation>Caricamento in corso...</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="615"/>
        <source>Page %1 of %2.</source>
        <translation>Pagina %1 di %2.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="619"/>
        <source>Showing %1 posts per page.</source>
        <translation>Mostra %1 posts per pagina.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="622"/>
        <source>%1 posts in total.</source>
        <translation>%1 post in totale.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="626"/>
        <source>Click here or press Control+G to jump to a specific page</source>
        <translation>Clicca qui o premi Control+G per saltare ad una pagina specifica</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="740"/>
        <source>&apos;%1&apos; cannot be updated because a comment is currently being composed.</source>
        <comment>%1 = feed&apos;s name</comment>
        <translation>&apos;%1&apos; non può essere aggiornato perchè un commento è in fase di composizione.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="848"/>
        <source>%1 more posts pending for next update.</source>
        <translation>%1 altri post in sospeso per il prossimo aggiornamento.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="854"/>
        <source>Click here to receive them now.</source>
        <translation>Clicca qui per riceverli ora.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="1139"/>
        <source>There are no posts</source>
        <translation>Non ci sono messaggi</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="81"/>
        <source>If you don&apos;t have a Pump account yet, you can get one at the following address, for instance:</source>
        <translation>se non hai ancora un account Pump, puoi averne uno al seguente indirizzo, per esempio:</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="106"/>
        <source>There are tooltips everywhere, so if you hover over a button or a text field with your mouse, you&apos;ll probably see some extra information.</source>
        <translation>Ci sono tooltip ovunque, quindi se ti fermi col mouse sopra ad un bottone o ad un testo, probabilmente vedrai informazioni extra.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="115"/>
        <source>Pump.io User Guide</source>
        <translation>Guida Utente Pump.io</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="126"/>
        <source>Direct Messages Timeline</source>
        <translation>Timeline dei messaggi diretti</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="127"/>
        <source>Here, you&apos;ll see posts specifically directed to you.</source>
        <translation>Qui potrai vedere messaggi indirizzati specificatamente a te.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="137"/>
        <source>Activity Timeline</source>
        <translation>Timeline delle Attività</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="138"/>
        <source>You&apos;ll see your own posts here.</source>
        <translation>Qui vedrai i tuoi messaggi.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="147"/>
        <source>Favorites Timeline</source>
        <translation>Timeline dei Preferiti</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="148"/>
        <source>Posts and comments you&apos;ve liked.</source>
        <translation>Messaggi e commenti che ti sono piaciuti.</translation>
    </message>
</context>
<context>
    <name>Timestamp</name>
    <message>
        <location filename="../src/timestamp.cpp" line="61"/>
        <source>Invalid timestamp!</source>
        <translation>Timestamp non Valida!</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="93"/>
        <source>A minute ago</source>
        <translation>Un minuto fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="97"/>
        <source>%1 minutes ago</source>
        <translation>%1 minuti fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="108"/>
        <source>An hour ago</source>
        <translation>Un&apos;ora fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="112"/>
        <source>%1 hours ago</source>
        <translation>%1 ore fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="79"/>
        <source>Just now</source>
        <translation>In questo istante</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="83"/>
        <source>In the future</source>
        <translation>Prossimamente</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="123"/>
        <source>Yesterday</source>
        <translation>Ieri</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="127"/>
        <source>%1 days ago</source>
        <translation>%1 giorni fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="138"/>
        <source>A month ago</source>
        <translation>Un mese fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="142"/>
        <source>%1 months ago</source>
        <translation>%1 mesi fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="151"/>
        <source>A year ago</source>
        <translation>Un anno fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="155"/>
        <source>%1 years ago</source>
        <translation>%1 anni fa</translation>
    </message>
</context>
<context>
    <name>UserPosts</name>
    <message>
        <location filename="../src/userposts.cpp" line="39"/>
        <source>Posts by %1</source>
        <translation>Post di %1</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="64"/>
        <source>Loading...</source>
        <translation>Caricamento in corso...</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="69"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="153"/>
        <source>Received &apos;%1&apos;.</source>
        <translation>Ricevuto &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="160"/>
        <source>%1 posts</source>
        <translation>%1 post</translation>
    </message>
    <message>
        <location filename="../src/userposts.cpp" line="169"/>
        <source>Error loading the timeline</source>
        <translation type="unfinished">Errore durante il caricamento della timeline</translation>
    </message>
</context>
</TS>
